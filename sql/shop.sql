-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: shop
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `shop`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `shop` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `shop`;

--
-- Table structure for table `sys_access_log`
--

DROP TABLE IF EXISTS `sys_access_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_access_log` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '接口名称',
  `business_type` int NOT NULL COMMENT '业务类型',
  `access_user` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '操作人员',
  `access_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '请求URL',
  `method` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '调用方法',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '请求方式',
  `source_ip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '主机地址',
  `source_port` int DEFAULT '0' COMMENT '源端口',
  `source_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '操作地点',
  `access_param` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '请求参数',
  `status` int DEFAULT '0' COMMENT '操作状态（1正常 0异常）',
  `json_result` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '返回参数',
  `error_msg` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '错误消息',
  `access_time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='操作日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_access_log`
--

LOCK TABLES `sys_access_log` WRITE;
/*!40000 ALTER TABLE `sys_access_log` DISABLE KEYS */;
INSERT INTO `sys_access_log` VALUES (1,'用户管理',3,'root','/sys/user','cn.fabhub.shopadmin.controller.SysUserController.update','PUT','127.0.0.1',57801,'局域网','{\"avatar\":\"https://m-zhou.oss-cn-chengdu.aliyuncs.com/%E7%8C%AB.jpg\",\"id\":74,\"loginName\":\"wu\",\"password\":\"123456\",\"phone\":\"18112345678\",\"roleIds\":[2],\"sex\":1,\"status\":0,\"username\":\"wu\"}',1,NULL,NULL,'2020-11-16 16:52:00'),(2,'角色管理',3,'root','/sys/role','cn.fabhub.shopadmin.controller.SysRoleController.update','PUT','127.0.0.1',57838,'局域网','{\"id\":2,\"isDefault\":1,\"remark\":\"普通用户\",\"resourceIds\":[58,3,34,37,43,51,59,36,47,48,49,50,38,52,54,53,40,60,68,69,81,84,85,82,83],\"roleName\":\"ROLE_USER\",\"status\":1,\"zhName\":\"普通用户\"}',1,NULL,NULL,'2020-11-16 16:52:21'),(3,'角色管理',3,'root','/sys/role','cn.fabhub.shopadmin.controller.SysRoleController.update','PUT','127.0.0.1',57853,'局域网','{\"id\":2,\"isDefault\":1,\"remark\":\"普通用户\",\"resourceIds\":[58,3,34,37,43,51,59,36,47,48,49,50,38,52,54,53,40,60,68,69,81,84,85,82,83],\"roleName\":\"ROLE_USER\",\"status\":0,\"zhName\":\"普通用户\"}',1,NULL,NULL,'2020-11-16 16:52:26'),(4,'资源管理',3,'root','/sys/resource','cn.fabhub.shopadmin.controller.SysResourceController.delete','PUT','127.0.0.1',57896,'局域网','{\"component\":\"monitor/datasource/Index\",\"icon\":\"book\",\"id\":57,\"name\":\"数据监控\",\"openType\":3,\"orderNum\":420,\"parentId\":35,\"router\":\"http://localhost:8080/druid/datasource.html\",\"status\":0,\"type\":2,\"visible\":1}',0,NULL,'\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 3\r\n### The error may exist in cn/fabhub/shopadmin/mapper/SysResourceMapper.xml\r\n### The error may involve defaultParameterMap\r\n### The error occurred while setting parameters\r\n### SQL: update sys_resource         set status = ?          WHERE id in\r\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 3\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 3','2020-11-16 16:52:41'),(5,'资源管理',3,'root','/sys/resource','cn.fabhub.shopadmin.controller.SysResourceController.delete','PUT','127.0.0.1',57905,'局域网','{\"component\":\"monitor/datasource/Index\",\"icon\":\"book\",\"id\":57,\"name\":\"数据监控\",\"openType\":3,\"orderNum\":420,\"parentId\":35,\"router\":\"http://localhost:8080/druid/datasource.html\",\"status\":1,\"type\":2,\"visible\":1}',1,NULL,NULL,'2020-11-16 16:52:54'),(6,'资源管理',3,'root','/sys/resource','cn.fabhub.shopadmin.controller.SysResourceController.delete','PUT','127.0.0.1',57939,'局域网','{\"id\":88,\"name\":\"测试狗\",\"openType\":3,\"orderNum\":1,\"parentId\":76,\"status\":0,\"type\":2,\"visible\":0}',0,NULL,'\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 3\r\n### The error may exist in cn/fabhub/shopadmin/mapper/SysResourceMapper.xml\r\n### The error may involve defaultParameterMap\r\n### The error occurred while setting parameters\r\n### SQL: update sys_resource         set status = ?          WHERE id in\r\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 3\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 3','2020-11-16 16:53:23'),(7,'资源管理',3,'root','/sys/resource','cn.fabhub.shopadmin.controller.SysResourceController.delete','PUT','127.0.0.1',58068,'局域网','{\"id\":88,\"name\":\"测试狗\",\"openType\":3,\"orderNum\":1,\"parentId\":76,\"status\":0,\"type\":2,\"visible\":0}',1,NULL,NULL,'2020-11-16 16:57:16'),(8,'资源管理',3,'root','/sys/resource','cn.fabhub.shopadmin.controller.SysResourceController.delete','PUT','127.0.0.1',58084,'局域网','{\"icon\":\"tool\",\"id\":76,\"name\":\"开发工具\",\"openType\":1,\"orderNum\":500,\"parentId\":0,\"router\":\"/dev\",\"status\":0,\"type\":1,\"visible\":0}',1,NULL,NULL,'2020-11-16 16:57:27'),(9,'资源管理',3,'root','/sys/resource','cn.fabhub.shopadmin.controller.SysResourceController.delete','PUT','127.0.0.1',58098,'局域网','{\"icon\":\"tool\",\"id\":76,\"name\":\"开发工具\",\"openType\":1,\"orderNum\":500,\"parentId\":0,\"router\":\"/dev\",\"status\":1,\"type\":1,\"visible\":0}',1,NULL,NULL,'2020-11-16 16:57:33'),(10,'资源管理',3,'root','/sys/resource','cn.fabhub.shopadmin.controller.SysResourceController.delete','PUT','127.0.0.1',58267,'局域网','{\"id\":88,\"name\":\"测试狗\",\"openType\":3,\"orderNum\":1,\"parentId\":76,\"status\":0,\"type\":2,\"visible\":0}',1,NULL,NULL,'2020-11-16 17:01:06'),(11,'资源管理',3,'root','/sys/resource','cn.fabhub.shopadmin.controller.SysResourceController.delete','PUT','127.0.0.1',58283,'局域网','{\"icon\":\"tool\",\"id\":76,\"name\":\"开发工具\",\"openType\":1,\"orderNum\":500,\"parentId\":0,\"router\":\"/dev\",\"status\":0,\"type\":1,\"visible\":0}',1,NULL,NULL,'2020-11-16 17:01:17'),(12,'资源管理',3,'root','/sys/resource','cn.fabhub.shopadmin.controller.SysResourceController.delete','PUT','127.0.0.1',58297,'局域网','{\"icon\":\"tool\",\"id\":76,\"name\":\"开发工具\",\"openType\":1,\"orderNum\":500,\"parentId\":0,\"router\":\"/dev\",\"status\":1,\"type\":1,\"visible\":0}',1,NULL,NULL,'2020-11-16 17:01:21'),(13,'资源管理',3,'root','/sys/resource','cn.fabhub.shopadmin.controller.SysResourceController.delete','PUT','127.0.0.1',58316,'局域网','{\"icon\":\"api\",\"id\":77,\"name\":\"数据接口\",\"openType\":3,\"orderNum\":510,\"parentId\":76,\"router\":\"http://localhost:8080/doc.html\",\"status\":0,\"type\":2,\"visible\":0}',1,NULL,NULL,'2020-11-16 17:01:30'),(14,'资源管理',3,'root','/sys/resource','cn.fabhub.shopadmin.controller.SysResourceController.delete','PUT','127.0.0.1',58333,'局域网','{\"icon\":\"api\",\"id\":77,\"name\":\"数据接口\",\"openType\":3,\"orderNum\":510,\"parentId\":76,\"router\":\"http://localhost:8080/doc.html\",\"status\":1,\"type\":2,\"visible\":0}',1,NULL,NULL,'2020-11-16 17:01:37'),(15,'角色管理',3,'root','/sys/role','cn.fabhub.shopadmin.controller.SysRoleController.update','PUT','127.0.0.1',58366,'局域网','{\"id\":1,\"isDefault\":0,\"remark\":\"超级管理员\",\"resourceIds\":[75,41,70,71,55,79,72,73,74,80,42,35,57,58,34,37,43,51,59,36,47,48,49,50,38,52,54,53,40,60,68,69,76,77,81,84,85,82,83,3],\"roleName\":\"ROLE_ADMIN\",\"status\":1,\"zhName\":\"超级管理员\"}',1,NULL,NULL,'2020-11-16 17:01:51'),(16,'角色管理',3,'root','/sys/role','cn.fabhub.shopadmin.controller.SysRoleController.update','PUT','127.0.0.1',51214,'局域网','{\"id\":33,\"isDefault\":0,\"remark\":\"炮灰\",\"resourceIds\":[34,37,43,51,59,36,47,48,49,50,38,52,54,53,41,70,71,55,79,75,42,58,68,69,72,73,74,80,35,57],\"roleName\":\"ROLE_TEST\",\"status\":1,\"zhName\":\"测试用户狗\"}',1,NULL,NULL,'2020-11-17 17:19:57'),(17,'角色管理',3,'root','/sys/role','cn.fabhub.shopadmin.controller.SysRoleController.update','PUT','127.0.0.1',51226,'局域网','{\"id\":33,\"isDefault\":0,\"remark\":\"炮灰\",\"resourceIds\":[34,37,43,51,59,36,47,48,49,50,38,52,54,53,41,70,71,55,79,75,58,68,69,72,73,74,80],\"roleName\":\"ROLE_TEST\",\"status\":1,\"zhName\":\"测试用户狗\"}',1,NULL,NULL,'2020-11-17 17:20:01'),(18,'角色管理',3,'root','/sys/role','cn.fabhub.shopadmin.controller.SysRoleController.update','PUT','127.0.0.1',51247,'局域网','{\"id\":33,\"isDefault\":0,\"remark\":\"炮灰\",\"resourceIds\":[34,37,43,51,59,36,47,48,49,50,38,52,54,53,41,70,71,55,79,75,58,68,69,72,73,74,80,3,40,60,81,84,85,82,83],\"roleName\":\"ROLE_TEST\",\"status\":1,\"zhName\":\"测试用户狗\"}',1,NULL,NULL,'2020-11-17 17:20:10'),(19,'角色管理',3,'root','/sys/role','cn.fabhub.shopadmin.controller.SysRoleController.update','PUT','127.0.0.1',51271,'局域网','{\"id\":33,\"isDefault\":0,\"remark\":\"炮灰\",\"resourceIds\":[],\"roleName\":\"ROLE_TEST\",\"status\":1,\"zhName\":\"测试用户狗\"}',1,NULL,NULL,'2020-11-17 17:20:17'),(61,'字典数据',1,'root','/sys/dict/data','cn.fabhub.shopadmin.controller.SysDictDataController.save','POST','127.0.0.1',53349,'局域网','{\"color\":\"red\",\"dictLabel\":\"用户被锁\",\"dictType\":\"sys_user_status\",\"dictValue\":2,\"orderNum\":3,\"remark\":\"\",\"status\":1}',1,NULL,NULL,'2020-12-01 21:43:24'),(62,'字典数据',1,'root','/sys/dict/data','cn.fabhub.shopadmin.controller.SysDictDataController.save','POST','127.0.0.1',53079,'局域网','{\"color\":\"yellow\",\"dictLabel\":\"密码过期\",\"dictType\":\"sys_user_status\",\"dictValue\":3,\"orderNum\":3,\"remark\":\"\",\"status\":1}',1,NULL,NULL,'2020-12-01 21:43:55'),(60,'字典数据',3,'root','/sys/dict/data','cn.fabhub.shopadmin.controller.SysDictDataController.update','PUT','127.0.0.1',57110,'局域网','{\"id\":40,\"orderNum\":2}',1,NULL,NULL,'2020-12-01 21:36:52'),(54,'日志管理',2,'root','/sys/operlog/22,21,20','cn.fabhub.shopadmin.controller.SysAccessLogController.delete','DELETE','127.0.0.1',64820,'局域网','',1,NULL,NULL,'2020-12-01 21:23:18'),(55,'角色管理',1,'root','/sys/role','cn.fabhub.shopadmin.controller.SysRoleController.save','POST','127.0.0.1',57789,'局域网','{\"isDefault\":0,\"resourceIds\":[],\"roleName\":\"role_ad\",\"status\":1,\"zhName\":\"广告\"}',1,NULL,NULL,'2020-12-01 21:30:08'),(56,'角色管理',2,'root','/sys/role/37','cn.fabhub.shopadmin.controller.SysRoleController.deleteBatch','DELETE','127.0.0.1',57798,'局域网','',1,NULL,NULL,'2020-12-01 21:30:12'),(57,'字典管理',1,'root','/sys/dict','cn.fabhub.shopadmin.controller.SysDictTypeController.save','POST','127.0.0.1',57063,'局域网','{\"dictName\":\"用户状态\",\"dictType\":\"sys_user_status\",\"orderNum\":10,\"remark\":\"标识用户的状态\",\"status\":1}',1,NULL,NULL,'2020-12-01 21:35:53'),(58,'字典数据',1,'root','/sys/dict/data','cn.fabhub.shopadmin.controller.SysDictDataController.save','POST','127.0.0.1',57086,'局域网','{\"color\":\"green\",\"dictLabel\":\"正常\",\"dictType\":\"sys_user_status\",\"dictValue\":1,\"orderNum\":1,\"remark\":\"\",\"status\":1}',1,NULL,NULL,'2020-12-01 21:36:20'),(59,'字典数据',1,'root','/sys/dict/data','cn.fabhub.shopadmin.controller.SysDictDataController.save','POST','127.0.0.1',57096,'局域网','{\"color\":\"red\",\"dictLabel\":\"禁用\",\"dictType\":\"sys_user_status\",\"dictValue\":0,\"orderNum\":1,\"remark\":\"\",\"status\":1}',1,NULL,NULL,'2020-12-01 21:36:40'),(23,'用户管理',3,'root','/sys/user','cn.fabhub.shopadmin.controller.SysUserController.update','PUT','127.0.0.1',61549,'局域网','{\"email\":\"15151@qq.com\",\"id\":94,\"loginName\":\"sx\",\"password\":\"123456\",\"phone\":\"18111336833\",\"roleIds\":[2],\"sex\":1,\"status\":1,\"username\":\"帅哥\"}',1,NULL,NULL,'2020-11-17 17:47:51'),(24,'资源管理',1,'root','/sys/resource','cn.fabhub.shopadmin.controller.SysResourceController.save','POST','127.0.0.1',64805,'局域网','{\"component\":\"monitor/job/Index\",\"name\":\"任务管理\",\"openType\":1,\"orderNum\":3,\"parentId\":35,\"status\":1,\"type\":2}',1,NULL,NULL,'2020-11-18 18:19:34'),(25,'资源管理',3,'root','/sys/resource','cn.fabhub.shopadmin.controller.SysResourceController.delete','PUT','127.0.0.1',64822,'局域网','{\"component\":\"monitor/job/Index\",\"id\":89,\"name\":\"任务管理\",\"openType\":1,\"orderNum\":430,\"parentId\":35,\"status\":1,\"type\":2,\"visible\":0}',1,NULL,NULL,'2020-11-18 18:19:48'),(26,'资源管理',3,'root','/sys/resource','cn.fabhub.shopadmin.controller.SysResourceController.delete','PUT','127.0.0.1',64836,'局域网','{\"component\":\"monitor/job/Index\",\"id\":89,\"name\":\"任务管理\",\"openType\":1,\"orderNum\":430,\"parentId\":35,\"router\":\"/job\",\"status\":1,\"type\":2,\"visible\":0}',1,NULL,NULL,'2020-11-18 18:20:02'),(27,'角色管理',3,'root','/sys/role','cn.fabhub.shopadmin.controller.SysRoleController.update','PUT','127.0.0.1',64878,'局域网','{\"id\":1,\"isDefault\":0,\"remark\":\"超级管理员\",\"resourceIds\":[3,34,37,43,51,59,36,47,48,49,50,38,52,54,53,40,60,68,69,81,84,85,82,83,41,70,71,55,79,75,58,72,73,74,80],\"roleName\":\"ROLE_ADMIN\",\"status\":1,\"zhName\":\"超级管理员\"}',1,NULL,NULL,'2020-11-18 18:20:23'),(65,'字典数据',3,'root','/sys/dict/data','cn.fabhub.shopadmin.controller.SysDictDataController.update','PUT','127.0.0.1',53141,'局域网','{\"id\":42,\"orderNum\":4}',1,NULL,NULL,'2020-12-01 21:45:44'),(66,'字典数据',1,'root','/sys/dict/data','cn.fabhub.shopadmin.controller.SysDictDataController.save','POST','127.0.0.1',53203,'局域网','{\"color\":\"red\",\"dictLabel\":\"密码过期\",\"dictType\":\"sys_user_status\",\"dictValue\":4,\"orderNum\":5,\"remark\":\"\",\"status\":1}',1,NULL,NULL,'2020-12-01 21:46:46'),(63,'字典数据',3,'root','/sys/dict/data','cn.fabhub.shopadmin.controller.SysDictDataController.update','PUT','127.0.0.1',53090,'局域网','{\"color\":\"yellow\",\"dictLabel\":\"密码过期\",\"dictType\":\"sys_user_status\",\"dictValue\":2,\"id\":43,\"orderNum\":3,\"status\":1}',1,NULL,NULL,'2020-12-01 21:44:08'),(64,'字典数据',3,'root','/sys/dict/data','cn.fabhub.shopadmin.controller.SysDictDataController.update','PUT','127.0.0.1',53104,'局域网','{\"color\":\"red\",\"dictLabel\":\"用户被锁\",\"dictType\":\"sys_user_status\",\"dictValue\":3,\"id\":42,\"orderNum\":3,\"status\":1}',1,NULL,NULL,'2020-12-01 21:44:13'),(52,'日志管理',2,'root','/sys/operlog/31,30','cn.fabhub.shopadmin.controller.SysAccessLogController.delete','DELETE','127.0.0.1',64797,'局域网','',1,NULL,NULL,'2020-12-01 21:23:08'),(53,'日志管理',2,'root','/sys/operlog/32,29,28','cn.fabhub.shopadmin.controller.SysAccessLogController.delete','DELETE','127.0.0.1',64807,'局域网','',1,NULL,NULL,'2020-12-01 21:23:12'),(33,'角色管理',3,'root','/sys/role','cn.fabhub.shopadmin.controller.SysRoleController.update','PUT','127.0.0.1',57308,'局域网','{\"id\":1,\"isDefault\":0,\"remark\":\"超级管理员\",\"resourceIds\":[3,34,37,43,51,59,36,47,48,49,50,38,52,54,53,40,60,68,69,81,84,85,82,83,41,70,71,55,79,75,58,72,73,74,80],\"roleName\":\"ROLE_ADMIN\",\"status\":1,\"zhName\":\"超级管理员\"}',1,NULL,NULL,'2020-11-18 19:11:56'),(34,'角色管理',3,'root','/sys/role','cn.fabhub.shopadmin.controller.SysRoleController.update','PUT','127.0.0.1',57321,'局域网','{\"id\":1,\"isDefault\":0,\"remark\":\"超级管理员\",\"resourceIds\":[3,34,37,43,51,59,36,47,48,49,50,38,52,54,53,40,60,68,69,81,84,85,82,83,41,70,71,55,79,75,58,72,73,74,80,35,42,57,89,91,90,92],\"roleName\":\"ROLE_ADMIN\",\"status\":1,\"zhName\":\"超级管理员\"}',1,NULL,NULL,'2020-11-18 19:12:00'),(35,'资源管理',1,'root','/sys/resource','cn.fabhub.shopadmin.controller.SysResourceController.save','POST','127.0.0.1',57779,'局域网','{\"name\":\"任务添加\",\"openType\":1,\"orderNum\":1,\"parentId\":89,\"permission\":\"sys:job:add\",\"status\":1,\"type\":3}',1,NULL,NULL,'2020-11-18 19:17:09'),(36,'资源管理',1,'root','/sys/resource','cn.fabhub.shopadmin.controller.SysResourceController.save','POST','127.0.0.1',57822,'局域网','{\"name\":\"任务更新\",\"openType\":1,\"orderNum\":1,\"parentId\":89,\"permission\":\"sys:job:update\",\"status\":1,\"type\":3}',1,NULL,NULL,'2020-11-18 19:17:31'),(37,'资源管理',1,'root','/sys/resource','cn.fabhub.shopadmin.controller.SysResourceController.save','POST','127.0.0.1',57856,'局域网','{\"name\":\"任务删除\",\"openType\":1,\"orderNum\":1,\"parentId\":89,\"permission\":\"sys:job:delete\",\"status\":1,\"type\":3}',1,NULL,NULL,'2020-11-18 19:17:53'),(38,'角色管理',3,'root','/sys/role','cn.fabhub.shopadmin.controller.SysRoleController.update','PUT','127.0.0.1',58337,'局域网','{\"id\":1,\"isDefault\":0,\"remark\":\"超级管理员\",\"resourceIds\":[3,34,37,43,51,59,36,47,48,49,50,38,52,54,53,40,60,68,69,81,84,85,82,83,41,70,71,55,79,75,58,72,73,74,80],\"roleName\":\"ROLE_ADMIN\",\"status\":1,\"zhName\":\"超级管理员\"}',1,NULL,NULL,'2020-11-18 19:23:02'),(39,'角色管理',3,'root','/sys/role','cn.fabhub.shopadmin.controller.SysRoleController.update','PUT','127.0.0.1',58354,'局域网','{\"id\":1,\"isDefault\":0,\"remark\":\"超级管理员\",\"resourceIds\":[3,34,37,43,51,59,36,47,48,49,50,38,52,54,53,40,60,68,69,81,84,85,82,83,41,70,71,55,79,75,58,72,73,74,80,35,42,57,89,94,95,90,91,92,93],\"roleName\":\"ROLE_ADMIN\",\"status\":1,\"zhName\":\"超级管理员\"}',1,NULL,NULL,'2020-11-18 19:23:06'),(40,'字典管理',1,'root','/sys/dict','cn.fabhub.shopadmin.controller.SysDictTypeController.save','POST','127.0.0.1',58594,'局域网','{\"dictName\":\"任务状态\",\"dictType\":\"sys_job_status\",\"orderNum\":12,\"remark\":\"描述定时任务的几个状态\",\"status\":1}',1,NULL,NULL,'2020-11-18 19:26:33'),(41,'字典数据',1,'root','/sys/dict/data','cn.fabhub.shopadmin.controller.SysDictDataController.save','POST','127.0.0.1',58710,'局域网','{\"color\":\"red\",\"dictLabel\":\"禁用\",\"dictType\":\"sys_job_status\",\"dictValue\":0,\"orderNum\":1,\"remark\":\"\",\"status\":1}',1,NULL,NULL,'2020-11-18 19:30:45'),(42,'字典数据',1,'root','/sys/dict/data','cn.fabhub.shopadmin.controller.SysDictDataController.save','POST','127.0.0.1',58720,'局域网','{\"color\":\"green\",\"dictLabel\":\"等待中\",\"dictType\":\"sys_job_status\",\"dictValue\":1,\"orderNum\":2,\"remark\":\"\",\"status\":1}',1,NULL,NULL,'2020-11-18 19:31:03'),(43,'字典数据',1,'root','/sys/dict/data','cn.fabhub.shopadmin.controller.SysDictDataController.save','POST','127.0.0.1',58732,'局域网','{\"color\":\"yellow\",\"dictLabel\":\"运行中\",\"dictType\":\"sys_job_status\",\"dictValue\":2,\"orderNum\":1,\"remark\":\"\",\"status\":1}',1,NULL,NULL,'2020-11-18 19:31:24'),(44,'字典数据',1,'root','/sys/dict/data','cn.fabhub.shopadmin.controller.SysDictDataController.save','POST','127.0.0.1',58755,'局域网','{\"color\":\"blue\",\"dictLabel\":\"暂停中\",\"dictType\":\"sys_job_status\",\"dictValue\":3,\"orderNum\":4,\"remark\":\"\",\"status\":1}',1,NULL,NULL,'2020-11-18 19:31:55'),(45,'字典数据',1,'root','/sys/dict/data','cn.fabhub.shopadmin.controller.SysDictDataController.save','POST','127.0.0.1',58776,'局域网','{\"color\":\"green\",\"dictLabel\":\"已完成\",\"dictType\":\"sys_job_status\",\"dictValue\":4,\"orderNum\":5,\"remark\":\"\",\"status\":1}',1,NULL,NULL,'2020-11-18 19:32:31'),(46,'字典数据',3,'root','/sys/dict/data','cn.fabhub.shopadmin.controller.SysDictDataController.update','PUT','127.0.0.1',58784,'局域网','{\"id\":36,\"orderNum\":1}',1,NULL,NULL,'2020-11-18 19:32:44'),(47,'字典数据',3,'root','/sys/dict/data','cn.fabhub.shopadmin.controller.SysDictDataController.update','PUT','127.0.0.1',58789,'局域网','{\"id\":36,\"orderNum\":2}',1,NULL,NULL,'2020-11-18 19:32:49'),(48,'字典数据',3,'root','/sys/dict/data','cn.fabhub.shopadmin.controller.SysDictDataController.update','PUT','127.0.0.1',58791,'局域网','{\"id\":37,\"orderNum\":3}',1,NULL,NULL,'2020-11-18 19:32:53'),(49,'用户管理',3,'root','/sys/user','cn.fabhub.shopadmin.controller.SysUserController.update','PUT','127.0.0.1',65506,'局域网','{\"avatar\":\"http://m-zhou.oss-cn-chengdu.aliyuncs.com/header.jpg\",\"email\":\"root@qq.com\",\"id\":1,\"loginName\":\"root\",\"password\":\"123456\",\"phone\":\"18112345678\",\"roleIds\":[1],\"sex\":1,\"status\":1,\"username\":\"管理员\"}',1,NULL,NULL,'2020-11-25 18:20:26'),(50,'登录日志',2,'root','/monitor/loginlog/34,33','cn.fabhub.shopadmin.controller.SysLoginLogController.delete','DELETE','127.0.0.1',65530,'局域网','',1,NULL,NULL,'2020-11-25 18:20:39'),(51,'登录日志',2,'root','/monitor/loginlog/34,33,35,32,31','cn.fabhub.shopadmin.controller.SysLoginLogController.delete','DELETE','127.0.0.1',49152,'局域网','',1,NULL,NULL,'2020-11-25 18:20:45'),(67,'字典数据',3,'root','/sys/dict/data','cn.fabhub.shopadmin.controller.SysDictDataController.update','PUT','127.0.0.1',53221,'局域网','{\"color\":\"red\",\"dictLabel\":\"认证过期\",\"dictType\":\"sys_user_status\",\"dictValue\":4,\"id\":44,\"orderNum\":5,\"status\":1}',1,NULL,NULL,'2020-12-01 21:47:04'),(68,'字典管理',3,'root','/sys/dict','cn.fabhub.shopadmin.controller.SysDictTypeController.update','PUT','127.0.0.1',58131,'局域网','{\"dictName\":\"存储仓库\",\"dictType\":\"sys_storage\",\"id\":14,\"orderNum\":9,\"remark\":\"存储仓库\",\"status\":1}',1,NULL,NULL,'2020-12-01 21:49:48'),(69,'字典管理',1,'root','/sys/dict','cn.fabhub.shopadmin.controller.SysDictTypeController.save','POST','127.0.0.1',54712,'局域网','{\"dictName\":\"系统状态\",\"dictType\":\"sys_status\",\"orderNum\":14,\"status\":1}',1,NULL,NULL,'2020-12-01 21:54:21'),(70,'角色管理',3,'root','/sys/role','cn.fabhub.shopadmin.controller.SysRoleController.update','PUT','127.0.0.1',58999,'局域网','{\"id\":2,\"isDefault\":1,\"remark\":\"普通用户\",\"resourceIds\":[3,34,36,37,38,40,43,47,48,49,50,51,52,53,54,58,59,60,68,69,81,82,83,84,85],\"roleName\":\"ROLE_USER\",\"status\":1,\"zhName\":\"普通用户\"}',1,NULL,NULL,'2020-12-01 22:09:07'),(71,'字典管理',2,'root','/sys/dict/17','cn.fabhub.shopadmin.controller.SysDictTypeController.delete','DELETE','127.0.0.1',53569,'局域网','',1,NULL,NULL,'2020-12-01 23:06:07'),(72,'字典管理',1,'root','/sys/dict','cn.fabhub.shopadmin.controller.SysDictTypeController.save','POST','127.0.0.1',61506,'局域网','{\"dictName\":\"测试状态\",\"dictType\":\"sys_test\",\"orderNum\":4,\"status\":1}',1,NULL,NULL,'2020-12-01 23:32:57'),(73,'字典数据',1,'root','/sys/dict/data','cn.fabhub.shopadmin.controller.SysDictDataController.save','POST','127.0.0.1',61527,'局域网','{\"dictLabel\":\"g\",\"dictType\":\"sys_test\",\"dictValue\":1,\"orderNum\":1,\"remark\":\"\",\"status\":1}',1,NULL,NULL,'2020-12-01 23:33:09'),(74,'字典管理',2,'root','/sys/dict/18','cn.fabhub.shopadmin.controller.SysDictTypeController.delete','DELETE','127.0.0.1',61540,'局域网','',0,NULL,'\r\n### Error updating database.  Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`shop`.`sys_dict_data`, CONSTRAINT `sys_dict_data_ibfk_1` FOREIGN KEY (`dict_type`) REFERENCES `sys_dict_type` (`dict_type`))\r\n### The error may exist in cn/fabhub/shopadmin/mapper/SysDictTypeMapper.java (best guess)\r\n### The error may involve defaultParameterMap\r\n### The error occurred while setting parameters\r\n### SQL: DELETE FROM sys_dict_type WHERE id IN (   ?  )\r\n### Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`shop`.`sys_dict_data`, CONSTRAINT `sys_dict_data_ibfk_1` FOREIGN KEY (`dict_type`) REFERENCES `sys_dict_type` (`dict_type`))\n; Cannot delete or update a parent row: a foreign key constraint fails (`shop`.`sys_dict_data`, CONSTRAINT `sys_dict_data_ibfk_1` FOREIGN KEY (`dict_type`) REFERENCES `sys_dict_type` (`dict_type`)); nested exception is java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`shop`.`sys_dict_data`, CONSTRAINT `sys_dict_data_ibfk_1` FOREIGN KEY (`dict_type`) REFERENCES `sys_dict_type` (`dict_type`))','2020-12-01 23:33:15'),(75,'文件管理',2,'root','/file/23','cn.fabhub.shopadmin.controller.SysFileInfoController.delete','DELETE','127.0.0.1',61668,'局域网','',1,NULL,NULL,'2020-12-01 23:34:53');
/*!40000 ALTER TABLE `sys_access_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dict_data`
--

DROP TABLE IF EXISTS `sys_dict_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_dict_data` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '字典数据编号',
  `dict_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典数据类型',
  `dict_label` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '字典标签',
  `dict_value` int NOT NULL COMMENT '字典键值',
  `color` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT 'green' COMMENT '颜色',
  `order_num` int DEFAULT NULL COMMENT '排序',
  `status` int NOT NULL DEFAULT '1' COMMENT '状态(0 禁用，1可用）',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '备注',
  `create_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `dict_type` (`dict_type`),
  CONSTRAINT `sys_dict_data_ibfk_1` FOREIGN KEY (`dict_type`) REFERENCES `sys_dict_type` (`dict_type`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='字典数据';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict_data`
--

LOCK TABLES `sys_dict_data` WRITE;
/*!40000 ALTER TABLE `sys_dict_data` DISABLE KEYS */;
INSERT INTO `sys_dict_data` VALUES (1,'sys_user_sex','帅哥',1,'blue',1,1,'性别男','2020-08-19 23:51:44','2020-11-11 19:33:46'),(2,'sys_user_sex','美女',2,'pink',2,1,'性别女','2020-08-20 10:26:41','2020-08-31 21:35:08'),(3,'sys_user_sex','保密',3,'black',3,1,'保密','2020-08-20 10:27:19','2020-08-26 22:13:22'),(4,'sys_user_sex','未知',4,'yellow',4,1,'未说明性别','2020-08-20 10:27:48','2020-11-06 17:09:29'),(5,'sys_oper_type','未知',0,'green',1,1,'未知操作，默认','2020-08-20 10:30:19','2020-11-11 19:25:09'),(6,'sys_oper_type','添加',1,'blue',2,1,'添加数据','2020-08-20 10:30:55','2020-08-26 18:58:50'),(7,'sys_oper_type','删除',2,'red',3,1,'删除数据','2020-08-20 10:31:21','2020-08-26 18:58:50'),(8,'sys_oper_type','修改',3,'#faad14',4,1,'修改数据','2020-08-20 10:31:39','2020-08-26 18:58:50'),(9,'sys_oper_type','查询',4,'green',5,1,'查询数据','2020-08-20 10:32:14','2020-08-26 18:58:50'),(10,'sys_oper_status','成功',1,'green',1,1,'系统运行正常','2020-08-20 11:31:55','2020-08-26 18:56:53'),(11,'sys_oper_status','失败',0,'red',2,1,'系统运行异常','2020-08-20 11:32:29','2020-08-26 19:03:32'),(12,'sys_resource_type','目录',1,'green',1,1,'','2020-08-20 12:38:41','2020-08-26 16:25:07'),(13,'sys_resource_type','菜单',2,'green',2,1,'','2020-08-20 12:38:51','2020-08-31 19:46:00'),(14,'sys_resource_type','按钮',3,'green',3,1,'','2020-08-20 12:39:10','2020-08-26 16:25:07'),(15,'sys_oper_type','清空',9,'red',10,1,'清空数据','2020-08-26 17:04:52','2020-11-11 17:21:59'),(16,'sys_row_disable','正常',1,'green',1,1,'','2020-08-26 17:28:45','2020-08-26 17:28:45'),(17,'sys_row_disable','禁用',0,'red',2,1,'','2020-08-26 17:28:59','2020-08-31 15:18:52'),(18,'sys_is_default','默认',1,'blue',1,1,'','2020-08-26 22:54:27','2020-08-26 22:54:27'),(19,'sys_is_default','非默认',0,'purple',2,1,'','2020-08-26 22:55:46','2020-08-26 22:55:46'),(22,'sys_yes_or_no','是',1,'blue',1,1,'','2020-08-31 19:48:48','2020-08-31 19:48:48'),(23,'sys_yes_or_no','否',0,'red',2,1,'','2020-08-31 19:49:08','2020-08-31 19:49:08'),(24,'sys_open_type','组件',1,'green',1,1,'','2020-09-14 13:47:01','2020-09-14 13:47:01'),(25,'sys_open_type','内链接',2,'green',2,0,'','2020-09-14 13:47:32','2020-09-14 16:20:13'),(26,'sys_open_type','外链接',3,'green',3,1,'','2020-09-14 13:47:43','2020-09-14 13:47:43'),(27,'sys_oper_type','授权',5,'green',6,1,'','2020-11-11 17:51:12','2020-11-11 19:25:28'),(28,'sys_oper_type','导入',6,'green',7,1,'','2020-11-11 17:51:44','2020-11-11 19:25:26'),(29,'sys_oper_type','导出',7,'green',8,1,'','2020-11-11 17:52:26','2020-11-11 19:25:20'),(30,'sys_oper_type','扫描',8,'green',9,1,'执行系统里面的操作','2020-11-11 17:53:20','2020-11-11 19:25:16'),(33,'sys_storage','阿里云',1,'green',1,1,'阿里云对象存储服务','2020-11-14 18:00:54','2020-11-14 18:01:05'),(34,'sys_storage','自建系统',2,'blue',2,1,'自己搭建的文件服务系统','2020-11-14 18:01:42','2020-11-14 18:01:42'),(35,'sys_job_status','禁用',0,'red',1,1,'','2020-11-18 19:30:45','2020-11-18 19:30:45'),(36,'sys_job_status','等待中',1,'green',2,1,'','2020-11-18 19:31:02','2020-11-18 19:32:49'),(37,'sys_job_status','运行中',2,'yellow',3,1,'','2020-11-18 19:31:24','2020-11-18 19:32:52'),(38,'sys_job_status','暂停中',3,'blue',4,1,'','2020-11-18 19:31:54','2020-11-18 19:31:54'),(39,'sys_job_status','已完成',4,'green',5,1,'','2020-11-18 19:32:30','2020-11-18 19:32:30'),(40,'sys_user_status','正常',1,'green',2,1,'','2020-12-01 21:36:19','2020-12-01 21:36:51'),(41,'sys_user_status','禁用',0,'red',1,1,'','2020-12-01 21:36:39','2020-12-01 21:36:39'),(42,'sys_user_status','用户被锁',3,'red',4,1,'','2020-12-01 21:43:24','2020-12-01 21:45:43'),(43,'sys_user_status','密码过期',2,'yellow',3,1,'','2020-12-01 21:43:55','2020-12-01 21:44:08'),(44,'sys_user_status','认证过期',4,'red',5,1,'','2020-12-01 21:46:45','2020-12-01 21:47:04'),(45,'sys_test','g',1,'green',1,1,'','2020-12-01 23:33:09','2020-12-01 23:33:09');
/*!40000 ALTER TABLE `sys_dict_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dict_type`
--

DROP TABLE IF EXISTS `sys_dict_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_dict_type` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '字典id',
  `dict_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典名称',
  `dict_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典编码',
  `order_num` int DEFAULT '1' COMMENT '排序',
  `status` int NOT NULL DEFAULT '1' COMMENT '状态(0禁用，1可用）',
  `remark` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '字典的描述',
  `create_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `sys_dict_type_dict_type_uindex` (`dict_type`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='基础字典';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict_type`
--

LOCK TABLES `sys_dict_type` WRITE;
/*!40000 ALTER TABLE `sys_dict_type` DISABLE KEYS */;
INSERT INTO `sys_dict_type` VALUES (4,'用户性别','sys_user_sex',1,1,'用户性别','2020-07-28 15:13:24','2020-11-09 13:32:58'),(6,'请求结果反馈','sys_oper_status',6,1,'描述请求是否成功','2020-08-19 21:44:08','2020-11-09 16:03:22'),(7,'操作类型','sys_oper_type',3,1,'记录操作日志的业务类型','2020-08-19 22:34:58','2020-11-07 22:24:38'),(8,'资源类型','sys_resource_type',4,1,'描述资源的类型','2020-08-20 12:38:00','2020-11-07 22:24:51'),(9,'数据行是否可用','sys_row_disable',8,1,'数据是否可用','2020-08-26 17:19:23','2020-11-09 16:03:29'),(10,'是否默认','sys_is_default',5,1,'系统某些数据是否是默认','2020-08-26 22:52:07','2020-11-07 22:24:52'),(12,'通用是否','sys_yes_or_no',2,1,'数据是否可用','2020-08-31 19:48:24','2020-11-08 23:03:20'),(13,'菜单打开方式','sys_open_type',7,1,'菜单为外连接还是组件','2020-09-14 13:46:23','2020-11-11 22:33:53'),(14,'存储仓库','sys_storage',9,1,'存储仓库','2020-11-14 18:00:29','2020-12-01 21:49:47'),(15,'任务状态','sys_job_status',12,1,'描述定时任务的几个状态','2020-11-18 19:26:32','2020-11-18 19:26:32'),(16,'用户状态','sys_user_status',10,1,'标识用户的状态','2020-12-01 21:35:52','2020-12-01 21:35:52'),(18,'测试状态','sys_test',4,1,NULL,'2020-12-01 23:32:57','2020-12-01 23:32:57');
/*!40000 ALTER TABLE `sys_dict_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_file_info`
--

DROP TABLE IF EXISTS `sys_file_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_file_info` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `filename` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '文件名称',
  `file_size` bigint DEFAULT '0' COMMENT '文件大小',
  `type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '文件类型',
  `storage` int DEFAULT NULL COMMENT '文件存放类型：阿里云、本地、七牛云',
  `access_path` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '访问路径',
  `create_by` bigint DEFAULT NULL COMMENT '谁创建',
  `update_by` bigint DEFAULT NULL COMMENT '更新者',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='文件存储';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_file_info`
--

LOCK TABLES `sys_file_info` WRITE;
/*!40000 ALTER TABLE `sys_file_info` DISABLE KEYS */;
INSERT INTO `sys_file_info` VALUES (22,'header.jpg',38527,'image/jpeg',1,'http://m-zhou.oss-cn-chengdu.aliyuncs.com/header.jpg',1,1,'2020-11-14 17:53:49','2020-11-14 17:53:49');
/*!40000 ALTER TABLE `sys_file_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_job`
--

DROP TABLE IF EXISTS `sys_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_job` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `job_name` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务分组',
  `job_class_name` varchar(100) COLLATE utf8mb4_general_ci NOT NULL COMMENT '执行类',
  `cron_expression` varchar(100) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'cron表达式',
  `trigger_state` varchar(15) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '任务状态',
  `status` int NOT NULL DEFAULT '0' COMMENT '任务状态',
  `old_job_name` varchar(50) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '修改之前的任务名称',
  `old_job_group` varchar(50) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '修改之前的任务分组',
  `description` varchar(100) COLLATE utf8mb4_general_ci NOT NULL COMMENT '描述',
  PRIMARY KEY (`id`),
  UNIQUE KEY `un_group_name` (`job_group`,`job_name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_job`
--

LOCK TABLES `sys_job` WRITE;
/*!40000 ALTER TABLE `sys_job` DISABLE KEYS */;
INSERT INTO `sys_job` VALUES (1,'小测试','测试组','cn.fabhub.shopadmin.quartz.job.TestDynamicJob','*/20 * * * * ?',NULL,3,'','','测试用例'),(2,'疫情自动填报','爬虫','cn.fabhub.shopadmin.quartz.job.AutoFillNPU','0 /1 * * * ?',NULL,3,'','','疫情自动填报'),(4,'线程测试','测试组','cn.fabhub.shopadmin.quartz.job.TestJob','*/3 * * * * ?',NULL,3,'','','测试线程'),(5,'邮件测试','测试组','cn.fabhub.shopadmin.quartz.job.SendMail','0 /1 * * * ?',NULL,3,'','','测试');
/*!40000 ALTER TABLE `sys_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_login_log`
--

DROP TABLE IF EXISTS `sys_login_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_login_log` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_id` bigint DEFAULT NULL COMMENT '用户id',
  `login_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户账号',
  `login_ip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '操作系统',
  `status` int NOT NULL DEFAULT '0' COMMENT '登录状态（0失败,成功 1）',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '提示消息',
  `login_time` datetime DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='系统访问记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_login_log`
--

LOCK TABLES `sys_login_log` WRITE;
/*!40000 ALTER TABLE `sys_login_log` DISABLE KEYS */;
INSERT INTO `sys_login_log` VALUES (1,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-06 18:08:46'),(2,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-06 18:10:06'),(3,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-06 20:14:40'),(4,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-07 21:10:53'),(5,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-07 23:11:07'),(6,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-08 12:52:24'),(7,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-08 22:55:49'),(8,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-09 13:31:25'),(9,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-09 15:55:58'),(10,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-11 15:54:23'),(11,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-11 17:54:25'),(12,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-11 19:54:31'),(13,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-11 19:54:43'),(14,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-11 19:54:44'),(15,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-11 21:55:55'),(16,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-12 11:10:37'),(17,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-12 13:23:01'),(18,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-12 18:07:10'),(19,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-12 20:15:39'),(20,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-12 22:23:54'),(25,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-14 17:50:43'),(26,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-16 16:51:44'),(27,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-16 18:08:42'),(28,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-16 21:39:32'),(29,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-17 17:47:25'),(30,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-11-18 18:18:48'),(36,1,'root','127.0.0.1','局域网','Chrome 8','Windows 10',1,NULL,'2020-12-01 21:06:00');
/*!40000 ALTER TABLE `sys_login_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_resource`
--

DROP TABLE IF EXISTS `sys_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_resource` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `parent_id` bigint NOT NULL COMMENT '父id',
  `type` int NOT NULL DEFAULT '1' COMMENT '资源类型(1：目录，2：菜单，3按钮）',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '资源名称',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '图标',
  `component` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '组件路径',
  `router` varchar(120) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '路由地址',
  `permission` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '权限编码',
  `open_type` int NOT NULL DEFAULT '1' COMMENT '打开方式：1组件，2内链，3外链',
  `visible` int NOT NULL DEFAULT '0' COMMENT '是否可见',
  `redirect` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '重定向',
  `order_num` int NOT NULL DEFAULT '1' COMMENT '排序编号',
  `status` int NOT NULL DEFAULT '1' COMMENT '状态(1可用，0禁用）',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sys_permission_code_uindex` (`permission`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='权限';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_resource`
--

LOCK TABLES `sys_resource` WRITE;
/*!40000 ALTER TABLE `sys_resource` DISABLE KEYS */;
INSERT INTO `sys_resource` VALUES (3,0,1,'系统管理','setting',NULL,'/sys',NULL,1,2,NULL,200,1,'2020-07-26 12:09:45','2020-09-14 12:32:53'),(34,3,2,'用户管理','user','system/user/Index','/user',NULL,1,2,NULL,210,1,'2020-07-26 16:58:17','2020-11-11 22:35:32'),(35,0,1,'系统监控','desktop',NULL,'/monitor',NULL,1,1,NULL,400,1,'2020-07-26 17:45:34','2020-09-14 12:32:53'),(36,3,2,'角色管理','team','system/role/Index','/role',NULL,1,1,NULL,220,1,'2020-07-26 18:17:37','2020-09-14 12:32:53'),(37,34,3,'添加用户',NULL,NULL,NULL,'sys:user:add',1,1,NULL,1,1,'2020-07-26 18:35:45','2020-11-11 22:35:32'),(38,3,2,'资源管理','apartment','system/resource/Index','/resource',NULL,1,1,NULL,230,1,'2020-07-26 18:36:32','2020-09-14 12:32:53'),(40,3,2,'字典管理','book','system/dict/Index','/dict',NULL,1,1,NULL,240,1,'2020-07-26 18:38:46','2020-09-14 12:32:53'),(41,75,2,'操作日志管理','read','system/operlog/Index','/operlog',NULL,1,1,NULL,350,1,'2020-07-26 18:39:42','2020-09-14 14:39:19'),(42,35,2,'服务监控','database','monitor/server/Index','/server',NULL,1,1,NULL,410,1,'2020-07-26 19:41:13','2020-09-14 12:32:53'),(43,34,3,'删除用户',NULL,NULL,NULL,'sys:user:delete',1,1,NULL,2,1,'2020-07-26 21:12:35','2020-11-11 22:35:32'),(47,36,3,'添加角色',NULL,NULL,NULL,'sys:role:add',1,1,NULL,1,1,'2020-07-27 15:37:23','2020-09-14 12:32:53'),(48,36,3,'删除角色',NULL,NULL,NULL,'sys:role:delete',1,1,NULL,2,1,'2020-07-27 15:40:16','2020-09-14 12:32:53'),(49,36,3,'修改角色',NULL,NULL,NULL,'sys:role:update',1,1,NULL,3,1,'2020-07-27 15:40:25','2020-09-14 12:32:53'),(50,36,3,'获得角色',NULL,NULL,NULL,'sys:role:get',1,1,NULL,4,1,'2020-07-27 15:40:53','2020-09-14 12:32:53'),(51,34,3,'修改用户',NULL,NULL,NULL,'sys:user:update',1,1,NULL,3,1,'2020-07-27 15:45:10','2020-11-11 22:35:32'),(52,38,3,'添加资源',NULL,NULL,NULL,'sys:resource:add',1,1,NULL,1,1,'2020-07-27 15:45:54','2020-09-14 12:32:53'),(53,38,3,'修改资源',NULL,NULL,NULL,'sys:resource:update',1,1,NULL,3,1,'2020-07-27 15:46:03','2020-09-14 12:32:53'),(54,38,3,'删除资源',NULL,NULL,NULL,'sys:resource:delete',1,1,NULL,2,1,'2020-07-27 15:46:13','2020-09-14 12:32:53'),(55,41,3,'获得操作日志',NULL,NULL,NULL,'sys:operlog:get',1,1,NULL,3,1,'2020-07-27 22:29:49','2020-09-14 14:39:19'),(57,35,2,'数据监控','book','monitor/datasource/Index','http://localhost:8080/druid/datasource.html',NULL,3,1,NULL,420,1,'2020-07-27 22:55:49','2020-11-11 17:06:07'),(58,0,1,'首页','home',NULL,'/index',NULL,1,1,NULL,100,1,'2020-07-28 11:47:38','2020-09-14 12:32:53'),(59,34,3,'获得用户',NULL,NULL,NULL,'sys:user:list',1,1,NULL,4,1,'2020-07-28 18:16:20','2020-11-11 22:35:32'),(60,40,3,'添加字典',NULL,NULL,NULL,'sys:dict:add',1,1,NULL,1,1,'2020-08-10 15:31:47','2020-09-14 12:32:53'),(68,40,3,'字典删除',NULL,NULL,NULL,'sys:dict:delete',1,1,NULL,2,1,'2020-09-04 09:51:46','2020-09-14 12:32:53'),(69,40,3,'字典修改',NULL,NULL,NULL,'sys:dict:update',1,1,NULL,3,1,'2020-09-04 09:52:09','2020-09-14 12:32:53'),(70,41,3,'删除操作日志',NULL,NULL,NULL,'sys:operlog:delete',1,1,NULL,1,1,'2020-09-04 09:56:40','2020-09-14 14:39:19'),(71,41,3,'清空操作日志',NULL,NULL,NULL,'sys:operlog:truncate',1,1,NULL,2,1,'2020-09-04 09:58:02','2020-09-14 14:39:19'),(72,75,2,'登录日志管理','read','system/loginlog/Index','/loginlog',NULL,1,1,NULL,360,1,'2020-09-04 09:58:54','2020-09-14 14:39:19'),(73,72,3,'删除登录日志',NULL,NULL,NULL,'sys:loginlog:delete',1,1,NULL,1,1,'2020-09-04 09:59:27','2020-09-14 14:39:19'),(74,72,3,'清空登录日志',NULL,NULL,NULL,'sys:loginlog:truncate',1,1,NULL,2,1,'2020-09-04 10:00:06','2020-11-06 17:13:29'),(75,0,1,'系统日志','read','','/log',NULL,1,0,NULL,300,1,'2020-09-12 17:25:35','2020-09-14 14:39:19'),(76,0,1,'开发工具','tool','','/dev',NULL,1,0,NULL,500,1,'2020-09-14 10:58:29','2020-11-16 17:01:21'),(77,76,2,'数据接口','api',NULL,'http://localhost:8080/doc.html',NULL,3,0,NULL,510,1,'2020-09-14 11:00:42','2020-11-16 17:01:36'),(79,41,3,'导出操作日志',NULL,NULL,NULL,'sys:operlog:export',1,0,NULL,4,1,'2020-10-21 11:09:45','2020-10-21 11:09:45'),(80,72,3,'导出登录日志',NULL,NULL,NULL,'sys:loginlog:export',1,0,NULL,3,1,'2020-10-21 11:10:18','2020-10-21 11:10:18'),(81,3,2,'文件管理','file','system/fileinfo/Index','/file',NULL,1,0,NULL,250,1,'2020-11-13 16:35:32','2020-11-13 16:51:08'),(82,81,3,'获得文件列表',NULL,NULL,NULL,NULL,1,0,NULL,4,1,'2020-11-13 16:37:12','2020-11-13 21:59:01'),(83,81,3,'文件下载',NULL,NULL,NULL,'sys:file:download',1,0,NULL,5,1,'2020-11-13 21:55:46','2020-11-13 21:59:10'),(84,81,3,'文件上传',NULL,NULL,NULL,'sys:file:upload',1,0,NULL,1,1,'2020-11-13 21:56:21','2020-11-13 21:56:21'),(85,81,3,'文件删除',NULL,NULL,NULL,'sys:file:delete',1,0,NULL,2,1,'2020-11-13 21:58:19','2020-11-13 21:58:40'),(88,76,2,'测试狗',NULL,NULL,NULL,NULL,3,0,NULL,1,1,'2020-11-14 15:20:51','2020-11-16 17:01:21'),(89,35,2,'任务管理','schedule','monitor/job/Index','/job',NULL,1,0,NULL,430,1,'2020-11-18 18:19:34','2020-11-18 18:54:55'),(90,89,3,'任务开启',NULL,NULL,NULL,'sys:job:trigger',1,0,NULL,1,1,'2020-11-18 19:10:49','2020-11-18 19:10:49'),(91,89,3,'任务暂停',NULL,NULL,NULL,'sys:job:pause',1,0,NULL,1,1,'2020-11-18 19:11:14','2020-11-18 19:11:14'),(92,89,3,'任务恢复',NULL,NULL,NULL,'sys:job:resume',1,0,NULL,1,1,'2020-11-18 19:11:37','2020-11-18 19:11:37'),(93,89,3,'任务添加',NULL,NULL,NULL,'sys:job:add',1,0,NULL,1,1,'2020-11-18 19:17:09','2020-11-18 19:17:09'),(94,89,3,'任务更新',NULL,NULL,NULL,'sys:job:update',1,0,NULL,1,1,'2020-11-18 19:17:31','2020-11-18 19:17:31'),(95,89,3,'任务删除',NULL,NULL,NULL,'sys:job:delete',1,0,NULL,1,1,'2020-11-18 19:17:52','2020-11-18 19:17:52');
/*!40000 ALTER TABLE `sys_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role`
--

DROP TABLE IF EXISTS `sys_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_role` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '角色Id',
  `role_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `zh_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色中文名称',
  `status` int NOT NULL DEFAULT '0' COMMENT '状态(1可用，0禁用）',
  `is_default` int DEFAULT '0' COMMENT '是否默认角色',
  `remark` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '角色描述',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='系统用户角色';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role`
--

LOCK TABLES `sys_role` WRITE;
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;
INSERT INTO `sys_role` VALUES (1,'ROLE_ADMIN','超级管理员',1,0,'超级管理员','2020-05-30 21:17:19','2020-11-12 22:39:57'),(2,'ROLE_USER','普通用户',1,1,'普通用户','2020-06-23 16:04:51','2020-12-01 22:09:07'),(33,'ROLE_TEST','测试用户狗',1,0,'炮灰','2020-09-04 10:05:21','2020-11-14 15:18:15');
/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role_resource`
--

DROP TABLE IF EXISTS `sys_role_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_role_resource` (
  `role_id` bigint NOT NULL,
  `resource_id` bigint NOT NULL,
  PRIMARY KEY (`role_id`,`resource_id`),
  KEY `resource_id` (`resource_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `sys_role_resource_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`id`),
  CONSTRAINT `sys_role_resource_ibfk_2` FOREIGN KEY (`resource_id`) REFERENCES `sys_resource` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='角色权限关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role_resource`
--

LOCK TABLES `sys_role_resource` WRITE;
/*!40000 ALTER TABLE `sys_role_resource` DISABLE KEYS */;
INSERT INTO `sys_role_resource` VALUES (1,3),(2,3),(33,3),(1,34),(2,34),(33,34),(1,35),(33,35),(1,36),(2,36),(33,36),(1,37),(2,37),(33,37),(1,38),(2,38),(33,38),(1,40),(2,40),(33,40),(1,41),(33,41),(1,42),(33,42),(1,43),(2,43),(33,43),(1,47),(2,47),(33,47),(1,48),(2,48),(33,48),(1,49),(2,49),(33,49),(1,50),(2,50),(33,50),(1,51),(2,51),(33,51),(1,52),(2,52),(33,52),(1,53),(2,53),(33,53),(1,54),(2,54),(33,54),(1,55),(33,55),(1,57),(33,57),(1,58),(2,58),(33,58),(1,59),(2,59),(33,59),(1,60),(2,60),(33,60),(1,68),(2,68),(33,68),(1,69),(2,69),(33,69),(1,70),(33,70),(1,71),(33,71),(1,72),(33,72),(1,73),(33,73),(1,74),(33,74),(1,75),(33,75),(33,76),(33,77),(1,79),(33,79),(1,80),(33,80),(1,81),(2,81),(33,81),(1,82),(2,82),(33,82),(1,83),(2,83),(33,83),(1,84),(2,84),(33,84),(1,85),(2,85),(33,85),(33,88),(1,89),(1,90),(1,91),(1,92),(1,93),(1,94),(1,95);
/*!40000 ALTER TABLE `sys_role_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_user` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户Id',
  `login_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录名称，由系统自动生成',
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户别名',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '手机号码',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '邮件号',
  `sex` int DEFAULT NULL COMMENT '性别',
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录密码',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户头像',
  `act_code` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '激活码',
  `status` int NOT NULL DEFAULT '0' COMMENT '状态(0,禁用，1可用）',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sys_user_login_name_uindex` (`login_name`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='系统用户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user`
--

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` VALUES (1,'root','管理员','18112345678','root@qq.com',1,'123456','http://m-zhou.oss-cn-chengdu.aliyuncs.com/header.jpg',NULL,1,'2020-06-08 13:40:43','2020-11-14 09:01:39'),(2,'zhou','周军伍','18112345678','zhou@qq.com',1,'123456','https://m-zhou.oss-cn-chengdu.aliyuncs.com/%E7%8C%AB.jpg',NULL,1,'2020-06-23 08:05:57','2020-08-26 13:44:03'),(71,'admin','周','18112345678','admin@qq.com',2,'admin','https://m-zhou.oss-cn-chengdu.aliyuncs.com/%E7%8C%AB.jpg',NULL,1,'2020-07-27 04:49:42','2020-09-14 02:14:26'),(74,'wu','wu','18112345678','',1,'123456','https://m-zhou.oss-cn-chengdu.aliyuncs.com/%E7%8C%AB.jpg',NULL,0,'2020-07-27 09:23:18','2020-11-16 08:51:59'),(75,'jun','jun','18111111111','1111@qq.com',2,'jun','https://m-zhou.oss-cn-chengdu.aliyuncs.com/%E7%8C%AB.jpg',NULL,0,'2020-07-27 13:51:02','2020-09-14 02:14:40'),(77,'chen','chen','','',2,'','https://m-zhou.oss-cn-chengdu.aliyuncs.com/%E7%8C%AB.jpg',NULL,1,'2020-07-27 14:58:47','2020-08-27 03:34:23'),(85,'long','long','18112345678','121@qq.com',1,'long','https://m-zhou.oss-cn-chengdu.aliyuncs.com/%E7%8C%AB.jpg',NULL,1,'2020-08-01 09:18:20','2020-09-13 07:20:33'),(94,'sx','帅哥','18111336833','15151@qq.com',1,'123456','',NULL,1,'2020-08-26 09:16:31','2020-11-14 07:18:02');
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user_role`
--

DROP TABLE IF EXISTS `sys_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_user_role` (
  `user_id` bigint NOT NULL COMMENT '用户Id',
  `role_id` bigint NOT NULL COMMENT '角色id\n',
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_id` (`role_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `sys_user_role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`id`),
  CONSTRAINT `sys_user_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户角色关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user_role`
--

LOCK TABLES `sys_user_role` WRITE;
/*!40000 ALTER TABLE `sys_user_role` DISABLE KEYS */;
INSERT INTO `sys_user_role` VALUES (1,1),(71,2),(74,2),(75,2),(77,2),(85,2),(94,2),(2,33);
/*!40000 ALTER TABLE `sys_user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `third_user`
--

DROP TABLE IF EXISTS `third_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `third_user` (
  `third_id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户Id',
  `login_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录方式，即平台',
  `id` bigint DEFAULT NULL COMMENT '用户在其他系统中的Id',
  `login` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户登录名称',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户昵称',
  `avatar_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '头像地址',
  `access_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '访问令牌',
  `refresh_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '刷新令牌',
  `expires_in` bigint DEFAULT NULL COMMENT '过期时间',
  `status` tinyint DEFAULT '1',
  `create_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '记录添加时间',
  PRIMARY KEY (`third_id`),
  UNIQUE KEY `third_user_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='第三方用户信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `third_user`
--

LOCK TABLES `third_user` WRITE;
/*!40000 ALTER TABLE `third_user` DISABLE KEYS */;
INSERT INTO `third_user` VALUES (1,'1',1,'1','1','1','1','1',1,1,'2020-08-26 18:31:11'),(5,'2',2,'2','2','2','2','2',NULL,1,'2020-08-26 18:32:07');
/*!40000 ALTER TABLE `third_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-01 23:35:37
