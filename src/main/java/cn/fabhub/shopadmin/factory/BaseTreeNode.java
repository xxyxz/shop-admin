package cn.fabhub.shopadmin.factory;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.List;

/**
 * 树节点接口
 * @author zhou
 * @date 2020-09-12 22:11
 */
public interface BaseTreeNode<T> extends Serializable {

    /**
     * 获取节点Id
     * @return id
     */
//    @JsonIgnore
    Long getId();

    /**
     * 获取父节点Id
     * @return 父id
     */
    @JsonIgnore
    Long getPid();

    /**
     * 设置孩子节点
     * @param children 子节点集合
     */
    void setChildren(List<T> children);
}
