package cn.fabhub.shopadmin.factory.entity;

import cn.fabhub.shopadmin.entity.SysRole;

/**
 * @author zhou
 * @date 2020-09-13 15:01
 */
public class SysRoleFactory {

    public static SysRole newInstance() {
        return new SysRole();
    }
}
