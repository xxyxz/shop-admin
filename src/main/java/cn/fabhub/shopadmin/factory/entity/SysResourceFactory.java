package cn.fabhub.shopadmin.factory.entity;

import cn.fabhub.shopadmin.entity.SysResource;

/**
 * @author zhou
 * @date 2020-09-13 15:30
 */
public class SysResourceFactory {

    public static SysResource newInstance() {
        return new SysResource();
    }

}
