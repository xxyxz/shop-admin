package cn.fabhub.shopadmin.factory.entity;

import cn.fabhub.shopadmin.entity.SysUser;

/**
 * @author zhou
 * @date 2020-09-13 15:03
 */
public class SysUserFactory {

    public static SysUser newInstance() {
        return new SysUser();
    }
}
