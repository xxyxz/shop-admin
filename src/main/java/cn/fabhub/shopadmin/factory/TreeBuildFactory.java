package cn.fabhub.shopadmin.factory;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhou
 * @date 2020-09-12 22:11
 */
@Data
public class TreeBuildFactory<T extends BaseTreeNode<T>> {

    /**
     * 构建树形结构
     */
    public List<T> buildTree(List<T> nodes) {
        List<T> treeNodes = new ArrayList<>();
        List<T> rootNodes = getRootNodes(nodes);
        for (T rootNode : rootNodes) {
            buildChildNode(nodes, rootNode);
            treeNodes.add(rootNode);
        }
        return treeNodes;
    }


    /**
     * 递归子节点
     */
    private void buildChildNode(List<T> nodes, T rootNode) {
        List<T> children = getChildNode(nodes, rootNode);
        if (!children.isEmpty()) {
            for (T child : children) {
                buildChildNode(nodes, child);
            }
            rootNode.setChildren(children);
        }
    }


    /**
     * 获取父节点下所有的子节点
     */
    private List<T> getChildNode(List<T> nodes, T rootNode) {
        List<T> childNodes = new ArrayList<>();
        for (T node : nodes) {
            if (rootNode.getId().equals(node.getPid())) {
                childNodes.add(node);
            }
        }
        return childNodes;
    }


    /**
     * 获取集合中所有的根节点
     */
    private List<T> getRootNodes(List<T> nodes) {
        List<T> rootNodes = new ArrayList<>();
        for (T node : nodes) {
            if (rootNode(nodes, node)) {
                rootNodes.add(node);
            }
        }
        return rootNodes;
    }


    /**
     * 判断是否为根节点
     */
    private boolean rootNode(List<T> nodes, T node) {
        boolean isRootNode = true;
        for (T n : nodes) {
            if (node.getPid().equals(n.getId())) {
                isRootNode = false;
                break;
            }
        }
        return isRootNode;
    }
}
