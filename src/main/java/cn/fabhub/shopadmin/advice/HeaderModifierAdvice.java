package cn.fabhub.shopadmin.advice;

import cn.fabhub.shopadmin.util.TokenService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import javax.servlet.http.HttpServletResponse;

/**
 * 修改利用@ResponseBody注解之后的响应头
 * @author zhou
 * @date 2020-08-27 14:00
 */
@ControllerAdvice
public class HeaderModifierAdvice implements ResponseBodyAdvice<Object> {

    @Override
    public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> aClass) {
        return true;
    }


    @Override
    public Object beforeBodyWrite(Object o,
                                  MethodParameter methodParameter,
                                  MediaType mediaType,
                                  Class<? extends HttpMessageConverter<?>> aClass,
                                  ServerHttpRequest serverHttpRequest,
                                  ServerHttpResponse serverHttpResponse) {
        // 转换对象
        HttpServletResponse response = ((ServletServerHttpResponse) serverHttpResponse).getServletResponse();
        // 获得token
        String token = TokenService.refresh();
        if (StringUtils.isNotBlank(token)) {
            /*刷新token*/
            response.addHeader(TokenService.TOKEN_KEY, token);
        }
        return o;
    }

}
