package cn.fabhub.shopadmin.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author zhou
 * @date 2020-09-01 17:07
 */
@Configuration
@EnableAsync
public class TaskConfig {

    @Bean
    public TaskExecutor  executor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(10);   // 核心线程数量
        executor.setMaxPoolSize(30); // 最大线程数量
        executor.setQueueCapacity(1000); // 队列大小
        executor.setKeepAliveSeconds(300); // 线程最大空闲时间
        executor.setThreadNamePrefix("async-Executor-"); // 指定新建线程名称的前缀
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        executor.initialize();
        return executor;
    }
}
