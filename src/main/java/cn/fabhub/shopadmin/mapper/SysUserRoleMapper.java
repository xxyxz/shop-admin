package cn.fabhub.shopadmin.mapper;

import cn.fabhub.shopadmin.entity.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @author zhou
 * @date 2020-08-01 17:04
 */

public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

    int batchInsert(@Param("list") List<SysUserRole> list);

    int deleteByUserId(@Param("userId") Long userId);

    int deleteByUserIds(@Param("userIds") Collection<? extends Serializable> userIds);

    List<Long> getRoleIdByUserId(@Param("userId") Long userId);

}