package cn.fabhub.shopadmin.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.fabhub.shopadmin.quartz.entity.SysJob;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/17 21:29
 */
@Mapper
public interface SysJobMapper extends BaseMapper<SysJob> {

    int updateStatus(@Param("jobName") String jobName, @Param("jobGroup") String jobGroup, @Param("status") Integer status);

    int delete(@Param("jobName") String jobName, @Param("jobGroup") String jobGroup);
}