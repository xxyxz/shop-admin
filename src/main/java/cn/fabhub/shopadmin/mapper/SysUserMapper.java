package cn.fabhub.shopadmin.mapper;
import org.apache.ibatis.annotations.Param;

import cn.fabhub.shopadmin.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author zhou
 * @date 2020-07-27 12:30
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    SysUser selectOneByLoginName(@Param("loginName")String loginName);

}