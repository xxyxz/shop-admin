package cn.fabhub.shopadmin.mapper;
import org.apache.ibatis.annotations.Param;

import cn.fabhub.shopadmin.entity.SysDictType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author zhou
 * @date 2020-08-19 20:43
*/
    
public interface SysDictTypeMapper extends BaseMapper<SysDictType> {

    int updateOrderNumByDictId(@Param("dictId")Long dictId, @Param("updatedOrderNum")Integer updatedOrderNum);
}