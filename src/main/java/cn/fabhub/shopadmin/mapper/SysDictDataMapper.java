package cn.fabhub.shopadmin.mapper;
import org.apache.ibatis.annotations.Param;

import cn.fabhub.shopadmin.entity.SysDictData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author zhou
 * @date 2020-08-19 23:17
 */
public interface SysDictDataMapper extends BaseMapper<SysDictData> {

    int updateDictType(@Param("oldDictType")String oldDictType,@Param("newDictType")String newDictType);

}