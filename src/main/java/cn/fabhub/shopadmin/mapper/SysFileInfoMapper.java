package cn.fabhub.shopadmin.mapper;

import cn.fabhub.shopadmin.entity.SysFileInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author  Mr.zhou
 * @date  2020/11/12 17:49
 * @version 1.0
 */
public interface SysFileInfoMapper extends BaseMapper<SysFileInfo> {
}