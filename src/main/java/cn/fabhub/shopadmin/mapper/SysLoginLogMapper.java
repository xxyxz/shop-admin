package cn.fabhub.shopadmin.mapper;

import cn.fabhub.shopadmin.entity.SysLoginLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author zhou
 * @date 2020-08-19 12:15
 */

public interface SysLoginLogMapper extends BaseMapper<SysLoginLog> {
    void truncate();
}