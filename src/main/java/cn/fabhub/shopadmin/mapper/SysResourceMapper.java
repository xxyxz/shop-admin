package cn.fabhub.shopadmin.mapper;

import cn.fabhub.shopadmin.entity.SysResource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.*;

/**
 * @author zhou
 * @date 2020-09-12 11:03
 */
public interface SysResourceMapper extends BaseMapper<SysResource> {

    Long countRelationOfRole(@Param("resourceId") Long resourceId);

    Long countSon(@Param("resourceId") Long resourceId);

    List<SysResource> getResourcesByRoleIds(@Param("roleIds") Collection<Long> roleIds);

    Set<SysResource> getRouteResourceByRoleIds(@Param("roleIds") Collection<Long> userIds);

    Set<String> getResourceCodeByRoleIds(@Param("roleIds") Collection<Long> roleIds);

    int updateStatusByIds(@Param("ids") Set<Long> ids, @Param("status") Integer status);
}