package cn.fabhub.shopadmin.mapper;

import cn.fabhub.shopadmin.entity.SysAccessLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author zhou
 * @date 2020-08-10 20:32
 */

public interface SysAccessLogMapper extends BaseMapper<SysAccessLog> {
    /**
     * 清空日志
     */
    void truncate();
}