package cn.fabhub.shopadmin.mapper;

import cn.fabhub.shopadmin.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.*;

/**
 * @author zhou
 * @date 2020-08-01 15:12
 */

public interface SysRoleMapper extends BaseMapper<SysRole> {

    Long countOccupy(@Param("roleIds") Collection<? extends Serializable> roleIds);

    List<SysRole> getRolesByUserId(@Param("userId") long userId);

    Set<Long> getRoleIdsByUserId(@Param("userId") Long userId);

    List<Long> getDefaultRoleIds();

}