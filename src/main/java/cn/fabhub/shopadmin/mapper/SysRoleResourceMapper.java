package cn.fabhub.shopadmin.mapper;

import cn.fabhub.shopadmin.entity.SysRoleResource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @author zhou
 * @date 2020-08-10 13:59
*/
    
public interface SysRoleResourceMapper extends BaseMapper<SysRoleResource> {


    int batchInsert(@Param("list") List<SysRoleResource> list);

    int deleteByRoleId(@Param("roleId")Long roleId);

    int deleteByRoleIds(@Param("roleIds")Collection<? extends Serializable> roleIds);

    List<Long> getResourceIdByRoleId(@Param("roleId")Long roleId);

}