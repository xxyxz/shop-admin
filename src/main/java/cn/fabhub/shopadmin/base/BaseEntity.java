package cn.fabhub.shopadmin.base;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zhou
 * @date 2020-07-25 17:05
 */
@Data
public class BaseEntity implements Serializable {

    /**
     * 条目状态
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Date createDate;

    /**
     * 修改时间
     */
    private Date updateDate;
}
