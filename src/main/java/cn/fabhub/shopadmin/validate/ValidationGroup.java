package cn.fabhub.shopadmin.validate;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/5 12:00
 */
public class ValidationGroup {

    public interface ResetPassword {}

    public interface Login extends ResetPassword {}

    public interface Register extends ResetPassword{}

     /*通用*/

    public interface Add{}

    public interface Patch{}

    public interface Update extends Patch{}

    public interface Delete {}

    public interface get{}
}
