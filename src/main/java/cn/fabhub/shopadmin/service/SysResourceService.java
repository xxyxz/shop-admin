package cn.fabhub.shopadmin.service;

import cn.fabhub.shopadmin.entity.SysResource;
import cn.fabhub.shopadmin.query.ResourceQuery;
import cn.fabhub.shopadmin.vo.SelectedTreeNode;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author zhou
 * @date 2020-05-30 22:09
 */
public interface SysResourceService extends IService<SysResource> {

    void removeChecked(Long id);

    List<SelectedTreeNode> toSelectedTreeNodeList(List<SysResource> resourceList);

    List<SysResource> list(ResourceQuery resourceQuery);

    void updateDetails(SysResource form);
}








