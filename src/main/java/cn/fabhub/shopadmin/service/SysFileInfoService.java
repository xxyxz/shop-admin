package cn.fabhub.shopadmin.service;

import cn.fabhub.shopadmin.entity.SysFileInfo;
import cn.fabhub.shopadmin.query.FileInfoQuery;
import cn.fabhub.shopadmin.vo.PR;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author  Mr.zhou
 * @date  2020/11/12 17:49
 * @version 1.0
 */
public interface SysFileInfoService extends IService<SysFileInfo> {

    /**
     * 异步保存文件
     * @param sysFileInfo 文件
     */
    void saveAsync(SysFileInfo sysFileInfo);

    PR<SysFileInfo> list(FileInfoQuery fileInfoQuery);
}
