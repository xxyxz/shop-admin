package cn.fabhub.shopadmin.service;

import cn.fabhub.shopadmin.vo.OnlineUser;

import java.util.List;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/10/18 19:04
 */
public interface OnlineUserService {

    void save(String token);

    void forceExit(String sessionId);

    List<OnlineUser> list();
}
