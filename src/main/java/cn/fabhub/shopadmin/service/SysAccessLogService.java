package cn.fabhub.shopadmin.service;

import cn.fabhub.shopadmin.entity.SysAccessLog;
import cn.fabhub.shopadmin.query.OperLogQuery;
import cn.fabhub.shopadmin.vo.PR;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author zhou
 * @date 2020-05-30 12:00
 */

public interface SysAccessLogService extends IService<SysAccessLog> {

    /**
     * 清空日志
     */
    void truncate();

    PR<SysAccessLog> list(OperLogQuery logQuery);

    void saveDetails(SysAccessLog sysAccessLog);
}


