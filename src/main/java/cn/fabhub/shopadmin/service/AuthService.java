package cn.fabhub.shopadmin.service;

import cn.fabhub.shopadmin.dto.UserDTO;
import cn.fabhub.shopadmin.entity.SysUser;
import cn.fabhub.shopadmin.vo.RouterVO;

import java.util.List;

/**
 * @author zhou
 * @date 2020-05-30 19:03
 */
public interface AuthService {

    /**
     * 注册用户
     * @param sysUser 用户
     */
    void register(SysUser sysUser);

    /**
     * 登录用户
     * @param sysUser 用户
     * @return 用户信息
     */
    SysUser login(SysUser sysUser);

    /**
     * 更新用户
     * @param sysUser 用户
     */
    void update(SysUser sysUser);

    /**
     * 获得用户
     * @param sysUser 用户
     * @return 用户详情
     */
    SysUser get(SysUser sysUser);

    /**
     * 获得用户
     * @param id id
     * @return user object
     */
    UserDTO get(Long id);

    /**
     * 获得路由表
     * @param userId 用户id
     * @return 路由表
     */
    List<RouterVO> getRouters(Long userId);
}
