package cn.fabhub.shopadmin.service;

import cn.fabhub.shopadmin.entity.SysDictData;
import cn.fabhub.shopadmin.query.DictDataQuery;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author zhou
 * @date 2020-08-19 21:25
 */

public interface SysDictDataService extends IService<SysDictData> {


    List<SysDictData> list(DictDataQuery dictDataQuery);


}
