package cn.fabhub.shopadmin.service;

import cn.fabhub.shopadmin.entity.SysDictType;
import cn.fabhub.shopadmin.query.DictQueryType;
import cn.fabhub.shopadmin.vo.PR;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author zhou
 * @date 2020-05-30 21:19
 */

public interface SysDictTypeService extends IService<SysDictType> {


    PR<SysDictType> list(DictQueryType dictQueryType);

    void updateDetails(SysDictType form);

    void updateOrderNum(Long id, Integer orderNum);
}

