package cn.fabhub.shopadmin.service;

import cn.fabhub.shopadmin.entity.SysUser;
import cn.fabhub.shopadmin.entity.SysUserRole;
import cn.fabhub.shopadmin.query.UserQuery;
import cn.fabhub.shopadmin.vo.PR;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author zhou
 * @date 2020-05-30 10:49
 */
public interface SysUserService extends IService<SysUser> {

    PR<SysUser> list(UserQuery userQuery);

    void saveDetails(SysUser form);

    List<SysUserRole> createUserRoleList(Long userId, List<Long>roleIds);

    void updateDetails(SysUser form);

    SysUser getDetails(Long id);
}


