package cn.fabhub.shopadmin.service;

import cn.fabhub.shopadmin.entity.SysLoginLog;
import cn.fabhub.shopadmin.query.LoginLogQuery;
import cn.fabhub.shopadmin.vo.PR;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author zhou
 * @date 2020-08-19 11:52
 */

public interface SysLoginLogService extends IService<SysLoginLog> {


    PR<SysLoginLog> list(LoginLogQuery loginLogQuery);

    void truncate();
}
