package cn.fabhub.shopadmin.service.impl;

import cn.fabhub.shopadmin.entity.SysDictType;
import cn.fabhub.shopadmin.mapper.SysDictDataMapper;
import cn.fabhub.shopadmin.mapper.SysDictTypeMapper;
import cn.fabhub.shopadmin.query.DictQueryType;
import cn.fabhub.shopadmin.service.SysDictTypeService;
import cn.fabhub.shopadmin.vo.PR;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Collection;

/**
 * @author zhou
 * @date 2020-05-30 21:19
 */
@Service
public class SysDictTypeServiceImpl extends ServiceImpl<SysDictTypeMapper, SysDictType> implements SysDictTypeService {

    @Resource
    private SysDictDataMapper sysDictDataMapper;


    @Override
    public PR<SysDictType> list(DictQueryType dictQueryType) {
        // 构建查询条件
        LambdaQueryWrapper<SysDictType> qw = new LambdaQueryWrapper<>();
        qw.orderByAsc(SysDictType::getOrderNum);
        // 构建分页
        Page<SysDictType> pageRequest = new Page<>(dictQueryType.getPageNum(), dictQueryType.getPageSize());
        // 执行查询
        Page<SysDictType> page = this.page(pageRequest, qw);
        // 返回转化的结果
        return PR.newInstance(page);
    }

    @Override
    @Transactional
    public boolean removeByIds(Collection<? extends Serializable> idList) {
        // 删除相关的字典数据
        this.baseMapper.deleteBatchIds(idList);
        return true;
    }

    @Override
    @Transactional
    public void updateDetails(SysDictType form) {
        // 通过id查询
        SysDictType oldType = this.getById(form.getId());
        // 如果两次的type不一样，那么更新
        if (StringUtils.isNotBlank(form.getDictType()) && !form.getDictType().equals(oldType.getDictType())){
            // 更新字典数据字典类型
            sysDictDataMapper.updateDictType(oldType.getDictType(), form.getDictType());
        }
        // 更新数据字典
        this.updateById(form);
    }

    @Override
    public void updateOrderNum(Long id, Integer orderNum) {
        // 更新
        this.baseMapper.updateOrderNumByDictId(id, orderNum);
    }
}

