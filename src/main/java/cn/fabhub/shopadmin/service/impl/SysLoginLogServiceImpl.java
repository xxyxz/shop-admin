package cn.fabhub.shopadmin.service.impl;

import cn.fabhub.shopadmin.entity.SysLoginLog;
import cn.fabhub.shopadmin.mapper.SysLoginLogMapper;
import cn.fabhub.shopadmin.query.LoginLogQuery;
import cn.fabhub.shopadmin.service.SysLoginLogService;
import cn.fabhub.shopadmin.vo.PR;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * @author zhou
 * @date 2020-08-19 11:52
 */

@Service
public class SysLoginLogServiceImpl extends ServiceImpl<SysLoginLogMapper, SysLoginLog> implements SysLoginLogService {

    @Override
    public PR<SysLoginLog> list(LoginLogQuery loginLogQuery) {
        LambdaQueryWrapper<SysLoginLog> qw = new LambdaQueryWrapper<>();
        qw.orderByDesc(SysLoginLog::getId);

        if (StringUtils.isNotBlank(loginLogQuery.getLoginName())) {
            qw.eq(SysLoginLog::getLoginTime, loginLogQuery.getLoginName());
        }

        if (loginLogQuery.getStatus() != null) {
            qw.eq(SysLoginLog::getStatus, loginLogQuery.getStatus());
        }

        if (loginLogQuery.getStartTime() != null) {
            qw.ge(SysLoginLog::getLoginTime, loginLogQuery.getStartTime());
        }

        if (loginLogQuery.getEndTime() != null) {
            qw.le(SysLoginLog::getLoginTime, loginLogQuery.getEndTime());
        }

        Page<SysLoginLog> page = new Page<>(loginLogQuery.getPageNum(), loginLogQuery.getPageSize());
        Page<SysLoginLog> result = this.page(page, qw);

        return PR.newInstance(result);
    }

    @Override
    public void truncate() {
        this.baseMapper.truncate();
    }
}
