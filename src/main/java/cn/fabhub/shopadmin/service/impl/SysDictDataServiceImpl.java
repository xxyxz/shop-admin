package cn.fabhub.shopadmin.service.impl;

import cn.fabhub.shopadmin.entity.SysDictData;
import cn.fabhub.shopadmin.mapper.SysDictDataMapper;
import cn.fabhub.shopadmin.query.DictDataQuery;
import cn.fabhub.shopadmin.service.SysDictDataService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * @author zhou
 * @date 2020-08-19 21:25
*/
    
@Service
public class SysDictDataServiceImpl extends ServiceImpl<SysDictDataMapper, SysDictData> implements SysDictDataService {

    @Override
    public List<SysDictData> list(DictDataQuery dictDataQuery) {
        LambdaQueryWrapper<SysDictData> qw = new LambdaQueryWrapper<>();
        qw.orderByAsc(SysDictData::getOrderNum);
        if (StringUtils.isNotBlank(dictDataQuery.getDictType())) {
            qw.eq(SysDictData::getDictType, dictDataQuery.getDictType());
        }
        if (dictDataQuery.getStatus() != null) {
            qw.eq(SysDictData::getStatus, dictDataQuery.getStatus());
        }
        return this.list(qw);
    }
}
