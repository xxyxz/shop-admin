package cn.fabhub.shopadmin.service.impl;

import cn.fabhub.shopadmin.entity.SysResource;
import cn.fabhub.shopadmin.exception.CustomException;
import cn.fabhub.shopadmin.mapper.SysResourceMapper;
import cn.fabhub.shopadmin.query.ResourceQuery;
import cn.fabhub.shopadmin.service.SysResourceService;
import cn.fabhub.shopadmin.vo.SelectedTreeNode;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zhou
 * @date 2020-05-30 22:09
 */

@Service
public class SysResourceServiceImpl extends ServiceImpl<SysResourceMapper, SysResource> implements SysResourceService {

    @Override
    public void removeChecked(Long id) {
        // 该资源是否有子节点
        if (existChildren(id)) {
            throw new CustomException("删除失败，请删除子节点后再删除本节点");
        }
        // 本资源下是否有占用
        if (existOccupy(id)) {
            throw new CustomException("删除失败，请删除和本节点和角色的关联后删除本节点");
        }
        this.removeById(id);
    }

    @Override
    public List<SysResource> list(ResourceQuery resourceQuery) {
        // 获得所有的资源
        LambdaQueryWrapper<SysResource> qw = new LambdaQueryWrapper<>();
        qw.orderByAsc(SysResource::getOrderNum);
        if (resourceQuery.getStatus() != null) {
            qw.eq(SysResource::getStatus, resourceQuery.getStatus());
        }
        if (StringUtils.isNotBlank(resourceQuery.getName())) {
            qw.like(SysResource::getName, resourceQuery.getName());
        }
        return this.list(qw);
    }

    @Override
    @Transactional
    public void updateDetails(SysResource form) {
        // 查询
        SysResource oldResource = this.getById(form.getId());
        // 修改子节点状态
        if (!oldResource.getStatus().equals(form.getStatus())) {
            Set<Long> children = findChildren(oldResource);
            if (!CollectionUtils.isEmpty(children)) {
                this.baseMapper.updateStatusByIds(children, form.getStatus());
            }
        }
        // 修改基本信息
        this.updateById(form);
    }


    /**
     * 查找节点的子节点
     * 思路一：先构建所有资源的树结构，然后通过递归查找该节点，获取其孩子
     * 思路er：先找查询出所有的节点集合，然后通过递归查找其孩子
     *
     * @param parent 父节点
     * @return 子节点
     */
    private Set<Long> findChildren(SysResource parent) {
        // 查找所有的资源
        Set<SysResource> parents = new HashSet<>();
        parents.add(parent);
        Set<SysResource> children = findChildren(parents, this.list());
        return children.stream().map(SysResource::getId).collect(Collectors.toSet());
    }

    private Set<SysResource> getChildrenByIds(Set<SysResource> parents, List<SysResource> all) {
        Set<Long> collect = parents.stream().map(SysResource::getId).collect(Collectors.toSet());
        return all.stream().filter(item -> collect.contains(item.getPid())).collect(Collectors.toSet());
    }


    private Set<SysResource> findChildren(Set<SysResource> parents, List<SysResource> all) {
        Set<SysResource> resources = new HashSet<>(getChildrenByIds(parents, all));
        if (resources.size() < 1) {
            return resources;
        }
        resources.addAll(findChildren(resources, all));
        return resources;
    }


    /**
     * 是否存在资源占用
     *
     * @param resourceId 资源id
     * @return 结果
     */
    private boolean existOccupy(Long resourceId) {
        return this.baseMapper.countRelationOfRole(resourceId) > 0;
    }

    /**
     * 是否存在子节点
     *
     * @param pid 父节点id
     * @return 存在？
     */
    private boolean existChildren(Long pid) {
        return this.baseMapper.countSon(pid) > 0;
    }

    @Override
    public List<SelectedTreeNode> toSelectedTreeNodeList(List<SysResource> resourceList) {
        List<SelectedTreeNode> result = new ArrayList<>(32);
        for (SysResource resource : resourceList) {
            // 创建目标对象
            SelectedTreeNode target = new SelectedTreeNode();
            target.setKey(resource.getId());
            target.setValue(resource.getId());
            target.setTitle(resource.getName());
            target.setIcon(resource.getIcon());
            target.setParentId(resource.getParentId());
            result.add(target);
        }
        return result;
    }
}








