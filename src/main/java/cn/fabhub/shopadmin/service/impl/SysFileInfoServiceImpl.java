package cn.fabhub.shopadmin.service.impl;


import cn.fabhub.shopadmin.entity.SysFileInfo;
import cn.fabhub.shopadmin.mapper.SysFileInfoMapper;
import cn.fabhub.shopadmin.query.FileInfoQuery;
import cn.fabhub.shopadmin.service.SysFileInfoService;
import cn.fabhub.shopadmin.vo.PR;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;


/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/12 17:49
 */
@Service
public class SysFileInfoServiceImpl extends ServiceImpl<SysFileInfoMapper, SysFileInfo> implements SysFileInfoService {


    @Override
    public void saveAsync(SysFileInfo sysFileInfo) {
        this.save(sysFileInfo);
    }

    @Override
    public PR<SysFileInfo> list(FileInfoQuery fileInfoQuery) {
        // 构造条件
        LambdaQueryWrapper<SysFileInfo> qw = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(fileInfoQuery.getStorage())){
            qw.eq(SysFileInfo::getStorage, fileInfoQuery.getStorage());
        }
        if (StringUtils.isNotBlank(fileInfoQuery.getType())){
            qw.eq(SysFileInfo::getType, fileInfoQuery.getType());
        }
        if (fileInfoQuery.getStartTime() != null) {
            qw.ge(SysFileInfo::getCreateTime, fileInfoQuery.getStartTime());
        }
        if (fileInfoQuery.getEndTime() != null) {
            qw.le(SysFileInfo::getCreateTime, fileInfoQuery.getEndTime());
        }

        // 构造分页
        Page<SysFileInfo> pageRequest = new Page<>(fileInfoQuery.getPageNum(), fileInfoQuery.getPageSize());
        // 执行分页查询
        Page<SysFileInfo> page = this.page(pageRequest, qw);
        return PR.newInstance(page);
    }
}
