package cn.fabhub.shopadmin.service.impl;

import cn.fabhub.shopadmin.entity.SysUser;
import cn.fabhub.shopadmin.entity.SysUserRole;
import cn.fabhub.shopadmin.exception.CustomException;
import cn.fabhub.shopadmin.mapper.SysRoleMapper;
import cn.fabhub.shopadmin.mapper.SysUserMapper;
import cn.fabhub.shopadmin.mapper.SysUserRoleMapper;
import cn.fabhub.shopadmin.query.UserQuery;
import cn.fabhub.shopadmin.service.SysUserService;
import cn.fabhub.shopadmin.vo.PR;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author zhou
 * @date 2020-05-30 10:37
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    @Resource
    private SysUserMapper sysUserMapper;

    @Resource
    private SysRoleMapper sysRoleMapper;

    @Resource
    private SysUserRoleMapper sysUserRoleMapper;


    @Override
    public PR<SysUser> list(UserQuery userQuery) {

        LambdaQueryWrapper<SysUser> qw = new LambdaQueryWrapper<>();

        if (StringUtils.isNoneBlank(userQuery.getLoginName())) {
            qw.like(SysUser::getLoginName, userQuery.getLoginName());
        }

        if (StringUtils.isNoneBlank(userQuery.getUsername())) {
            qw.like(SysUser::getUsername, userQuery.getUsername());
        }

        if (StringUtils.isNoneBlank(userQuery.getPhone())) {
            qw.like(SysUser::getPhone, userQuery.getPhone());
        }

        if (StringUtils.isNoneBlank(userQuery.getEmail())) {
            qw.like(SysUser::getEmail, userQuery.getEmail());
        }

        if (userQuery.getSex() != null) {
            qw.eq(SysUser::getSex, userQuery.getSex());
        }

        if (userQuery.getStatus() != null) {
            qw.eq(SysUser::getStatus, userQuery.getStatus());
        }

        if (userQuery.getStartTime() != null) {
            qw.ge(SysUser::getCreateDate, userQuery.getStartTime());
        }

        if (userQuery.getEndTime() != null) {
            qw.le(SysUser::getCreateDate, userQuery.getEndTime());
        }
        Page<SysUser> pageRequest = new Page<>(userQuery.getPageNum(), userQuery.getPageSize());
        Page<SysUser> page = this.page(pageRequest, qw);
        return PR.newInstance(page);
    }


    @Override
    @Transactional
    public void saveDetails(SysUser form) {
        // 保存用户实体
        this.save(form);
        // 给用户分配角色
        List<SysUserRole> userRoleList;
        if (CollectionUtils.isEmpty(form.getRoleIds())) {
            // 添加默认角色
            List<Long> defaultRoleIds = sysRoleMapper.getDefaultRoleIds();
            if (CollectionUtils.isEmpty(defaultRoleIds)) {
                throw new CustomException("用户没有默认的角色可以分配");
            }
            userRoleList = createUserRoleList(form.getId(), defaultRoleIds);
        } else {
            // 添加前端分配的角色
            userRoleList = createUserRoleList(form.getId(), form.getRoleIds());
        }
        // 保存
        sysUserRoleMapper.batchInsert(userRoleList);
    }


    @Override
    @Transactional
    public boolean removeByIds(Collection<? extends Serializable> idList) {
        // 先删除关系
        sysUserRoleMapper.deleteByUserIds(idList);
        // 后删除实体
        sysUserMapper.deleteBatchIds(idList);
        return true;
    }


    @Override
    public List<SysUserRole> createUserRoleList(Long userId, List<Long> roleIds) {
        List<SysUserRole> sysUserRoles = new ArrayList<>();
        for (Long roleId : roleIds) {
            sysUserRoles.add(new SysUserRole(userId, roleId));
        }
        return sysUserRoles;
    }

    @Override
    @Transactional
    public void updateDetails(SysUser form) {
        // 修改基本信息
        this.updateById(form);
        // 删除和角色的管理关系
        sysUserRoleMapper.deleteByUserId(form.getId());
        // 保存用户角色关系
        sysUserRoleMapper.batchInsert(createUserRoleList(form.getId(), form.getRoleIds()));
    }

    @Override
    public SysUser getDetails(Long id) {
        // 获得基本信息
        SysUser result = this.getById(id);
        // 获得用户持有的角色
        result.setRoleIds(sysUserRoleMapper.getRoleIdByUserId(id));
        return result;
    }
}


