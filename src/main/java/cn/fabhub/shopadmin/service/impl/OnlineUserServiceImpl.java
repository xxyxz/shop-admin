package cn.fabhub.shopadmin.service.impl;

import cn.fabhub.shopadmin.service.OnlineUserService;
import cn.fabhub.shopadmin.vo.OnlineUser;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/10/18 19:04
 */
@Service
public class OnlineUserServiceImpl implements OnlineUserService {

    private static final String ONLINE_USER_KEY = "online:user:";

    private static final long TOKEN_EXPIRE = 30;

    @Resource
    private RedisTemplate<String, OnlineUser> redisTemplate;


    @Override
    public void save(String token) {
        OnlineUser onlineUser = new OnlineUser();
        onlineUser.setToken(token);
        redisTemplate.opsForValue().set(ONLINE_USER_KEY + token, onlineUser, TOKEN_EXPIRE, TimeUnit.MINUTES);
    }


    @Override
    public void forceExit(String token) {
        redisTemplate.delete(ONLINE_USER_KEY + token);
    }

    @Override
    public List<OnlineUser> list() {
        Set<String> keys = redisTemplate.keys(ONLINE_USER_KEY + "*");
        List<OnlineUser> list = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(keys)) {
            for (String key : keys) {
                OnlineUser onlineUser = redisTemplate.opsForValue().get(key);
                list.add(onlineUser);
            }
        }
        return list;
    }
}
