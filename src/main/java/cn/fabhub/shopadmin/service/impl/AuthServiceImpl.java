package cn.fabhub.shopadmin.service.impl;

import cn.fabhub.shopadmin.constant.NormalStatus;
import cn.fabhub.shopadmin.constant.ResourceType;
import cn.fabhub.shopadmin.constant.RoleBase;
import cn.fabhub.shopadmin.dto.UserDTO;
import cn.fabhub.shopadmin.entity.*;
import cn.fabhub.shopadmin.exception.Assert;
import cn.fabhub.shopadmin.exception.CustomException;
import cn.fabhub.shopadmin.mapper.SysResourceMapper;
import cn.fabhub.shopadmin.mapper.SysRoleMapper;
import cn.fabhub.shopadmin.mapper.SysUserMapper;
import cn.fabhub.shopadmin.mapper.SysUserRoleMapper;
import cn.fabhub.shopadmin.service.AuthService;
import cn.fabhub.shopadmin.service.SysLoginLogService;
import cn.fabhub.shopadmin.service.SysUserService;
import cn.fabhub.shopadmin.util.IpUtils;
import cn.fabhub.shopadmin.util.ServletUtils;
import cn.fabhub.shopadmin.vo.RouterVO;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import eu.bitwalker.useragentutils.UserAgent;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zhou
 * @date 2020-05-30 19:07
 */
@Service
@CacheConfig(cacheNames = "sys:user")
public class AuthServiceImpl implements AuthService, UserDetailsService {

    @Resource
    private SysUserMapper sysUserMapper;

    @Resource
    private SysRoleMapper sysRoleMapper;

    @Resource
    private SysResourceMapper sysResourceMapper;

    @Resource
    private SysUserRoleMapper sysUserRoleMapper;

    @Resource
    private SysUserService sysUserService;

    @Resource
    private SysLoginLogService sysLoginLogService;


    /**
     * 注册用户
     *
     * @param sysUser 用户
     */
    @Override
    @Transactional
    public void register(SysUser sysUser) {

        // 通过用户手机查询用户
        // 通过邮箱查询用户
        LambdaQueryWrapper<SysUser> queryWrapper = new LambdaQueryWrapper<>(sysUser);
        SysUser dataUser = sysUserMapper.selectOne(queryWrapper);
        if (dataUser != null) {
            throw new CustomException("用户已经注册");
        }
        // 保存用户
        sysUserMapper.insert(sysUser);
        // 判断是否保存成功
        if (sysUser.getId() == null) {
            throw new CustomException("保存用户失败！");
        }
        // 给用户授权
        List<Long> defaultRoleIds = sysRoleMapper.getDefaultRoleIds();
        // 判断
        if (CollectionUtils.isEmpty(defaultRoleIds)) {
            throw new CustomException("系统没有默认用户，不允许添加用户");
        }
        // 构建集合
        List<SysUserRole> sysUserRoles = sysUserService.createUserRoleList(sysUser.getId(), defaultRoleIds);
        // 存入数据库
        sysUserRoleMapper.batchInsert(sysUserRoles);
    }

    /**
     * 登录用户
     *
     * @param sysUser 用户
     * @return 用户信息
     */
    @Override
    public SysUser login(SysUser sysUser) {
        SysLoginLog sysLoginLog = SysLoginLog.builder().build();
        String loginIp = IpUtils.getIpAddr(ServletUtils.getRequest());
        String loginLocation = IpUtils.getLocation(loginIp);


        UserAgent userAgent = UserAgent.parseUserAgentString(ServletUtils.getRequest().getHeader("User-Agent"));
        String os = userAgent.getOperatingSystem().getName();
        String browser = userAgent.getBrowser().getName();

        // 通过用户手机查询用户
        SysUser dataUser = (SysUser) this.loadUserByUsername(sysUser.getLoginName());
        // 封装
        sysLoginLog.setLoginIp(loginIp);
        sysLoginLog.setLoginLocation(loginLocation);
        sysLoginLog.setOs(os);
        sysLoginLog.setBrowser(browser);
        sysLoginLog.setLoginName(sysUser.getLoginName());
        sysLoginLog.setLoginTime(new Date());

        if (dataUser == null) {
            sysLoginLog.setMsg("用户不存在");
            sysLoginLogService.save(sysLoginLog);
            throw new CustomException("用户不存在");
        }

        sysLoginLog.setUserId(dataUser.getId());
        if (dataUser.getStatus().equals(NormalStatus.DISABLE.getCode())) {
            sysLoginLog.setMsg("用户状态异常");
            sysLoginLogService.save(sysLoginLog);
            throw new CustomException("用户状态异常");
        }

        if (!sysUser.getPassword().equals(dataUser.getPassword())) {
            sysLoginLog.setMsg("登录名称和密码不匹配");
            sysLoginLogService.save(sysLoginLog);
            throw new CustomException("登录名称和密码不匹配");
        }

        sysLoginLog.setStatus(NormalStatus.NORMAL.getCode());
        sysLoginLogService.save(sysLoginLog);
        return dataUser;
    }

    /**
     * 更新用户
     *
     * @param sysUser 用户
     */
    @Override
    public void update(SysUser sysUser) {
        sysUserMapper.updateById(sysUser);
    }

    /**
     * 获得用户
     * 通过用户的登录名称，前端保存用户登录名称，时间，邮箱等视情况而定
     *
     * @param sysUser 用户
     * @return 用户详情
     */
    @Override
    public SysUser get(SysUser sysUser) {
        // 通过用户手机查询用户
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>(sysUser);
        SysUser dataUser = sysUserMapper.selectOne(queryWrapper);
        if (dataUser == null) {
            throw new CustomException("无效的登录信息，请重新登录");
        }
        return dataUser;
    }

    /**
     * 获得用户
     *
     * @param id id
     * @return user object
     */
    @Override
    public UserDTO get(Long id) {
        // First get user object by id
        SysUser sysUser = sysUserMapper.selectById(id);
        // Then get user's role
        List<SysRole> roles = sysRoleMapper.getRolesByUserId(id);
        //
        // put roles and resource into user object
        UserDTO sysUserDTO = UserDTO.builder().build();
        BeanUtils.copyProperties(sysUser, sysUserDTO);

        roles.forEach(item -> {
            if (RoleBase.ROLE_ADMIN.getRoleName().equals(item.getRoleName())) {
                // 如果是管理用户
                Set<String> resourceAll = new HashSet<>();
                resourceAll.add("*");
                sysUserDTO.setPermissions(resourceAll);
            }else {
                // 如果不是管理用户：从数据库里面读取
                // 获得角色
                Set<Long> roleIds = roles.stream().map(SysRole::getId).collect(Collectors.toSet());
                // 获取权限标识符
                Set<String> resourceCodes = sysResourceMapper.getResourceCodeByRoleIds(roleIds);
                sysUserDTO.setPermissions(resourceCodes);
            }
        });
        Set<String> roleNames = roles.stream().map(SysRole::getZhName).collect(Collectors.toSet());
        sysUserDTO.setRoles(roleNames);
        return sysUserDTO;
    }


    /**
     * 获得父节点
     *
     * @param parents 父节点id
     * @param all     所有资源
     * @return 父节点集合
     */
    private Set<SysResource> getByIds(Set<SysResource> parents, List<SysResource> all) {
        Set<Long> collect = parents.stream().map(SysResource::getParentId).collect(Collectors.toSet());
        return all.stream().filter(item -> collect.contains(item.getId())).collect(Collectors.toSet());
    }

    /**
     * 递归查找父节点
     *
     * @param parents 子节点
     * @param all     所有节点
     * @return 父节点集合
     */
    public Set<SysResource> findParent(Set<SysResource> parents, List<SysResource> all) {
        Set<SysResource> resources = new HashSet<>(getByIds(parents, all));
        if (resources.size() < 1) {
            return resources;
        }
        resources.addAll(findParent(resources, all));
        return resources;
    }


    @Override
    public List<RouterVO> getRouters(Long userId) {
        // 获得用户资源集合
        List<SysResource> userResources = sysResourceMapper.getResourcesByRoleIds(sysRoleMapper.getRoleIdsByUserId(userId));
        // 获得所有资源集合
        List<SysResource> all = sysResourceMapper.selectList(null);
        // 获得用户资源集合中可能存在的父资源
        Set<SysResource> parent = findParent(new HashSet<>(userResources), all);
        // 将父资源添加到用户资源集合
        parent.addAll(userResources);
        // 去掉资源集合中的Button资源
        parent.removeIf(item -> item.getType().equals(ResourceType.BUTTON));
        // 转换资源集合为List：使用Set可以去重
        List<SysResource> result = new ArrayList<>(parent);
        // 按照排序字段对其进行排序
        result.sort(Comparator.comparing(SysResource::getOrderNum));
        // 转换
        return RouterVO.parseRouter(result);
    }

    @Override
    public UserDetails loadUserByUsername(String loginName) throws UsernameNotFoundException {
        // 查询用户
        SysUser dataUser = sysUserMapper.selectOneByLoginName(loginName);
        Assert.notNull(dataUser, "user " + loginName + " not found in system");
        // 获得角色
        Set<Long> roleIds = sysRoleMapper.getRoleIdsByUserId(dataUser.getId());
        if (roleIds.contains(1L)) {
            Set<String> resourceAll = new HashSet<>();
            resourceAll.add("*");
            dataUser.setPermissions(resourceAll);
        } else {
            // 获取权限标识符
            Set<String> resourceCodes = sysResourceMapper.getResourceCodeByRoleIds(roleIds);
            dataUser.setPermissions(resourceCodes);
        }
        return dataUser;
    }
}
