package cn.fabhub.shopadmin.service.impl;

import cn.fabhub.shopadmin.entity.SysAccessLog;
import cn.fabhub.shopadmin.mapper.SysAccessLogMapper;
import cn.fabhub.shopadmin.query.OperLogQuery;
import cn.fabhub.shopadmin.service.SysAccessLogService;
import cn.fabhub.shopadmin.vo.PR;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author zhou
 * @date 2020-05-30 12:00
 */

@Service
public class SysAccessLogServiceImpl extends ServiceImpl<SysAccessLogMapper, SysAccessLog> implements SysAccessLogService {

    /**
     * 清空日志
     */
    @Override
    public void truncate() {
        this.baseMapper.truncate();
    }


    @Override
    public PR<SysAccessLog> list(OperLogQuery logQuery) {
        LambdaQueryWrapper<SysAccessLog> qw = new LambdaQueryWrapper<>();
        qw.orderByDesc(SysAccessLog::getId);

        if (logQuery.getBusinessType() != null) {
            qw.eq(SysAccessLog::getBusinessType, logQuery.getBusinessType());
        }

        if (logQuery.getStatus() != null) {
            qw.eq(SysAccessLog::getStatus, logQuery.getStatus());
        }

        if (logQuery.getStartTime() != null) {
            qw.ge(SysAccessLog::getAccessTime, logQuery.getStartTime());
        }

        if (logQuery.getEndTime() != null) {
            qw.le(SysAccessLog::getAccessTime, logQuery.getEndTime());
        }

        Page<SysAccessLog> pageRequest = new Page<>(logQuery.getPageNum(), logQuery.getPageSize());
        Page<SysAccessLog> page = this.page(pageRequest, qw);
        return PR.newInstance(page);
    }

    @Override
    public void saveDetails(SysAccessLog sysAccessLog) {
        this.save(sysAccessLog);
    }
}


