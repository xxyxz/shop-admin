package cn.fabhub.shopadmin.service.impl;

import cn.fabhub.shopadmin.entity.SysRole;
import cn.fabhub.shopadmin.entity.SysRoleResource;
import cn.fabhub.shopadmin.exception.CustomException;
import cn.fabhub.shopadmin.mapper.SysRoleMapper;
import cn.fabhub.shopadmin.mapper.SysRoleResourceMapper;
import cn.fabhub.shopadmin.query.RoleQuery;
import cn.fabhub.shopadmin.service.SysRoleService;
import cn.fabhub.shopadmin.vo.PR;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author zhou
 * @date 2020-05-30 11:56
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

    @Resource
    private SysRoleResourceMapper sysRoleResourceMapper;


    @Override
    public PR<SysRole> list(RoleQuery roleQuery) {
        LambdaQueryWrapper<SysRole> qw = new LambdaQueryWrapper<>();

        if (StringUtils.isNotBlank(roleQuery.getZhName())) {
            qw.like(SysRole::getZhName, roleQuery.getZhName());
        }

        if (StringUtils.isNotBlank(roleQuery.getRoleName())) {
            qw.like(SysRole::getRoleName, roleQuery.getRoleName());
        }

        if (roleQuery.getStartTime() != null) {
            qw.ge(SysRole::getCreateDate, roleQuery.getStartTime());
        }

        if (roleQuery.getEndTime() != null) {
            qw.le(SysRole::getCreateDate, roleQuery.getEndTime());
        }

        if (roleQuery.getStatus() != null) {
            qw.eq(SysRole::getStatus, roleQuery.getStatus());
        }

        Page<SysRole> pageRequest = new Page<>(roleQuery.getPageNum(), roleQuery.getPageSize());
        Page<SysRole> pageResult = this.page(pageRequest, qw);
        return PR.newInstance(pageResult);
    }

    @Override
    public SysRole getDetails(Long roleId) {
        // 获得基础信息
        SysRole result = this.getById(roleId);
        // 获得角色持有资源id集合：
        List<Long> resourceIds = sysRoleResourceMapper.getResourceIdByRoleId(roleId);
        result.setResourceIds(resourceIds);
        return result;
    }

    @Override
    @Transactional
    public void saveDetails(SysRole form) {
        // 保存基本信息
        // 角色编码转化为大写
        form.setRoleName(form.getRoleName().toUpperCase());
        this.save(form);
        // 给角色分配资源
        saveRelateToResource(form.getId(), form.getResourceIds());
    }

    private Boolean existOccupy(Collection<? extends Serializable> roleIds) {
        return this.baseMapper.countOccupy(roleIds) > 0;
    }

    @Override
    @Transactional
    public boolean removeByIds(Collection<? extends Serializable> idList) {
        // 检查是否有占用
        if (existOccupy(idList)) {
            throw new CustomException("存在资源占用，无法删除");
        }
        // 先删除角色和资源的关系
        sysRoleResourceMapper.deleteByRoleIds(idList);
        // 删除角色
        this.baseMapper.deleteBatchIds(idList);
        return true;
    }

    @Override
    @Transactional
    public void updateDetails(SysRole form) {
        // 更新基础信息
        form.setRoleName(form.getRoleName().toUpperCase());
        this.updateById(form);
        // 删除角色和资源的关联信息
        saveRelateToResource(form.getId(), form.getResourceIds());
    }

    private void saveRelateToResource(Long roleId, List<Long> resourceIds) {
        sysRoleResourceMapper.deleteByRoleId(roleId);
        if (!CollectionUtils.isEmpty(resourceIds)) {
            // 保存新的和资源的关联信息
            sysRoleResourceMapper.batchInsert(createRoleResourceList(roleId, resourceIds));
        }
    }

    public List<SysRoleResource> createRoleResourceList(Long roleId, List<Long> resourceIds) {
        List<SysRoleResource> result = new ArrayList<>();
        for (Long resourceId : resourceIds) {
            result.add(new SysRoleResource(roleId, resourceId));
        }
        return result;
    }
}

