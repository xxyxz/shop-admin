package cn.fabhub.shopadmin.service;

import cn.fabhub.shopadmin.entity.SysRole;
import cn.fabhub.shopadmin.query.RoleQuery;
import cn.fabhub.shopadmin.vo.PR;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author zhou
 * @date 2020-05-30 11:56
 */
public interface SysRoleService extends IService<SysRole> {


    void saveDetails(SysRole form);

    void updateDetails(SysRole form);

    PR<SysRole> list(RoleQuery roleQuery);

    SysRole getDetails(Long roleId);

}

