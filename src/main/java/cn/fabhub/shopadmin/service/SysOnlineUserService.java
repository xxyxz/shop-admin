package cn.fabhub.shopadmin.service;

import cn.fabhub.shopadmin.entity.SysUser;
import cn.fabhub.shopadmin.query.BaseQuery;

import java.util.List;

/**
 * @author zhou
 * @date 2020-09-30 12:55
 */
public interface SysOnlineUserService {

    List<SysUser> list(BaseQuery baseQuery);

    void forceExist(Long userId);

}
