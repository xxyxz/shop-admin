package cn.fabhub.shopadmin.controller;

import cn.fabhub.shopadmin.annotation.Log;
import cn.fabhub.shopadmin.constant.BusinessType;
import cn.fabhub.shopadmin.constant.ModelTitle;
import cn.fabhub.shopadmin.dto.UserDTO;
import cn.fabhub.shopadmin.entity.SysUser;
import cn.fabhub.shopadmin.factory.entity.SysUserFactory;
import cn.fabhub.shopadmin.query.UserQuery;
import cn.fabhub.shopadmin.service.SysUserService;
import cn.fabhub.shopadmin.util.CustomBeanUtils;
import cn.fabhub.shopadmin.validate.ValidationGroup;
import cn.fabhub.shopadmin.vo.PR;
import cn.fabhub.shopadmin.vo.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhou
 * @date 2020-05-30 10:38
 */
@Api(tags = "用户接口")
@RestController
@RequestMapping("/sys/user")
public class SysUserController {

    @Resource
    private SysUserService sysUserService;


    @ApiOperation(value = "添加用户")
    @Log(title = ModelTitle.USER_MANAGER, businessType = BusinessType.INSERT)
    @PreAuthorize("@ss.hasPermission('sys:user:add')")
    @PostMapping
    public R save(@Validated(ValidationGroup.Add.class) @RequestBody UserDTO sysUserDTO) {
        SysUser form = SysUserFactory.newInstance();
        CustomBeanUtils.copyPropertiesIgnoreNull(sysUserDTO, form);
        sysUserService.saveDetails(form);
        return R.ok();
    }

    @ApiOperation(value = "删除用户")
    @Log(title = ModelTitle.USER_MANAGER, businessType = BusinessType.DELETE)
    @PreAuthorize("@ss.hasPermission('sys:user:delete')")
    @DeleteMapping("/{ids}")
    public R delete(@PathVariable List<Long> ids) {
        sysUserService.removeByIds(ids);
        return R.ok();
    }

    @ApiOperation(value = "修改用户")
    @Log(title = ModelTitle.USER_MANAGER, businessType = BusinessType.UPDATE)
    @PreAuthorize("@ss.hasPermission('sys:user:update')")
    @PutMapping
    public R update(@Validated(ValidationGroup.Update.class) @RequestBody UserDTO sysUserDTO) {
        SysUser form = SysUserFactory.newInstance();
        CustomBeanUtils.copyPropertiesIgnoreNull(sysUserDTO, form);
        sysUserService.updateDetails(form);
        return R.ok();
    }

    @ApiOperation(value = "获得用户")
    @GetMapping("/{id}")
    public R getById(@PathVariable Long id) {
        SysUser user = sysUserService.getDetails(id);
        return R.ok(user);
    }

    @ApiOperation(value = "获得用户列表")
    @PreAuthorize("@ss.hasPermission('sys:user:list')")
    @GetMapping
    public R getList(UserQuery userQuery) {
        PR<SysUser> result = sysUserService.list(userQuery);
        return R.ok(result);
    }

    @ApiOperation(value = "导出用户")
    @Log(title = ModelTitle.USER_MANAGER, businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public void exportUser() {
        // todo
        System.out.println("导出用户");
    }

    @ApiOperation(value = "导入用户")
    @Log(title = ModelTitle.USER_MANAGER, businessType = BusinessType.IMPORT)
    @PostMapping("/import")
    public void importUser() {
        // todo
        System.out.println("导入用户");
    }

}
