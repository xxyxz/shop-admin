package cn.fabhub.shopadmin.controller;

import cn.fabhub.shopadmin.annotation.Log;
import cn.fabhub.shopadmin.constant.BusinessType;
import cn.fabhub.shopadmin.constant.ModelTitle;
import cn.fabhub.shopadmin.dto.RoleDTO;
import cn.fabhub.shopadmin.entity.SysRole;
import cn.fabhub.shopadmin.factory.entity.SysRoleFactory;
import cn.fabhub.shopadmin.query.RoleQuery;
import cn.fabhub.shopadmin.service.SysRoleService;
import cn.fabhub.shopadmin.util.Converter;
import cn.fabhub.shopadmin.util.CustomBeanUtils;
import cn.fabhub.shopadmin.validate.ValidationGroup;
import cn.fabhub.shopadmin.vo.PR;
import cn.fabhub.shopadmin.vo.R;
import cn.fabhub.shopadmin.vo.Select;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhou
 * @date 2020-05-30 19:30
 */
@Api(tags = "角色接口")
@RestController
@RequestMapping("/sys/role")
public class SysRoleController {

    @Resource
    private SysRoleService sysRoleService;


    @ApiOperation(value = "添加角色")
    @Log(title = ModelTitle.ROLE_MANAGER, businessType = BusinessType.INSERT)
    @PreAuthorize("@ss.hasPermission('sys:role:add')")
    @PostMapping
    public R save(@Validated(ValidationGroup.Add.class) @RequestBody RoleDTO roleDTO) {
        SysRole form = SysRoleFactory.newInstance();
        CustomBeanUtils.copyPropertiesIgnoreNull(roleDTO, form);
        sysRoleService.saveDetails(form);
        return R.ok();
    }

    @ApiOperation(value = "删除角色")
    @Log(title = ModelTitle.ROLE_MANAGER, businessType = BusinessType.DELETE)
    @PreAuthorize("@ss.hasPermission('sys:role:delete')")
    @DeleteMapping("/{ids}")
    public R deleteBatch(@PathVariable List<Long> ids) {
        sysRoleService.removeByIds(ids);
        return R.ok();
    }

    @ApiOperation(value = "修改角色")
    @Log(title = ModelTitle.ROLE_MANAGER, businessType = BusinessType.UPDATE)
    @PreAuthorize("@ss.hasPermission('sys:role:update')")
    @PutMapping
    public R update(@Validated(ValidationGroup.Update.class) @RequestBody RoleDTO roleDTO) {
        SysRole form = SysRoleFactory.newInstance();
        CustomBeanUtils.copyPropertiesIgnoreNull(roleDTO, form);
        sysRoleService.updateDetails(form);
        return R.ok();
    }

    @ApiOperation(value = "获得角色")
    @GetMapping("/{roleId}")
    public R get(@PathVariable Long roleId) {
        SysRole result = sysRoleService.getDetails(roleId);
        return R.ok(result);
    }

    @ApiOperation(value = "获得角色列表")
    @GetMapping
    public R getList(RoleQuery roleQuery) {

        if (roleQuery.getSimple()) {
            List<SysRole> list = sysRoleService.list();
            List<Select> result = new ArrayList<>();
            for (SysRole role: list) {
                result.add(Converter.toSimple(role));
            }
            return R.ok(result);
        }

        PR<SysRole> list = sysRoleService.list(roleQuery);
        return R.ok(list);
    }

    @ApiOperation(value = "更新角色权限")
    @Log(title = ModelTitle.ROLE_MANAGER, businessType = BusinessType.GRANT)
    @PutMapping("/resource")
    public R updateAuthority() {
        // todo 分配权限
        return R.ok();
    }

}
