package cn.fabhub.shopadmin.controller;

import cn.fabhub.shopadmin.annotation.Log;
import cn.fabhub.shopadmin.constant.BusinessType;
import cn.fabhub.shopadmin.dto.DictDataDTO;
import cn.fabhub.shopadmin.entity.SysDictData;
import cn.fabhub.shopadmin.query.DictDataQuery;
import cn.fabhub.shopadmin.service.SysDictDataService;
import cn.fabhub.shopadmin.util.CustomBeanUtils;
import cn.fabhub.shopadmin.validate.ValidationGroup;
import cn.fabhub.shopadmin.vo.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhou
 * @date 2020-08-19 21:28
 */
@Api(tags = "字典数据")
@RestController
@RequestMapping("/sys/dict/data")
public class SysDictDataController {

    @Resource
    private SysDictDataService sysDictDataService;

    @ApiOperation(value = "添加字典数据")
    @Log(title = "字典数据", businessType = BusinessType.INSERT)
    @PreAuthorize("@ss.hasPermission('sys:dict:add')")
    @PostMapping
    public R save(@Validated(ValidationGroup.Add.class) @RequestBody DictDataDTO dictDataDTO) {
        // 拷贝
        SysDictData form = SysDictData.builder().build();
        CustomBeanUtils.copyPropertiesIgnoreNull(dictDataDTO, form);
        // 校验是否可以用 todo
        sysDictDataService.save(form);
        return R.ok();
    }

    @ApiOperation(value = "删除字典数据")
    @Log(title = "字典数据", businessType = BusinessType.DELETE)
    @PreAuthorize("@ss.hasPermission('sys:dict:delete')")
    @DeleteMapping("/{ids}")
    public R delete(@PathVariable List<Long> ids) {
        sysDictDataService.removeByIds(ids);
        return R.ok();
    }

    @ApiOperation(value = "修改字段数据")
    @Log(title = "字典数据", businessType = BusinessType.UPDATE)
    @PreAuthorize("@ss.hasPermission('sys:dict:update')")
    @PutMapping
    public R update(@Validated(ValidationGroup.Patch.class) @RequestBody DictDataDTO dictDataDTO) {
        SysDictData form = SysDictData.builder().build();
        CustomBeanUtils.copyPropertiesIgnoreNull(dictDataDTO, form);
        sysDictDataService.updateById(form);
        return R.ok();
    }

    @ApiOperation(value = "获得字典数据")
    @GetMapping("/{id}")
    public R get(@PathVariable Long id) {
        SysDictData result = sysDictDataService.getById(id);
        return R.ok(result);
    }

    @ApiOperation(value = "获得字典数据")
    @GetMapping("/type/{dictType}")
    public R get(@PathVariable String dictType) {
        DictDataQuery query = new DictDataQuery().setDictType(dictType).setStatus(true);
        List<SysDictData> list = sysDictDataService.list(query);
        return R.ok(list);
    }


    @ApiOperation(value = "获得字典数据")
    @GetMapping
    public R getList(DictDataQuery dictDataQuery) {
        List<SysDictData> result = sysDictDataService.list(dictDataQuery);
        return R.ok(result);
    }
}
