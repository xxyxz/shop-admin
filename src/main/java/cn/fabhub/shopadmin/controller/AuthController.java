package cn.fabhub.shopadmin.controller;
import cn.fabhub.shopadmin.annotation.Log;
import cn.fabhub.shopadmin.constant.BusinessType;
import cn.fabhub.shopadmin.constant.FileConstant;
import cn.fabhub.shopadmin.dto.AuthUserDTO;
import cn.fabhub.shopadmin.dto.UserDTO;
import cn.fabhub.shopadmin.entity.SysUser;
import cn.fabhub.shopadmin.exception.CustomException;
import cn.fabhub.shopadmin.factory.TreeBuildFactory;
import cn.fabhub.shopadmin.factory.entity.SysUserFactory;
import cn.fabhub.shopadmin.service.AuthService;
import cn.fabhub.shopadmin.service.SysUserService;
import cn.fabhub.shopadmin.third.clould.AliyunOssUtils;
import cn.fabhub.shopadmin.util.CustomBeanUtils;
import cn.fabhub.shopadmin.util.TokenService;
import cn.fabhub.shopadmin.validate.ValidationGroup;
import cn.fabhub.shopadmin.vo.LoginUser;
import cn.fabhub.shopadmin.vo.R;
import cn.fabhub.shopadmin.vo.RouterVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author zhou
 * @date 2020-05-30 18:46
 */
@Api(tags = "用户认证接口")
@RestController
@RequestMapping("/auth")
public class AuthController {

    @Resource
    private SysUserService sysUserService;

    @Resource
    private AuthService authService;


    @ApiOperation(value = "注册用户")
    @PostMapping("/register")
    public R register(@Validated(ValidationGroup.Register.class) @RequestBody AuthUserDTO authUserDTO) {
        // todo 验证码
        SysUser form = SysUserFactory.newInstance();
        CustomBeanUtils.copyPropertiesIgnoreNull(authUserDTO, form);
        authService.register(form);
        return R.ok();
    }


    @ApiOperation(value = "用户登录")
    @PostMapping("/login")
    public R login(@Validated(ValidationGroup.Login.class) @RequestBody AuthUserDTO authUserDTO) {
        // 验证验证码是否正确 todo
        SysUser form = SysUserFactory.newInstance();
        CustomBeanUtils.copyPropertiesIgnoreNull(authUserDTO, form);
        SysUser login = authService.login(form);
        // 保存登录信息到redis
        String token = TokenService.createToken(login.getId(), login.getLoginName());
        LoginUser loginUser = new LoginUser(token);
        return R.ok(loginUser);
    }


    @ApiOperation(value = "获得用户详情")
    @GetMapping("/info")
    public R getInfo() {
        Long userId = TokenService.getUserId();
        UserDTO userDTO = authService.get(userId);
        // 获得请求头中的token
        return R.ok(userDTO);
    }


    /**
     * 获得route
     *
     * @return router
     */
    @ApiOperation(value = "获得路由")
    @GetMapping("/route")
    public R getRoutes() {
        // 获得 userID
        Long userId = TokenService.getUserId();
        // 获得路由列表
        List<RouterVO> routers = authService.getRouters(userId);
        return R.ok(new TreeBuildFactory<RouterVO>().buildTree(routers));
    }


    @ApiOperation(value = "用户退出")
    @PostMapping("/logout")
    public R logout() {
        return R.ok();
    }


    @ApiOperation(value = "修改用户头像")
    @Log(title = "用户认证", businessType = BusinessType.UPDATE)
    @PostMapping("/avatar")
    public R updateAvatar(@RequestParam("avatar") MultipartFile multipartFile) {
        InputStream inputStream = null;
        try {
            // 获得文件原始名称
            String filename = multipartFile.getOriginalFilename();
            inputStream = multipartFile.getInputStream();
            // 保存文件到阿里云oss
            String avatar = AliyunOssUtils.putObject(filename, inputStream);
            // 保存文件路径到avatar
            Long userId = TokenService.getUserId();
            SysUser older = SysUserFactory.newInstance();
            older.setId(userId)
                    .setAvatar(avatar);
            sysUserService.updateById(older);
            return R.ok(avatar);
        } catch (IOException e) {
            e.printStackTrace();
            throw new CustomException(FileConstant.NOT_FOUND_INPUT_STREAM);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
