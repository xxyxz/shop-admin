package cn.fabhub.shopadmin.controller;

import cn.fabhub.shopadmin.annotation.Log;
import cn.fabhub.shopadmin.constant.BusinessType;
import cn.fabhub.shopadmin.constant.ModelTitle;
import cn.fabhub.shopadmin.dto.DictTypeDTO;
import cn.fabhub.shopadmin.entity.SysDictType;
import cn.fabhub.shopadmin.query.DictQueryType;
import cn.fabhub.shopadmin.service.SysDictTypeService;
import cn.fabhub.shopadmin.validate.ValidationGroup;
import cn.fabhub.shopadmin.vo.PR;
import cn.fabhub.shopadmin.vo.R;
import cn.fabhub.shopadmin.vo.SelectedVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhou
 * @date 2020-05-30 21:20
 */
@Api(tags = "字典类型")
@RestController
@RequestMapping("/sys/dict")
public class SysDictTypeController {

    @Resource
    private SysDictTypeService sysDictTypeService;


    @ApiOperation(value = "添加字典")
    @Log(title = ModelTitle.DICT_MANAGER, businessType = BusinessType.INSERT)
    @PreAuthorize("@ss.hasPermission('sys:dict:add')")
    @PostMapping
    public R save(@Validated(ValidationGroup.Add.class) @RequestBody DictTypeDTO dictTypeDTO) {
        SysDictType form = SysDictType.builder().build();
        BeanUtils.copyProperties(dictTypeDTO, form);
        sysDictTypeService.save(form);
        return R.ok();
    }


    @ApiOperation(value = "删除字典")
    @Log(title = ModelTitle.DICT_MANAGER, businessType = BusinessType.DELETE)
    @PreAuthorize("@ss.hasPermission('sys:dict:delete')")
    @DeleteMapping("/{ids}")
    public R delete(@PathVariable List<Long> ids) {
        sysDictTypeService.removeByIds(ids);
        return R.ok();
    }


    @ApiOperation(value = "修改字典")
    @Log(title = ModelTitle.DICT_MANAGER, businessType = BusinessType.UPDATE)
    @PreAuthorize("@ss.hasPermission('sys:dict:update')")
    @PutMapping
    public R update(@Validated(ValidationGroup.Update.class) @RequestBody DictTypeDTO dictTypeDTO) {
        SysDictType form = SysDictType.builder().build();
        BeanUtils.copyProperties(dictTypeDTO, form);
        sysDictTypeService.updateDetails(form);
        return R.ok();
    }


    @ApiOperation(value = "修改顺序")
    @Log(title = ModelTitle.DICT_MANAGER, businessType = BusinessType.UPDATE)
    @PutMapping("/order")
    public R updateOrderNum(@RequestBody DictTypeDTO dictTypeDTO) {
        SysDictType form = SysDictType.builder().build().setId(dictTypeDTO.getId()).setOrderNum(dictTypeDTO.getOrderNum());
        sysDictTypeService.updateDetails(form);
        return R.ok();
    }


    @ApiOperation(value = "查询字典:id")
    @GetMapping("/{dictId}")
    public R get(@PathVariable Long dictId) {
        SysDictType result = sysDictTypeService.getById(dictId);
        return R.ok(result);
    }


    @ApiOperation(value = "查询字典类型")
    @GetMapping("/selected")
    public R get() {
        List<SysDictType> list = sysDictTypeService.list();
        List<SelectedVO> result = new ArrayList<>();
        for (SysDictType sysDictType : list) {
            result.add(new SelectedVO(sysDictType.getDictName(), sysDictType.getDictType()));
        }
        return R.ok(result);
    }


    @ApiOperation(value = "查询字典")
    @GetMapping
    public R getList(DictQueryType dictQueryType) {
        PR<SysDictType> result = sysDictTypeService.list(dictQueryType);
        return R.ok(result);
    }

}
