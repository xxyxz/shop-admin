package cn.fabhub.shopadmin.controller;

import cn.fabhub.shopadmin.annotation.Log;
import cn.fabhub.shopadmin.constant.BusinessType;
import cn.fabhub.shopadmin.constant.ModelTitle;
import cn.fabhub.shopadmin.service.OnlineUserService;
import cn.fabhub.shopadmin.vo.R;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author zhou
 * @date 2020-09-29 20:36
 */
@RestController
@RequestMapping("/sys/online")
public class SysOnlineUserController {

    @Resource
    private OnlineUserService onlineUserService;


    @Log(title = ModelTitle.ONLINE_USER_MANAGER, businessType = BusinessType.GET)
    @GetMapping
    public R list() {
        return R.ok(onlineUserService.list());
    }


    @DeleteMapping("/exist")
    public R forceExist() {
        return R.ok();
    }

}
