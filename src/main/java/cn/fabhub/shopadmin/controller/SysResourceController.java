package cn.fabhub.shopadmin.controller;

import cn.fabhub.shopadmin.annotation.Log;
import cn.fabhub.shopadmin.constant.BusinessType;
import cn.fabhub.shopadmin.constant.ModelTitle;
import cn.fabhub.shopadmin.dto.ResourceDTO;
import cn.fabhub.shopadmin.entity.SysResource;
import cn.fabhub.shopadmin.factory.TreeBuildFactory;
import cn.fabhub.shopadmin.factory.entity.SysResourceFactory;
import cn.fabhub.shopadmin.query.ResourceQuery;
import cn.fabhub.shopadmin.service.SysResourceService;
import cn.fabhub.shopadmin.util.CustomBeanUtils;
import cn.fabhub.shopadmin.validate.ValidationGroup;
import cn.fabhub.shopadmin.vo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhou
 * @date 2020-05-30 22:15
 */
@Api(tags = "资源接口")
@RestController
@RequestMapping("/sys/resource")
public class SysResourceController {

    @Resource
    private SysResourceService sysResourceService;


    @ApiOperation(value = "添加资源")
    @Log(title = "资源管理", businessType = BusinessType.INSERT)
    @PreAuthorize("@ss.hasPermission('sys:resource:add')")
    @PostMapping
    public R save(@Validated(ValidationGroup.Add.class) @RequestBody ResourceDTO resourceDTO) {
        SysResource form = SysResourceFactory.newInstance();
        CustomBeanUtils.copyPropertiesIgnoreNull(resourceDTO, form);
        sysResourceService.save(form);
        return R.ok();
    }

    @ApiOperation(value = "扫描系统资源")
    @Log(title = ModelTitle.RESOURCE_MANAGER, businessType = BusinessType.SCAN_SYSTEM)
    @PreAuthorize("@ss.hasPermission('sys:resource:scan')")
    @PutMapping("/scan")
    public R scanApiToSystem() {
        return R.ok();
    }


    @ApiOperation(value = "删除资源：id")
    @Log(title = "资源管理", businessType = BusinessType.DELETE)
    @PreAuthorize("@ss.hasPermission('sys:resource:delete')")
    @DeleteMapping("/{id}")
    public R delete(@PathVariable Long id) {
        sysResourceService.removeChecked(id);
        return R.ok();
    }


    @ApiOperation(value = "删除资源：batch")
    @Log(title = "资源管理", businessType = BusinessType.DELETE)
    @PreAuthorize("@ss.hasPermission('sys:resource:delete')")
    @DeleteMapping
    public R delete(List<Long> ids) {
        sysResourceService.removeByIds(ids);
        return R.ok();
    }


    @ApiOperation(value = "修改资源")
    @Log(title = "资源管理", businessType = BusinessType.UPDATE)
    @PreAuthorize("@ss.hasPermission('sys:resource:update')")
    @PutMapping
    public R delete(@Validated(ValidationGroup.Update.class) @RequestBody ResourceDTO resourceDTO) {
        SysResource form = SysResourceFactory.newInstance();
        CustomBeanUtils.copyPropertiesIgnoreNull(resourceDTO, form);
        sysResourceService.updateDetails(form);
        return R.ok();
    }


    @ApiOperation(value = "查询资源：id")
    @GetMapping("/{id}")
    public R getById(@PathVariable Long id) {
        SysResource resource = sysResourceService.getById(id);
        return R.ok(resource);
    }


    @ApiOperation(value = "查询资源")
    @GetMapping
    public R getList(ResourceQuery resourceQuery) {

        List<SysResource> list = sysResourceService.list(resourceQuery);
        // 数据为表格形式
        if (resourceQuery.isTable()) {
            return R.ok(new TreeBuildFactory<SysResource>().buildTree(list));
        } else {
            //  数据是下拉菜单形式
            List<SelectedTreeNode> selectedTreeNodes = sysResourceService.toSelectedTreeNodeList(list);
            // 是否添加一个最大根节点
            if (resourceQuery.isHasRoot()) {
                selectedTreeNodes.add(new SelectedTreeNode(0L, "主目录", -1L));
            }
            return R.ok(new TreeBuildFactory<SelectedTreeNode>().buildTree(selectedTreeNodes));
        }
    }

}
