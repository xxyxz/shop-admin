package cn.fabhub.shopadmin.controller;

import cn.fabhub.shopadmin.vo.R;
import cn.fabhub.shopadmin.vo.server.Server;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

/**
 * @author zhou
 * @date 2020-07-28 19:06
 */
@Api(tags = "服务器")
@RestController
@RequestMapping("/server")
public class ServerController {

    @ApiOperation(value = "获取机器")
    @GetMapping
    public R getInfo() {
        Server server = new Server();
        server.copyTo();
        return R.ok(server);
    }
}
