package cn.fabhub.shopadmin.controller;

import cn.fabhub.shopadmin.annotation.Log;
import cn.fabhub.shopadmin.constant.BusinessType;
import cn.fabhub.shopadmin.entity.SysAccessLog;
import cn.fabhub.shopadmin.query.OperLogQuery;
import cn.fabhub.shopadmin.service.SysAccessLogService;
import cn.fabhub.shopadmin.vo.PR;
import cn.fabhub.shopadmin.vo.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhou
 * @date 2020-05-30 21:31
 */
@Api(tags = "操作日志")
@RestController
@RequestMapping("/sys/operlog")
public class SysAccessLogController {

    @Resource
    private SysAccessLogService sysAccessLogService;


    @ApiOperation(value = "删除操作日志")
    @Log(title = "日志管理", businessType = BusinessType.DELETE)
    @PreAuthorize("@ss.hasPermission('sys:operlog:delete')")
    @DeleteMapping("/{ids}")
    public R delete(@PathVariable List<Long> ids) {
        sysAccessLogService.removeByIds(ids);
        return R.ok();
    }

    @ApiOperation(value = "清空操作日志")
    @Log(title = "日志管理", businessType = BusinessType.TRUNCATE)
    @PreAuthorize("@ss.hasPermission('sys:operlog:truncate')")
    @DeleteMapping("/truncate")
    public R delete() {
        sysAccessLogService.truncate();
        return R.ok();
    }

    @ApiOperation(value = "查询操作日志")
    @GetMapping
    public R getList(OperLogQuery logQuery) {
        PR<SysAccessLog> list = sysAccessLogService.list(logQuery);
        return R.ok(list);
    }

}
