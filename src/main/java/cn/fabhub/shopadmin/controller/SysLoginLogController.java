package cn.fabhub.shopadmin.controller;

import cn.fabhub.shopadmin.annotation.Log;
import cn.fabhub.shopadmin.constant.BusinessType;
import cn.fabhub.shopadmin.entity.SysLoginLog;
import cn.fabhub.shopadmin.query.LoginLogQuery;
import cn.fabhub.shopadmin.service.SysLoginLogService;
import cn.fabhub.shopadmin.vo.PR;
import cn.fabhub.shopadmin.vo.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhou
 * @date 2020-08-19 13:12
 */
@Api(tags = "登录日志")
@RestController
@RequestMapping("/monitor/loginlog")
public class SysLoginLogController {

    @Resource
    private SysLoginLogService sysLoginLogService;


    @ApiOperation(value = "删除登录日志")
    @Log(title = "登录日志", businessType = BusinessType.DELETE)
    @PreAuthorize("@ss.hasPermission('sys:loginlog:delete')")
    @DeleteMapping("/{ids}")
    public R delete(@PathVariable List<Long> ids) {
        sysLoginLogService.removeByIds(ids);
        return R.ok();
    }

    @ApiOperation(value = "清空登录日志")
    @Log(title = "登录日志", businessType = BusinessType.TRUNCATE)
    @PreAuthorize("@ss.hasPermission('sys:loginlog:truncate')")
    @DeleteMapping("/truncate")
    public R truncate() {
        sysLoginLogService.truncate();
        return R.ok();
    }

    @ApiOperation(value = "获得登录日志")
    @GetMapping
    public R getList(LoginLogQuery loginLogQuery) {
        PR<SysLoginLog> result = sysLoginLogService.list(loginLogQuery);
        return R.ok(result);
    }

}
