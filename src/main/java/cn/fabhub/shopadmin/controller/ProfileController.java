package cn.fabhub.shopadmin.controller;

import cn.fabhub.shopadmin.annotation.Log;
import cn.fabhub.shopadmin.constant.BusinessType;
import cn.fabhub.shopadmin.vo.R;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhou
 * @date 2020-08-25 11:26
 */
@Api(tags = "个人信息")
@RestController
@RequestMapping("/profile")
public class ProfileController {


    @Log(title = "个人设置", businessType = BusinessType.UPDATE)
    @GetMapping
    public R getProfile() {
        return R.ok();
    }


    @Log(title = "个人设置", businessType = BusinessType.UPDATE)
    @PutMapping
    public R updateProfile() {
        return R.ok();
    }


}
