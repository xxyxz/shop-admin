package cn.fabhub.shopadmin.controller;

import cn.fabhub.shopadmin.annotation.Log;
import cn.fabhub.shopadmin.constant.BusinessType;
import cn.fabhub.shopadmin.vo.R;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhou
 * @date 2020-08-19 15:11
 */
@Api(tags = "公共接口")
@RestController
@RequestMapping("/common")
public class CommonController {


    @Log(title = "文件上传", businessType = BusinessType.INSERT)
    @PostMapping("/upload")
    public R uploadFile() {

        return R.ok();
    }


    @Log(title = "文件下载", businessType = BusinessType.EXPORT)
    @GetMapping("/download")
    public void downloadFile() {

    }
}
