package cn.fabhub.shopadmin.controller;

import cn.fabhub.shopadmin.annotation.Log;
import cn.fabhub.shopadmin.constant.BusinessType;
import cn.fabhub.shopadmin.constant.FileStorage;
import cn.fabhub.shopadmin.constant.ModelTitle;
import cn.fabhub.shopadmin.entity.SysFileInfo;
import cn.fabhub.shopadmin.query.FileInfoQuery;
import cn.fabhub.shopadmin.service.SysFileInfoService;
import cn.fabhub.shopadmin.third.clould.AliyunOssUtils;
import cn.fabhub.shopadmin.util.ServletUtils;
import cn.fabhub.shopadmin.util.TokenService;
import cn.fabhub.shopadmin.vo.PR;
import cn.fabhub.shopadmin.vo.R;
import io.swagger.annotations.Api;
import org.apache.commons.io.IOUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/12 17:53
 */
@Api(tags = "文件管理")
@RestController
@RequestMapping("/file")
public class SysFileInfoController {

    @Resource
    private SysFileInfoService sysFileInfoService;

    /**
     * 多文件上传
     *
     * @return 结果
     */
    @Log(title = ModelTitle.FILE_MANAGER, businessType = BusinessType.INSERT)
    @PostMapping("/uploads")
    public R uploadFiles(@RequestParam("files") MultipartFile[] multipartFile) {
        System.err.println(multipartFile.length);
        return R.ok();
    }


    @Log(title = ModelTitle.FILE_MANAGER, businessType = BusinessType.INSERT)
    @PostMapping("/upload")
    public R uploadFile(@RequestParam("file") MultipartFile multipartFile) {
        InputStream inputStream = null;
        try {
            // 获得原始文件名称
            String originalFilename = multipartFile.getOriginalFilename();
            // 获得文件流
            inputStream = multipartFile.getInputStream();
            // 保存文件到阿里云
            String accessPath = AliyunOssUtils.putObject(originalFilename, inputStream);
            // 获得用户Id
            long size = multipartFile.getSize();
            Long userId = TokenService.getUserId();
            // 创建文件表单对象
            SysFileInfo sysFileInfo = new SysFileInfo();
            sysFileInfo.setFilename(originalFilename)
                    .setAccessPath(accessPath)
                    .setCreateBy(userId)
                    .setUpdateBy(userId)
                    .setFileSize(size)
                    .setStorage(FileStorage.ALIYUN_OSS.getCode());
            // 获得文件类型：从阿里云
            String type = AliyunOssUtils.getObjectMetadata(originalFilename).getContentType();
            sysFileInfo.setType(type);
            // 保存
            sysFileInfoService.saveAsync(sysFileInfo);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } finally {
            try {
                assert inputStream != null;
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return R.ok();
    }


    @Log(title = ModelTitle.FILE_MANAGER, businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R delete(@PathVariable List<Long> ids) {
        sysFileInfoService.removeByIds(ids);
        return R.ok();
    }


    /**
     * 获得文件列表
     * @param fileInfoQuery 条件
     * @return 结果
     */
    @GetMapping
    public R getList(FileInfoQuery fileInfoQuery) {
        PR<SysFileInfo> list = sysFileInfoService.list(fileInfoQuery);
        return R.ok(list);
    }


    /**
     * 下载文件
     *
     */
    @GetMapping("/download/{id}")
    public void downloadFile(@PathVariable Long id) {
        // 获得文件信息
        SysFileInfo fileInfo = sysFileInfoService.getById(id);
        // 获得文件输入流
        InputStream inputStream = AliyunOssUtils.getObject(fileInfo.getFilename());
        // 获得Response
        HttpServletResponse response = ServletUtils.getResponse();
        // 清空Response
        response.reset();
        // 设置响应头
        response.setContentType("application/x-download; charset=utf-8");
        response.setHeader("Content-Disposition", "attachment; filename=" + fileInfo.getFilename());
        // 拷贝文件流
        try {
            IOUtils.copy(inputStream, response.getOutputStream());
            response.getOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
