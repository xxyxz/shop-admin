package cn.fabhub.shopadmin.constant;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/17 22:04
 */
public enum JobStatus {

    /**
     * 禁用
     */
    DISABLE(0),

    /**
     * 等待中
     */
    WAITING(1),

    /**
     * 运行中
     */
    RUNNING(2),

    /**
     * 暂停中
     */
    PAUSING(3),

    /**
     * 已经完成
     */
    COMPLETED(4)



    ;
    private final Integer code;

    JobStatus(int code){
        this.code = code;
    }

    public Integer getCode() {
        return this.code;
    }
}
