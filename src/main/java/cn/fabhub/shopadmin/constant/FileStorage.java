package cn.fabhub.shopadmin.constant;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/12 19:38
 */
public enum FileStorage {

    ALIYUN_OSS(1)
    ;

    public final Integer code;

    FileStorage(int code) {
        this.code = code;
    }

    public Integer getCode() {
        return this.code;
    }
}
