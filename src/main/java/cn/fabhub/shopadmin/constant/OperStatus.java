package cn.fabhub.shopadmin.constant;

/**
 * @author zhou
 * @date 2020-08-26 18:46
 */
public enum OperStatus {
    /**
     * 失败
     */
    FAIL(0),

    /**
     * 成功
     */
    SUCCESS(1);

    private final Integer code;

    OperStatus(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return this.code;
    }
}
