package cn.fabhub.shopadmin.constant;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/10/19 17:03
 */
public interface ModelTitle {

    String USER_MANAGER = "用户管理";

    String ROLE_MANAGER = "角色管理";

    String RESOURCE_MANAGER = "资源管理";

    String DICT_MANAGER = "字典管理";

    String FILE_MANAGER = "文件管理";

    String DICT_DATA_MANAGER = "字典数据管理";

    String LOGIN_LOG_MANAGER = "登录日志管理";

    String OPER_LOG_MANAGER = "操作日志管理";

    String ONLINE_USER_MANAGER = "在线用户管理";
}
