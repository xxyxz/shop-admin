package cn.fabhub.shopadmin.constant;

/**
 * 业务操作类型
 *
 * @author zhou
 */
public interface BusinessType {

    /**
     * 不明确的操作
     */
    int UNKNOWN = 0;
    
    /**
     * 新增
     */
     int INSERT =  1;

    /**
     * 删除
     */
     int DELETE = 2;

    /**
     * 修改
     */
     int UPDATE = 3;

    /**
     * 查询
     */
     int GET = 4;

    /**
     * 授权
     */
     int GRANT = 5;

    /**
     * 导出
     */
     int EXPORT = 6;

    /**
     * 导入
     */
     int IMPORT = 7;

    /**
     * 扫描
     */
     int SCAN_SYSTEM = 8;

    /**
     * 清空数据
     */
     int TRUNCATE = 9;

}
