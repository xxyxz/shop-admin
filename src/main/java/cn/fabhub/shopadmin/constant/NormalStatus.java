package cn.fabhub.shopadmin.constant;

/**
 * @author zhou
 * @date 2020-08-26 18:44
 */
public enum  NormalStatus {

    /**
     * 禁用
     */
    DISABLE(0),

    /**
     * 正常
     */
    NORMAL(1);


    private final Integer code;

    NormalStatus(Integer code) {
        this.code = code;
    }

    public java.lang.Integer getCode() {
        return this.code;
    }
}
