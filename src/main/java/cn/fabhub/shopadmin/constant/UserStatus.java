package cn.fabhub.shopadmin.constant;

/**
 * @author zhou
 * @date 2020-09-03 22:13
 */
public enum UserStatus {

    DISABLE(0),

    NORMAL(1),

    EXPIRE(2),

    LOCK(3),

    CREDENTIALS_EXPIRE(4)
    ;

    private final Integer code;

    UserStatus(int code) {
        this.code = code;
    }

    public Integer getCode() {
        return this.code;
    }

}
