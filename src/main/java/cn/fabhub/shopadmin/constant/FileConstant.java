package cn.fabhub.shopadmin.constant;

/**
 * @author zhou
 * @date 2020-08-24 21:30
 */
public interface FileConstant {

    String UPLOAD_FILE_ERROR = "上传文件异常";

    String FILE_ALREADY_EXIST = "上传文件已经存在";

    String SAVE_FILE_ERROR = "保存文件失败";

    String NOT_FOUND_INPUT_STREAM = "没有文件输入流";

    String SAVE_FILE_TO_ALIYUN_OSS = "保存文件到阿里OSS失败";
}
