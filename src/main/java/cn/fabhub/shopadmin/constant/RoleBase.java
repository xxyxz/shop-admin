package cn.fabhub.shopadmin.constant;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/25 12:32
 */
public enum RoleBase {

    /**
     * 管理员角色
     */
    ROLE_ADMIN("ROLE_ADMIN"),

    /**
     * 普通用户
     */
    ROLE_USER("ROLE_USER"),

    ;
    private  final  String roleName;

    RoleBase(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }
}
