package cn.fabhub.shopadmin.constant;

/**
 * @author zhou
 * @date 2020-09-04 17:33
 */
public enum ResourceType {
    CATALOGUE(1),
    MENU(2),
    BUTTON(3);

    public final Integer code;

    ResourceType(int code) {
        this.code = code;
    }

    public Integer getCode() {
        return this.code;
    }
}
