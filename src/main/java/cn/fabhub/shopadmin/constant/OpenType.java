package cn.fabhub.shopadmin.constant;

/**
 * @author zhou
 * @date 2020-09-14 13:50
 */
public enum OpenType {
    COMPONENT(1),

    INNER_LINK( 2),

    EXTERNAL_LINK(3);

    public final Integer code;

    OpenType(int code) {
        this.code = code;
    }

    public Integer getCode() {
        return this.code;
    }
}
