package cn.fabhub.shopadmin.annotation;

import cn.fabhub.shopadmin.constant.BusinessType;

import java.lang.annotation.*;

/**
 * 自定义操作日志注解
 *
 * @author zhou
 * @date 2020-08-10 18:30
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {

    String title() default "";

    int businessType() default BusinessType.UNKNOWN;

    boolean isSaveParameters() default true;
}
