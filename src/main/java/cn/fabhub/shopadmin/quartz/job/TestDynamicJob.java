package cn.fabhub.shopadmin.quartz.job;

import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/17 23:30
 */
public class TestDynamicJob extends QuartzJobBean {

    private static final Logger logger = LoggerFactory.getLogger(TestDynamicJob.class);

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) {
        logger.error("Hello quartz" + jobExecutionContext.getJobDetail().getJobClass());
    }
}
