package cn.fabhub.shopadmin.quartz.job;

import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/17 17:52
 */
public class TestJob extends QuartzJobBean {

    private static final Logger logger = LoggerFactory.getLogger(TestJob.class);

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) {
        logger.error("Hello quartz" + jobExecutionContext.getJobDetail().getJobClass());
    }
}
