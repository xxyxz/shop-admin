package cn.fabhub.shopadmin.quartz.job;

import cn.fabhub.shopadmin.spider.nwpu.SpiderStarter;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/18 20:53
 */
@DisallowConcurrentExecution
public class AutoFillNPU extends QuartzJobBean {

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) {
        System.out.println(jobExecutionContext.getTrigger().getNextFireTime());
        new SpiderStarter().startOne();
    }
}
