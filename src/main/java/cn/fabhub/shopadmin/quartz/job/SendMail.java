package cn.fabhub.shopadmin.quartz.job;

import cn.fabhub.shopadmin.mail.MailService;
import org.quartz.JobExecutionContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

import javax.annotation.Resource;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/19 12:03
 */
public class SendMail extends QuartzJobBean {

    @Resource
    private MailService mailService;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) {
        if (mailService == null) {
            System.err.println("邮件注入失败！");
            return;
        }
        mailService.sendSimpleMail("1226105312@qq.com", "自动发送邮件测试", "测试类是：" + jobExecutionContext.getJobDetail().getJobClass());
    }
}
