package cn.fabhub.shopadmin.quartz.service;

import cn.fabhub.shopadmin.quartz.controller.JobQuery;
import cn.fabhub.shopadmin.quartz.entity.SysJob;
import cn.fabhub.shopadmin.vo.PR;
import com.baomidou.mybatisplus.extension.service.IService;
import org.quartz.SchedulerException;

/**
 * @author  Mr.zhou
 * @date  2020/11/17 21:29
 * @version 1.0
 */
public interface SysJobService extends IService<SysJob> {

    /**
     * 初始化任务调度
     */
    void initSchedule() throws SchedulerException, IllegalAccessException, InstantiationException, ClassNotFoundException;

    /**
     * 保存任务
     * @param sysJob 任务
     */
    void saveDetails(SysJob sysJob);

    /**
     * 触发任务
     */
    void triggerJob(SysJob sysJob) throws SchedulerException, ClassNotFoundException;

    /**
     * 暂停任务
     */
    void pauseJob(SysJob sysJob) throws SchedulerException;

    /**
     * 恢复任务
     */
    void resumeJob(SysJob sysJob) throws SchedulerException;

    /**
     * 删除任务
     */
    void deleteJob(SysJob sysJob) throws SchedulerException;

    /**
     * 删除任务
     * @param forever 是否永久删除任务：永久删除任务，还会从数据库中删除记录
     */
    void deleteJob(SysJob sysJob, boolean forever) throws SchedulerException;

    /**
     * 获得Job列表
     * @param jobQuery 查询参数
     * @return 列表
     */
    PR<SysJob> list(JobQuery jobQuery);

    /**
     * 更新任务
     */
    void updateDetails(SysJob sysJob) throws SchedulerException, ClassNotFoundException;
}
