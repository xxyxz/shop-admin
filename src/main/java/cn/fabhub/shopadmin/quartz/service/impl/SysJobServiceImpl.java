package cn.fabhub.shopadmin.quartz.service.impl;

import cn.fabhub.shopadmin.constant.JobStatus;
import cn.fabhub.shopadmin.mapper.SysJobMapper;
import cn.fabhub.shopadmin.quartz.controller.JobQuery;
import cn.fabhub.shopadmin.quartz.entity.SysJob;
import cn.fabhub.shopadmin.quartz.service.SysJobService;
import cn.fabhub.shopadmin.vo.PR;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.quartz.*;
import org.springframework.data.annotation.Transient;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/17 21:29
 */
@Service
public class SysJobServiceImpl extends ServiceImpl<SysJobMapper, SysJob> implements SysJobService {

    @Resource
    private Scheduler scheduler;

    @Resource
    private SysJobService sysJobService;


    /**
     * 初始化任务调度
     */
    @Override
    public void initSchedule() throws SchedulerException {
        // 读取数据库
        List<SysJob> list = this.list();
        for (SysJob sysJob : list) {
            if (JobStatus.RUNNING.getCode().equals(sysJob.getStatus())) {
                sysJobService.resumeJob(sysJob);
            }
        }
    }

    @Override
    public void saveDetails(SysJob sysJob) {
        sysJob.setStatus(JobStatus.WAITING.getCode());
        this.save(sysJob);
    }

    /**
     * 触发任务
     */
    @Override
    public void triggerJob(SysJob sysJob) throws SchedulerException, ClassNotFoundException {
        isExistJob(sysJob);
        // 创建Job实现类
        Class<? extends Job> aClass = (Class<? extends Job>) Class.forName(sysJob.getJobClassName());

        JobKey jobKey = JobKey.jobKey(sysJob.getJobName(), sysJob.getJobGroup());
        // 创建JobDetail
        JobDetail jobDetail = JobBuilder.newJob(aClass)
                .withIdentity(jobKey)
                .build();

        // 定义触发规则
        Trigger trigger = TriggerBuilder.newTrigger()
                .forJob(jobDetail)
                .startNow()
                .withSchedule(CronScheduleBuilder.cronSchedule(sysJob.getCronExpression()))
                .build();

        // 将作业和触发器添加到调度器中
        scheduler.scheduleJob(jobDetail, trigger);
        // 启动
        if (!scheduler.isShutdown()) {
            scheduler.start();
        }
        // 更新状态
        this.getBaseMapper().updateStatus(sysJob.getJobName(), sysJob.getJobGroup(), JobStatus.RUNNING.getCode());
    }


    private void isExistJob(SysJob sysJob) throws SchedulerException {
        try {
            TriggerKey triggerKey = TriggerKey.triggerKey(sysJob.getJobName(), sysJob.getJobGroup());
            scheduler.getTrigger(triggerKey);
        } catch (SchedulerException e) {
            throw new SchedulerException("没有此任务");
        }
    }

    /**
     * 暂停任务
     */
    @Transient
    @Override
    public void pauseJob(SysJob sysJob) throws SchedulerException {
        // 任务存在是否存在
        isExistJob(sysJob);
        JobKey jobKey = JobKey.jobKey(sysJob.getJobName(), sysJob.getJobGroup());
        // 暂停
        scheduler.pauseJob(jobKey);
        // 更新状态
        this.getBaseMapper().updateStatus(sysJob.getJobName(), sysJob.getJobGroup(), JobStatus.PAUSING.getCode());
    }

    /**
     * 恢复任务
     */
    @Override
    public void resumeJob(SysJob sysJob) throws SchedulerException {
        isExistJob(sysJob);
        JobKey jobKey = JobKey.jobKey(sysJob.getJobName(), sysJob.getJobGroup());
        scheduler.resumeJob(jobKey);
        // 更新状态
        this.getBaseMapper().updateStatus(sysJob.getJobName(), sysJob.getJobGroup(), JobStatus.RUNNING.getCode());
    }

    /**
     * 删除任务：仅仅是从调度器中删除任务
     */
    @Override
    public void deleteJob(SysJob sysJob) throws SchedulerException {
        TriggerKey triggerKey = TriggerKey.triggerKey(sysJob.getJobName(), sysJob.getJobGroup());
        // 停止触发器
        scheduler.pauseTrigger(triggerKey);
        // 移除触发器
        scheduler.unscheduleJob(triggerKey);
        // 移除任务
        scheduler.deleteJob(JobKey.jobKey(sysJob.getJobName(), sysJob.getJobGroup()));
    }

    /**
     * 删除任务
     *
     * @param forever 是否永久删除任务：永久删除任务，还会从数据库中删除记录
     */
    @Override
    public void deleteJob(SysJob sysJob, boolean forever) throws SchedulerException {
        this.deleteJob(sysJob);
        if (forever) {
            this.getBaseMapper().delete(sysJob.getJobName(), sysJob.getJobGroup());
        }
    }

    /**
     * 获得Job列表
     *
     * @param jobQuery 查询参数
     * @return 列表
     */
    @Override
    public PR<SysJob> list(JobQuery jobQuery) {
        // 创建分页
        Page<SysJob> pageRequest = new Page<>(jobQuery.getPageNum(), jobQuery.getPageSize());
        // 请求
        Page<SysJob> page = this.page(pageRequest);
        return PR.newInstance(page);
    }

    /**
     * 更新任务
     * 先从调度器里面删除任务，如果是运行中，则继续运行
     */
    @Override
    public void updateDetails(SysJob sysJob) throws SchedulerException, ClassNotFoundException {
        // 从调度器里面删除任务
        this.deleteJob(sysJob);
        // 更新
        this.updateById(sysJob);
        // 如果在运行中：继续运行
        if (JobStatus.RUNNING.getCode().equals(sysJob.getStatus())) {
            this.triggerJob(sysJob);
        }
    }

}
