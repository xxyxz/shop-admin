package cn.fabhub.shopadmin.quartz.listener;

import cn.fabhub.shopadmin.quartz.service.SysJobService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/18 12:32
 */
@Component
@Order(value = 1)
public class ScheduleJobInitListener implements CommandLineRunner {


    @Resource
    private SysJobService sysJobService;


    @Override
    public void run(String... args) throws Exception {
        System.err.println("启动初始化");
        sysJobService.initSchedule();
    }
}
