package cn.fabhub.shopadmin.quartz.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author  Mr.zhou
 * @date  2020/11/17 21:29
 * @version 1.0
 */
@Data
@NoArgsConstructor
public class SysJob {
    /**
    * 自增主键
    */
    private Long id;

    /**
    * 任务名称
    */
    private String jobName;

    /**
    * 任务分组
    */
    private String jobGroup;

    /**
    * 执行类
    */
    private String jobClassName;

    /**
    * cron表达式
    */
    private String cronExpression;

    /**
    * 任务状态
    */
    private String triggerState;

    /**
     * 任务状态
     */
    private Integer status;

    /**
    * 修改之前的任务名称
    */
    private String oldJobName;

    /**
    * 修改之前的任务分组
    */
    private String oldJobGroup;

    /**
    * 描述
    */
    private String description;
}