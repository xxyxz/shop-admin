package cn.fabhub.shopadmin.quartz.controller;

import cn.fabhub.shopadmin.query.BaseQuery;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/17 22:32
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class JobQuery extends BaseQuery {

    private String jobName;

    private String jobGroup;

}
