package cn.fabhub.shopadmin.quartz.controller;

import cn.fabhub.shopadmin.quartz.entity.SysJob;
import cn.fabhub.shopadmin.quartz.service.SysJobService;
import cn.fabhub.shopadmin.quartz.utils.CronUtils;
import cn.fabhub.shopadmin.quartz.utils.JobClassUtils;
import cn.fabhub.shopadmin.vo.PR;
import cn.fabhub.shopadmin.vo.R;
import cn.fabhub.shopadmin.vo.Select;
import org.quartz.SchedulerException;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.Set;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/17 21:13
 */
@RestController
@RequestMapping("/job")
public class JobController {

    @Resource
    private SysJobService sysJobService;


    /**
     * 获得任务列表
     *
     * @param jobQuery 查询参数
     * @return 结果
     */
    @GetMapping
    public R get(JobQuery jobQuery) {
        PR<SysJob> pr = sysJobService.list(jobQuery);
        return R.ok(pr);
    }

    /**
     * 添加任务
     *
     * @param sysJob 任务
     * @return 结果
     */
    @PostMapping
    public R save(@RequestBody SysJob sysJob) {
        sysJobService.saveDetails(sysJob);
        return R.ok();
    }

    /**
     * 更新任务
     */
    @PutMapping
    public R update(@RequestBody SysJob sysJob) throws ClassNotFoundException, SchedulerException {
        sysJobService.updateDetails(sysJob);
        return R.ok();
    }

    /**
     * 获得任务
     */
    @GetMapping("/{id}")
    public R get(@PathVariable Long id) {
        SysJob sysJob = sysJobService.getById(id);
        return R.ok(sysJob);
    }

    /**
     * 开启任务
     */
    @PostMapping("/trigger/{id}")
    public R trigger(@PathVariable Long id) throws SchedulerException, ClassNotFoundException {
        sysJobService.triggerJob(sysJobService.getById(id));
        return R.ok();
    }

    /**
     * 暂停任务
     */
    @PutMapping("/pause/{id}")
    public R pause(@PathVariable Long id) throws SchedulerException {
        sysJobService.pauseJob(sysJobService.getById(id));
        return R.ok();
    }

    /**
     * 恢复任务
     */
    @PutMapping("/resume/{id}")
    public R resume(@PathVariable Long id) throws SchedulerException {
        sysJobService.resumeJob(sysJobService.getById(id));
        return R.ok();
    }

    /**
     * 删除任务
     */
    @DeleteMapping("/delete/{id}")
    public R delete(@PathVariable Long id) throws SchedulerException {
        sysJobService.deleteJob(sysJobService.getById(id));
        return R.ok();
    }

    /**
     * 获得实现QuartzJobBean的类
     */
    @GetMapping("/class")
    public R getClassList() {
        Set<Select> subClassNames = JobClassUtils.getSubClasses();
        return R.ok(subClassNames);
    }

    @GetMapping("/cron/next")
    public R getNextTimes(String cron) throws ParseException {
        if (CronUtils.isValidated(cron)) {
            return R.ok(CronUtils.getNext(cron, 10));
        }
        return R.build(602, "错误的Cron表达式");
    }
}
