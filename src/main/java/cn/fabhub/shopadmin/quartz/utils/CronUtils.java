package cn.fabhub.shopadmin.quartz.utils;

import org.quartz.CronExpression;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/17 16:19
 */
public class CronUtils {

    /**
     * 验证cron表达式是否有效
     *
     * @param cron cron表达式
     * @return 验证结果
     */
    public static boolean isValidated(String cron) {
        return CronExpression.isValidExpression(cron);
    }


    public static List<Date> getNext(String cronString, int count) throws ParseException {
        List<Date> res = new ArrayList<>();
        CronExpression cronExpression = new CronExpression(cronString);
        Date pre = new Date();
        Date next;

        for (int i = 0; i < count; i++) {
            next = cronExpression.getNextValidTimeAfter(pre);
            res.add(next);
            pre = next;
        }
        return res;
    }

    /**
     * 获得下次执行时间
     *
     * @param cron cron表达式
     * @return 执行时间
     */
    public static Date getNextExecution(String cron) {
        CronExpression cronExpression = null;
        try {
            cronExpression = new CronExpression(cron);
            return cronExpression.getNextInvalidTimeAfter(new Date());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
