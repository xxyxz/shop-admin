package cn.fabhub.shopadmin.quartz.utils;

import cn.fabhub.shopadmin.vo.Select;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ConfigurationBuilder;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/18 21:23
 */
public class JobClassUtils {

    public static Set<Select> getSubClasses() {
        Set<Select> res = new HashSet<>();
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .forPackages("cn.fabhub.shopadmin.quartz.job")
                .setScanners(new SubTypesScanner()));
        Set<Class<? extends QuartzJobBean>> subTypes = reflections.getSubTypesOf(QuartzJobBean.class);

        for (Class<? extends QuartzJobBean> aClass : subTypes) {
            res.add(Select.builder().key(aClass.getTypeName()).title(aClass.getName()).build());
        }
        return res;
    }

}
