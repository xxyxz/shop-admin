package cn.fabhub.shopadmin.third.clould;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.OSSObject;
import com.aliyun.oss.model.ObjectMetadata;

import java.io.InputStream;
import java.net.URL;
import java.util.Date;

/**
 * @author zhou
 * @date 2020-08-24 22:26
 */
public class AliyunOssUtils {

    private static final String endpoint = "oss-cn-chengdu.aliyuncs.com";
    private static final String accessKeyId = "LTAI4FzCJbj8tfBs8mjdX3eK";
    private static final String accessKeySecret = "x8CIiWqLkmBrj1JnsnXYtAC5llqtKI";
    private static final String bucketName = "m-zhou";
    private static final Long expireTime = 20 * 365 * 24 * 3600 * 1000L;

    private static OSS getOSSClient() {
        return new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
    }


    /**
     * 生成截止时间
     *
     * @return 截止时间
     */
    private static Date generateDiedLine() {
        return new Date(new Date().getTime() + expireTime);
    }


    /**
     * 保存文件
     *
     * @param fileName    文件名称
     * @param inputStream 输入流
     * @return 访问url
     */
    public static String putObject(String fileName, InputStream inputStream) {
        OSS ossClient = getOSSClient();
        ossClient.putObject(bucketName, fileName, inputStream);
        URL url = ossClient.generatePresignedUrl(bucketName, fileName, generateDiedLine());
        ossClient.shutdown();
        return url.toString().split("\\?")[0];
    }


    /**
     * 文件元数据
     *
     * @param filename 文件名
     * @return 元数据对象
     */
    public static ObjectMetadata getObjectMetadata(String filename) {
        OSS ossClient = getOSSClient();
        ObjectMetadata objectMetadata = ossClient.getObjectMetadata(bucketName, filename);
        ossClient.shutdown();
        return objectMetadata;
    }

    /**
     * 获得文件输入流
     * @param filename 文件名称
     * @return 输入流
     */
    public static InputStream getObject(String filename) {
        OSS ossClient = getOSSClient();
        OSSObject object = ossClient.getObject(bucketName, filename);
        return object.getObjectContent();
    }


    /**
     * 删除对象文件
     *
     * @param filenames 文件
     */
    public static void deleteObject(String... filenames) {
        OSS ossClient = getOSSClient();
        for (String filename : filenames) {
            ossClient.deleteObject(bucketName, filename);
        }
        ossClient.shutdown();
    }
}
