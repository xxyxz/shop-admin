package cn.fabhub.shopadmin.spider.nwpu;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/15 15:05
 */
public class Auth {

    public WebDriver Login(String username, String password) throws InterruptedException, MalformedURLException {
        SomeWebDriver someWebDriver = new SomeWebDriver();
        // 获得远程调用
        WebDriver webDriver = someWebDriver.getRemoteWebDriver();
        // 打开链接
        String LOGIN_URL = "https://ecampus.nwpu.edu.cn";
        webDriver.get(LOGIN_URL);
        // 等待一段时间
        webDriver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
        WebElement usernameElement = webDriver.findElement(By.id("username"));
        WebElement passwordElement = webDriver.findElement(By.id("password"));
        WebElement submitElement = webDriver.findElement(By.name("submit"));

        // 设置值
        usernameElement.sendKeys(username);
        System.out.println("已经输入用户名称");
        passwordElement.sendKeys(password);
        System.out.println("已经输入用户密码");
        submitElement.click();
        System.out.println("已经点击用户登录");
        Thread.sleep(1000);
        return webDriver;
    }


    public boolean lastInput(WebDriver webDriver) {
        String url = "http://yqtb.nwpu.edu.cn/wx/xg/yz-mobile/index.jsp";
        webDriver.get(url);
        // 等待一段时间
        webDriver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
        // 获得元素
        WebElement element2 = webDriver.findElement(By.linkText("健康登记"));
        element2.click();
        System.out.println("已经点击健康登记");

        // 等待页面元素加载
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // 点击提交信息
        WebElement submitElement = webDriver.findElement(By.linkText("提交填报信息"));
        submitElement.click();
        System.out.println("已经点击提交填报信息");

        // 勾选确认
        WebElement element1 = webDriver.findElement(By.cssSelector("div[class='weui-cells weui-cells_checkbox']"));
        element1.click();
        System.out.println("已经勾选确认");

        // 提交
        WebElement confirmSubmit = webDriver.findElement(By.linkText("确认提交"));
        confirmSubmit.click();
        System.out.println("已经确认提交");
        // 等待12秒
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // 查找是否有完成填报标识
        WebElement element = webDriver.findElement(By.xpath("//*[contains(text(), \"您已提交今日填报，重新提交将覆盖上一次的信息\")]"));
        return element != null;
    }
}
