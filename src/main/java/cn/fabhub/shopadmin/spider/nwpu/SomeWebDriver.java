package cn.fabhub.shopadmin.spider.nwpu;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/15 18:13
 */
@Component
@ConfigurationProperties(prefix = "chrome")
public class SomeWebDriver {

    private static String host = "chrome";

    private static String port = "4444";

    public void setHost(String host) {
        SomeWebDriver.host = host;
    }

    public void setPort(String port) {
        SomeWebDriver.port = port;
    }

    public String getHost() {
        return host;
    }

    public String getPort() {
        return port;
    }

    public static ChromeOptions getChromeOptions() {
        // 设置chromedriver地址
        System.setProperty("webdriver.chrome.driver", "G:\\chromedriver.exe");
        // 关闭日志
        System.setProperty("webdriver.chrome.silentOutput", "true");
        // 实例化一个Chrome
        ChromeOptions chromeOptions = new ChromeOptions();
        // 字符编码utf-8
        chromeOptions.addArguments("lang=zh_CN.UTF-8");
        // 开启最大化
        chromeOptions.addArguments("–start-maximized");
        chromeOptions.addArguments("–no-sandbox");
        // 开启无头模式
        chromeOptions.addArguments("--headless");
        chromeOptions.addArguments("--disable-gpu");
        chromeOptions.addArguments("--window-size=1920,1080");
        chromeOptions.addArguments("--silent");
        return chromeOptions;
    }

    /**
     * 获得本地浏览器
     *
     * @return 浏览器
     */
    public WebDriver getLocalWebDriver() {
        ChromeOptions chromeOptions = getChromeOptions();
        return new ChromeDriver(chromeOptions);
    }

    /**
     * 获得WebDriver 远程调用
     *
     * @return WebDriver
     * @throws MalformedURLException 不正确的url
     */
    public WebDriver getRemoteWebDriver() throws MalformedURLException {
        String url = "http://" + SomeWebDriver.host + ":" + SomeWebDriver.port + "/wd/hub";
        System.out.println(url);
        DesiredCapabilities chrome = DesiredCapabilities.chrome();
        return new RemoteWebDriver(new URL(url), chrome);
    }
}
