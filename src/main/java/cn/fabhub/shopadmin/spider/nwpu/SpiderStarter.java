package cn.fabhub.shopadmin.spider.nwpu;

import cn.fabhub.shopadmin.mail.MailService;
import org.openqa.selenium.WebDriver;

import javax.annotation.Resource;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/15 21:35
 */
public class SpiderStarter {


    @Resource
    private MailService mailService;


    public void startOne() {
        Auth auth = new Auth();
        String username = "2020264857";
        String password = "20200520zhou";
        String indexPage = "ecampus.nwpu.edu.cn/portal-web/html/index.html";
        WebDriver webDriver = null;
        try {
            // 获得登录过后的浏览器
            webDriver = auth.Login(username, password);
            // 判断是否登录成功
            if (webDriver.getCurrentUrl().contains(indexPage)) {
                // 登录成功
                if (auth.lastInput(webDriver)) {
                    System.out.println("填报成功");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            mailService.sendSimpleMail("1226105312@qq.com", "自动填报错误信息", "错误信息：" + e.getMessage());
        } finally {
            if (webDriver != null) {
                webDriver.close();
            }
        }
    }
}
