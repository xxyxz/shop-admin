package cn.fabhub.shopadmin.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

import java.util.Date;

/**
 * 基础字典类型
 * @author zhou
 * @date 2020-08-19 20:43
 */

@Data
@SuperBuilder
@Accessors(chain = true)
@NoArgsConstructor
public class SysDictType {
    /**
     * 字典id
     */
    private Long id;

    /**
     * 字典名称
     */
    private String dictName;

    /**
     * 字典编码
     */
    private String dictType;

    /**
     * 排序
     */
    private Integer orderNum;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 字典的描述
     */
    private String remark;

    /**
     * 创建时间
     */
    private Date createDate;

    /**
     * 更新时间
     */
    private Date updateDate;

}