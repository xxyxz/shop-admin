package cn.fabhub.shopadmin.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

import java.util.Date;

/**
 * 操作日志
 *
 * @author zhou
 * @date 2020-08-10 20:32
 */
@Data
@SuperBuilder
@NoArgsConstructor
@Accessors(chain = true)
public class SysAccessLog {

    /**
     * 日志主键
     */
    private Long id;

    /**
     * 接口名称
     */
    private String title;

    /**
     * 业务类型
     */
    private Integer businessType;

    /**
     * 操作人员
     */
    private String accessUser;

    /**
     * 请求URL
     */
    private String accessUrl;

    /**
     * 调用方法
     */
    private String method;

    /**
     * 请求方式
     */
    private String requestMethod;

    /**
     * 主机地址
     */
    private String sourceIp;

    /**
     * 源端口
     */
    private Integer sourcePort;

    /**
     * 操作地点
     */
    private String sourceLocation;

    /**
     * 请求参数
     */
    private String accessParam;

    /**
     * 操作状态（0正常 1异常）
     */
    private Integer status;

    /**
     * 返回参数
     */
    private String jsonResult;

    /**
     * 错误消息
     */
    private String errorMsg;

    /**
     * 操作时间
     */
    private Date accessTime;
}