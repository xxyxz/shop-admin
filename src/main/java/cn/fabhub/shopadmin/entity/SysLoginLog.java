package cn.fabhub.shopadmin.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

import java.util.Date;

/**
 *
 * 系统访问记录
 * @author zhou
 * @date 2020-08-19 12:15
*/
@Data
@SuperBuilder
@Accessors(chain = true)
@NoArgsConstructor
public class SysLoginLog {
    /**
     * 访问ID
     */
    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 用户账号
     */
    private String loginName;

    /**
     * 登录IP地址
     */
    private String loginIp;

    /**
     * 登录地点
     */
    private String loginLocation;

    /**
     * 浏览器类型
     */
    private String browser;

    /**
     * 操作系统
     */
    private String os;

    /**
     * 登录状态（0成功 1失败）
     */
    private Integer status;

    /**
     * 提示消息
     */
    private String msg;

    /**
     * 访问时间
     */
    private Date loginTime;
}