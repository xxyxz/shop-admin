package cn.fabhub.shopadmin.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

import java.util.Date;

/**
 * @author zhou
 * @date 2020-08-19 23:17
 */

@Data
@SuperBuilder
@Accessors(chain = true)
@NoArgsConstructor
public class SysDictData {

    /**
     * 字典数据编号
     */
    private Long id;

    /**
     * 字典数据类型
     */
    private String dictType;

    /**
     * 字典标签
     */
    private String dictLabel;

    /**
     * 颜色
     */
    private String color;

    /**
     * 字典键值
     */
    private Integer dictValue;

    /**
     * 排序
     */
    private Integer orderNum;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private Date createDate;

    /**
     * 更新时间
     */
    private Date updateDate;
}