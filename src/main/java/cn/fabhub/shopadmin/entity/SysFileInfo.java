package cn.fabhub.shopadmin.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 *
 * 文件存储
 * @author  Mr.zhou
 * @date  2020/11/12 17:49
 * @version 1.0
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class SysFileInfo {

    /**
    * 主键
    */
    private Long id;

    /**
    * 文件名称
    */
    private String filename;

    /**
    * 文件大小
    */
    private Long fileSize;

    /**
    * 文件类型
    */
    private String type;

    /**
    * 文件存放类型：阿里云、本地、七牛云
    */
    private Integer storage;

    /**
    * 访问路径
    */
    private String accessPath;

    /**
    * 谁创建
    */
    private Long createBy;

    /**
    * 更新者
    */
    private Long updateBy;

    /**
    * 创建时间
    */
    private Date createTime;

    /**
    * 更新时间
    */
    private Date updateTime;
}