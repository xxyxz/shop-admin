package cn.fabhub.shopadmin.entity;

import cn.fabhub.shopadmin.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 系统用户角色
 * @author zhou
 * @date 2020-08-01 15:12
*/
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class SysRole extends BaseEntity {

    /**
     * 角色Id
     */
    private Long id;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 角色中文名称
     */
    private String zhName;

    /**
     * 是否默认角色
     */
    private Integer isDefault;

    /**
     * 角色描述
     */
    private String remark;

    /**
     * 资源id集合
     */
    transient private List<Long> resourceIds;
}