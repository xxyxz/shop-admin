package cn.fabhub.shopadmin.entity;

import cn.fabhub.shopadmin.base.BaseEntity;
import cn.fabhub.shopadmin.constant.UserStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 系统用户表
 *
 * @author zhou
 * @date 2020-07-27 12:30
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class SysUser extends BaseEntity implements UserDetails{

    /**
     * 用户Id
     */
    private Long id;

    /**
     * 登录名称，由系统自动生成
     */
    private String loginName;

    /**
     * 用户别名
     */
    private String username;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 邮件号
     */
    private String email;

    /**
     * 性别(字典)
     */
    private Integer sex;

    /**
     * 登录密码
     */
    private String password;

    /**
     * 用户头像
     */
    private String avatar;

    /**
     * 激活码
     */
    private String actCode;

    transient private List<Long> roleIds;

    transient private Set<SysRole> roles;

    transient private Set<String> permissions;


    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>(8);
        for (String permission: permissions) {
            authorities.add(new SimpleGrantedAuthority(permission));
        }
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return !(this.getStatus().equals(UserStatus.EXPIRE.getCode()));
    }

    @Override
    public boolean isAccountNonLocked() {
        return !(this.getStatus().equals(UserStatus.LOCK.getCode()));
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return !(this.getStatus().equals(UserStatus.CREDENTIALS_EXPIRE.getCode()));
    }

    @Override
    public boolean isEnabled() {
        return this.getStatus().equals(UserStatus.NORMAL.getCode());
    }
}