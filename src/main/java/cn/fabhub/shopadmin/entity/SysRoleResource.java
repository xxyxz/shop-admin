package cn.fabhub.shopadmin.entity;

import lombok.Data;

/**
 * 角色权限关联表
 *
 * @author zhou
 * @date 2020-08-10 13:59
 */
@Data
public class SysRoleResource {

    private Long roleId;

    private Long resourceId;

    public SysRoleResource(Long roleId, Long resourceId) {
        this.roleId = roleId;
        this.resourceId = resourceId;
    }
}