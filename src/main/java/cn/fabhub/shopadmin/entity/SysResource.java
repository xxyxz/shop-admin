package cn.fabhub.shopadmin.entity;

import cn.fabhub.shopadmin.base.BaseEntity;
import cn.fabhub.shopadmin.factory.BaseTreeNode;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author zhou
 * @date 2020-09-12 11:03
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class SysResource extends BaseEntity implements BaseTreeNode<SysResource> {

    /**
     * 主键
     */
    private Long id;

    /**
     * 父id
     */
    private Long parentId;

    /**
     * 资源类型(1：目录，2：菜单，3按钮）
     */
    private Integer type;

    /**
     * 资源名称
     */
    private String name;

    /**
     * 图标
     */
    private String icon;

    /**
     * 组件路径
     */
    private String component;

    /**
     * 路由地址
     */
    private String router;

    /**
     * 权限编码
     */
    private String permission;

    /**
     * 打开方式：0无，1组件，2内链，3外链
     */
    private Integer openType;

    /**
     * 是否可见
     */
    private Integer visible;

    /**
     * 重定向
     */
    private String redirect;

    /**
     * 排序编号
     */
    private Integer orderNum;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    transient private List<SysResource> children;

    /**
     * 获取父节点Id
     *
     * @return 父id
     */
    @Override
    public Long getPid() {
        return this.getParentId();
    }

    /**
     * 设置孩子节点
     *
     * @param children 子节点集合
     */
    @Override
    public void setChildren(List<SysResource> children) {
        this.children = children;
    }
}