package cn.fabhub.shopadmin.entity;

import lombok.Data;

/**
 * 用户角色关联表
 *
 * @author zhou
 * @date 2020-08-01 17:04
 */
@Data
public class SysUserRole {

    /**
     * 用户Id
     */
    private Long userId;

    /**
     * 角色id
     */
    private Long roleId;

    public SysUserRole(Long userId, Long roleId) {
        this.userId = userId;
        this.roleId = roleId;
    }
}