package cn.fabhub.shopadmin.aspect;

import cn.fabhub.shopadmin.annotation.Log;
import cn.fabhub.shopadmin.constant.OperStatus;
import cn.fabhub.shopadmin.entity.SysAccessLog;
import cn.fabhub.shopadmin.service.SysAccessLogService;
import cn.fabhub.shopadmin.util.IpUtils;
import cn.fabhub.shopadmin.util.TokenService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Objects;
import java.util.regex.Pattern;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author zhou
 * @date 2020-08-10 20:58
 */
@Aspect
@Component
public class LogAspect {

    private static final Logger logger = getLogger(LogAspect.class);

    private static final Pattern METHOD_PATTERN = Pattern.compile("post|put|patch", Pattern.CASE_INSENSITIVE);

    @Resource
    private SysAccessLogService sysAccessLogService;


    // 配置切入点
    @Pointcut("@annotation(cn.fabhub.shopadmin.annotation.Log)")
    public void logAspect() {
    }

    /**
     * 是否存在注解，如果存在就获取
     */
    private Log getAnnotationLog(JoinPoint joinPoint) {
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();

        if (method != null) {
            return method.getAnnotation(Log.class);
        }
        logger.warn("未获取到注释方法：" + methodSignature);
        return null;
    }

    /**
     * 请求处理
     *
     * @param joinPoint 连接点
     * @param e         异常
     */
    @Async
    void handler(JoinPoint joinPoint, Exception e) {

        // 获得日志注解
        Log log = getAnnotationLog(joinPoint);
        if (log == null) {
            return;
        }

        // 添加日志title
        String title = log.title();

        int businessType = log.businessType();

        // 获得接口调用方法
        // 获得接口所在类
        String method = joinPoint.getSignature().getDeclaringTypeName();
        method += "." + joinPoint.getSignature().getName();

        // 获得request
        HttpServletRequest request = ((ServletRequestAttributes) (Objects.requireNonNull(RequestContextHolder.getRequestAttributes()))).getRequest();

        // 获得请求用户
        String loginName = TokenService.getLoginName();

        // 获得请求url
        String requestUri = request.getRequestURI();

        // 获得请求方法
        String requestMethod = request.getMethod();

        // 获得请求IP
        String requestIp = IpUtils.getIpAddr(request);

        // 获得请求参数: 连接在url后面的参数
        String params = getParamsString(joinPoint, request);

        // 获得源端口
        int requestPort = request.getRemotePort();

        // 日志请求是否正常
        int status = e == null ? OperStatus.SUCCESS.getCode() : OperStatus.FAIL.getCode();

        // 封装操作日志
        SysAccessLog sysAccessLog = new SysAccessLog();
        sysAccessLog
                .setTitle(title)
                .setBusinessType(businessType)
                .setAccessUser(loginName)
                .setAccessUrl(requestUri)
                .setMethod(method)
                .setRequestMethod(requestMethod)
                .setSourceIp(requestIp)
                .setSourcePort(requestPort)
                .setSourceLocation(IpUtils.getLocation(requestIp))
                .setAccessParam(params)
                .setStatus(status)
                .setErrorMsg(status == OperStatus.SUCCESS.getCode() ? null : Objects.requireNonNull(e).getMessage())
                .setAccessTime(new Date());

        // 保存
        sysAccessLogService.saveDetails(sysAccessLog);
    }

    /**
     * 获得请求参数
     *
     * @param joinPoint 连接点
     * @param request   请求
     * @return 参数
     */
    private String getParamsString(JoinPoint joinPoint, HttpServletRequest request) {
        StringBuilder params = new StringBuilder();

        // 获得请求体参数
        if (METHOD_PATTERN.matcher(request.getMethod()).find()) {
            Object[] args = joinPoint.getArgs();
            for (Object o : args) {
                // 可能会出现异常：JSONException
                if (!(o instanceof MultipartFile)) {
                    String string = JSON.toJSONString(o);
                    params.append(string);
                }
            }
        }

        // 获得queryString
        if (StringUtils.isNotBlank(request.getQueryString())) {
            params.append(request.getQueryString());
        }
        return params.toString();
    }


    /**
     * 返回之后的处理
     *
     * @param joinPoint 连接点
     */
    @AfterReturning(value = "logAspect()")
    public void afterReturning(JoinPoint joinPoint) {
        handler(joinPoint, null);
    }

    /**
     * 抛出异常的处理
     *
     * @param joinPoint 连接点
     * @param e         异常
     */
    @AfterThrowing(value = "logAspect()", throwing = "e")
    public void afterThrowing(JoinPoint joinPoint, Exception e) {
        handler(joinPoint, e);
    }
}
