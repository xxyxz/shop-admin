package cn.fabhub.shopadmin.util;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhou
 * @date 2020-05-30 19:43
 */
public class CustomBeanUtils {

    /**
     * 复试对象
     * @param source 源对象
     * @param target 目标对象
     * @throws BeansException 复制异常
     */
    public static void copyProperties(Object source, Object target) throws BeansException {
        BeanUtils.copyProperties(source, target);
    }

    /**
     * 复试对象 忽略null字段
     * @param source 源对象
     * @param target 目标对象
     * @throws BeansException 复制异常
     */
    public static void copyPropertiesIgnoreNull(Object source, Object target) throws BeansException {

        List<String> ignoreProperties = new ArrayList<>(8);
        Class<?> sourceClass = source.getClass();
        Field[] declaredFields = sourceClass.getDeclaredFields();
        for (Field field : declaredFields) {
            try {
                field.setAccessible(true);
                Object o = field.get(source);
                if (o == null) {
                    ignoreProperties.add(field.getName());
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        String[] strings = ignoreProperties.toArray(new String[0]);
        BeanUtils.copyProperties(source, target, strings);
    }
}
