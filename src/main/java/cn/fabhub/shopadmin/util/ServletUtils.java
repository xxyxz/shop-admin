package cn.fabhub.shopadmin.util;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author zhou
 * @date 2020-08-19 12:21
 */
public class ServletUtils {


    /**
     * 获取参数
     * @param key key
     * @return value
     */
    public static String getParameter(String key) {
        return getRequest().getParameter(key);
    }

    /**
     * 获得请求头
     * @param key 请求头
     * @return 对应的值
     */
    public static String getHeader(String key) {
        return getRequest().getHeader(key);
    }

    /**
     * 获得session 对象
     * @return Session对象
     */
    public static HttpSession getSession() {
        return getRequest().getSession();
    }

    /**
     * 获取Request 对象
     * @return Request
     */
    public static HttpServletRequest getRequest() {
        return getRequestAttributes().getRequest();
    }

    /**
     * 获取Response
     * @return 对象
     */
    public static HttpServletResponse getResponse() {
        return  getRequestAttributes().getResponse();
    }

    /**
     * 获得ServletRequestAttributes
     * @return ServletRequestAttributes对象
     */
    public static ServletRequestAttributes getRequestAttributes() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        return (ServletRequestAttributes) requestAttributes;
    }

}
