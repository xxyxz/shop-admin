package cn.fabhub.shopadmin.util;

import cn.fabhub.shopadmin.entity.SysRole;
import cn.fabhub.shopadmin.vo.Select;

/**
 * @author zhou
 * @date 2020-07-27 16:21
 */
public class Converter {

    public static Select toSimple(SysRole role) {
        Select tSelect = new Select();
        tSelect.setKey(role.getId());
        tSelect.setTitle(role.getZhName());
        return tSelect;
    }
}


