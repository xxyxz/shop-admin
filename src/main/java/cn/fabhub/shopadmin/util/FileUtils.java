package cn.fabhub.shopadmin.util;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.HttpHeaders;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/14 16:15
 */
public class FileUtils {


    /**
     * 获得文件类型
     * @param inputStream 输入流
     * @return 文件类型
     */
    public static String getMimeType(InputStream inputStream){
        AutoDetectParser parser = new AutoDetectParser();
        parser.setParsers(new HashMap<>());
        Metadata metadata = new Metadata();
        try {
            parser.parse(inputStream, new DefaultHandler(), metadata, new ParseContext());
        } catch (TikaException | SAXException | IOException e) {
            e.printStackTrace();
        }
        return metadata.get(HttpHeaders.CONTENT_TYPE);
    }
}
