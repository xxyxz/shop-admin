package cn.fabhub.shopadmin.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author zhou
 * @date 2020-05-30 12:26
 */
public class IpUtils {


    private static final Logger logger = LoggerFactory.getLogger(IpUtils.class);

    private static final String LOOPBACK_IPV6 = "0:0:0:0:0:0:0:1";

    private static final String LOOPBACK_IPV4 = "172.0.0.1";

    private static final String UNKNOWN = "unknown";

    /**
     * 局域网地址范围分三类，以下IP段为内网IP段：
     * C类：192.168.0.0 - 192.168.255.255
     * B类：172.16.0.0 - 172.31.255.255
     * A类：10.0.0.0 - 10.255.255.255
     * 环回地址：127
     */
    public static boolean isLocalIp(String ip) {
        if (LOOPBACK_IPV6.equals(ip)) {
            return true;
        }
        String pattern = "^192.168.|^10.|^127.|^172.(1[6-9]|2[0-9]|3[0-1]).";
        // 创建 Pattern 对象
        Pattern p = Pattern.compile(pattern);
        // 现在创建 matcher 对象
        return p.matcher(ip).find();
    }

    public static String getLocation(String ip) {
        // 先判断是否局域网
        if (isLocalIp(ip)) {
            return "局域网";
        }

        // 通过百度查询
        String location = getIpLocationFromBaidu(ip);
        if (StringUtils.isNotBlank(location)) {
            return location;
        }
        location = getIpLocationFromTaiPingYang(ip);
        if (StringUtils.isNotBlank(location)) {
            return location;
        }

        return UNKNOWN;
    }


    /**
     * 从百度获得归属地
     * @param ip ip
     * @return 归属地
     */
    private static String getIpLocationFromBaidu(String ip) {
        try {
            String url = "http://opendata.baidu.com/api.php";
            Map<String, String> params = new HashMap<>(4);
            params.put("query", ip);
            params.put("resource_id", "6006");
            params.put("format", "json");
            String str = HttpClientTool.doGet(url, params);
            JSONObject jsonObject = JSONObject.parseObject(str);
            if (!"0".equals(jsonObject.get("status"))) {
                return null;
            }

            JSONArray data = jsonObject.getJSONArray("data");
            if (data.size() > 0) {
                return data.getJSONObject(0).get("location").toString();
            }
        } catch (Exception e) {
            logger.error("从百度查询ip归属地出错", e);
        }

        return null;
    }

    /**
     * 从太平洋获得ip归属地
     * @param ip ip
     * @return 归属地
     */
    private static String getIpLocationFromTaiPingYang(String ip) {
        try {
            String url = "http://whois.pconline.com.cn/ipJson.jsp";
            Map<String, String> params = new HashMap<>(2);
            params.put("ip", ip);
            params.put("json", "true");
            String result = HttpClientTool.doGet(url, params);
            return JSONObject.parseObject(result).getString("addr");
        }catch (Exception e) {
            logger.error("从太平洋查询ip归属出错");
        }
        return null;
    }


    /**
     * 从请求中获取真实IP
     *
     * @param request 请求
     * @return ip
     */
    public static String getIpAddr(HttpServletRequest request) {
        if (request == null) {
            return "unknown";
        }

        String ip = request.getHeader("x-forwarded-for");
        if (isNotIp(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }

        if (isNotIp(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }

        if (isNotIp(ip)) {
            ip = request.getRemoteAddr();
        }

        return ip;
    }

    private static boolean isNotIp(String ip) {
        return !isIp(ip);
    }

    private static boolean isIp(String ip) {
        return ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip);
    }

    public static String getHostIp() {
        try {
            return InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException ignored) {
        }
        return LOOPBACK_IPV4;
    }

    public static String getHostName() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException ignored) {
            logger.error("未知主机名称");
        }
        return "未知";
    }
}
