package cn.fabhub.shopadmin.util;

import cn.fabhub.shopadmin.vo.R;
import com.alibaba.fastjson.JSON;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 响应工具类
 *
 * @author zhou
 * @date 2020-09-13 17:10
 */
public class ResponseUtils {

    public static void exceptionHandle(HttpServletResponse response,
                                       Integer code,
                                       String message) throws IOException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        R errorResponseData = R.build(code, message);
        String errorResponseJsonData = JSON.toJSONString(errorResponseData);
        response.getWriter().write(errorResponseJsonData);
    }

    /**
     * 响应异常，向前端返回ErrorResponseData的json数据，用于全局异常处理器
     *
     * @author xuyuxiang
     * @date 2020/3/20 11:31
     */
    public static R responseDataError(Integer code, String message) {
        return R.build(code, message);
    }
}
