package cn.fabhub.shopadmin.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;

/**
 * @author zhou
 * @date 2020-08-25 8:07
 */
@Component
@ConfigurationProperties(prefix = "token.config")
public class JwtUtils {

    // 令牌密钥
    private static String secret = "79c10980b0af4e9f984a67c40a464fb2";

    // 一秒
    private static final int SECOND = 1000;
    private static final int MINUTE = 60 * SECOND;
    // 过期时间
    private static int expireTime = 1200 * MINUTE;


    public int getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(int expireTime) {
        JwtUtils.expireTime = expireTime * MINUTE;
    }

    public void setSecret(String secret) {
        JwtUtils.secret = secret;
    }

    public String getSecret() {
        return secret;
    }

    /**
     * 创建Token、
     *
     * @param id id
     * @param subject 主题
     * @return token
     */
    public static String createToken(Long id, String subject) {
        return Jwts.builder()
                .setId(String.valueOf(id))
                .setSubject(subject)
                .setExpiration(createDiedLine())
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }


    /**
     * 创建Token
     *
     * @param claims 要求
     * @return token字符串
     */
    public static String createToken(Map<String, Object> claims) {
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(createDiedLine())
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }


    /**
     * 获得键为id的值
     * @param token token字符串
     * @return value
     */
    public static String getId(String token) {
        return getClaims(token).getId();
    }


    /**
     * 获得主题
     * @param token token字符串
     * @return value
     */
    public static String getSubject(String token) {
        return getClaims(token).getSubject();
    }


    /**
     * 刷新token
     *
     * @param oldToken 源token
     * @return 新token
     */
    public static String refresh(String oldToken) {
        try {
            // 获得声明
            Claims claims = getClaims(oldToken);
            // 过期时间差值
            long intervalTime = claims.getExpiration().getTime() - new Date().getTime();
            if (intervalTime / MINUTE < 30) {
                return createToken(claims);
            }
            // 不需要更新，返回为空
            return null;
        }catch (JwtException e) {
            // 更新失败，返回为空
            return null;
        }
    }


    /**
     * 解析token
     *
     * @param token token
     * @return Claims
     */
    public static Claims getClaims(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }


    /**
     * 是否是有效的token
     *
     * @param token token
     * @return 结果
     */
    public static Boolean isValidated(String token) {
        try {
            getClaims(token);
            return true;
        } catch (JwtException e) {
            return false;
        }
    }


    /**
     * 生成过期时间
     *
     * @return 过期时间
     */
    private static Date createDiedLine() {
        return new Date(new Date().getTime() + expireTime);
    }
}