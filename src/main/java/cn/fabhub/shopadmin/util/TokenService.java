package cn.fabhub.shopadmin.util;

import org.apache.commons.lang3.StringUtils;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/6 15:19
 */
public class TokenService {

    public static final String TOKEN_KEY = "Access-Token";
    public static final String TOKEN_PREFIX = "Bearer ";


    /**
     * 从请求中获得token
     * @return token 带有前缀
     */
    public static String getTokenWithPrefix() {
        return ServletUtils.getHeader(TOKEN_KEY);
    }

    /**
     * 获得jwt
     * @return jwt字符串
     */
    public static String getToken() {
        String tokenWithPrefix = getTokenWithPrefix();
        // 去掉前缀
        if (StringUtils.isNotBlank(tokenWithPrefix)) {
            // 去掉前缀
            return tokenWithPrefix.replaceFirst(TOKEN_PREFIX, "");
        }
        return null;
    }


    /**
     * 是否存在token
     * @return 结果
     */
    public static Boolean existToken() {
        String token = getToken();
        return StringUtils.isNotBlank(token);
    }


    /**
     * 刷新token
     * @return 新的token
     */
    public static String refresh() {
        // 从请求中获得不带有前缀的token
        String oldToken = getToken();
        // 刷新token
        if (StringUtils.isNotBlank(oldToken)) {
            // 刷新并加上前缀
            String newToken = JwtUtils.refresh(oldToken);
            if (StringUtils.isNotBlank(newToken)) {
                return TOKEN_PREFIX + newToken;
            }
        }
        return null;
    }

    /**
     * 创建token
     *
     * @param userId    用户id
     * @param loginName 用户名
     * @return token
     */
    public static String createToken(Long userId, String loginName) {
        String token = JwtUtils.createToken(userId, loginName);
        return TOKEN_PREFIX + token;
    }


    /**
     * 获得用户的Id
     * @return id
     */
    public static Long getUserId() {
        String id = JwtUtils.getId(getToken());
        return Long.parseLong(id);
    }


    /**
     * 获得用户登录名
     * @return 用户登录名
     */
    public static String getLoginName() {
        return JwtUtils.getSubject(getToken());
    }

}
