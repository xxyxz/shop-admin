package cn.fabhub.shopadmin.query;

import cn.fabhub.shopadmin.util.MDateUtils;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author zhou
 * @date 2020-07-25 19:03
 */
@Data
public class BaseQuery {

    /*当前页码*/
    private Long pageNum = 1L;

    /*分页大小*/
    private Integer pageSize = 10;

    /**
     * 状态
     */
    private Integer status;

    /*起始时间*/
    // @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date startTime;

    /*结束时间*/
    // @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date endTime;

    public void setEndTime(Date endTime) {
        this.endTime = MDateUtils.addDays(endTime, 1);
    }

}
