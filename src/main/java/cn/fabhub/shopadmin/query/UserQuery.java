package cn.fabhub.shopadmin.query;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author zhou
 * @date 2020-07-25 19:54
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UserQuery extends BaseQuery {

    // 登录名称
    private String loginName;

    // 用户名
    private String username;

    // 电话
    private String phone;

    // 邮件
    private String email;

    // 性别
    private Integer sex;
}
