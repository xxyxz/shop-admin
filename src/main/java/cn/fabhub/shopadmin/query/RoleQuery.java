package cn.fabhub.shopadmin.query;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author zhou
 * @date 2020-07-25 19:57
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class RoleQuery extends BaseQuery {

    // 是否是简单数据返回格式：只包含，id，和name
    private Boolean simple = false;

    // 角色名称
    private String zhName;

    // 角色标识
    private String roleName;

}
