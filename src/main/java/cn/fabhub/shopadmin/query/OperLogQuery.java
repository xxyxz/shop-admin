package cn.fabhub.shopadmin.query;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author zhou
 * @date 2020-07-25 19:59
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class OperLogQuery extends BaseQuery{

   private Integer businessType;
}
