package cn.fabhub.shopadmin.query;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author zhou
 * @date 2020-07-28 14:37
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class DictQueryType extends BaseQuery{

}
