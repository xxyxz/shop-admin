package cn.fabhub.shopadmin.query;

import lombok.Data;

/**
 * @author zhou
 * @date 2020-07-26 15:19
 */
@Data
public class ResourceQuery {

    // 是否有最大根节点
    private boolean hasRoot = false;

    // 数据类型是Table还是select
    private boolean isTable = true;

    // 资源状态
    private Integer status;

    // 资源名称
    private String name;

}
