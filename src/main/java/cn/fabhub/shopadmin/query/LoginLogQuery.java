package cn.fabhub.shopadmin.query;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author zhou
 * @date 2020-08-19 13:44
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class LoginLogQuery extends BaseQuery{

    // 登录账户
    private String loginName;

}
