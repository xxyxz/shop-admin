package cn.fabhub.shopadmin.query;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/13 14:29
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class FileInfoQuery extends BaseQuery {

    private Integer fileSize;

    private String type;

    private String storage;
}
