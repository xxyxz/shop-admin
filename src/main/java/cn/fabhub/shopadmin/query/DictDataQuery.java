package cn.fabhub.shopadmin.query;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author zhou
 * @date 2020-08-19 22:25
 */
@Data
@Accessors(chain = true)
public class DictDataQuery {

    public String dictType;

    public Boolean status;
}
