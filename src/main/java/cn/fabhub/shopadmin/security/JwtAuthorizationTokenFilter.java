package cn.fabhub.shopadmin.security;

import cn.fabhub.shopadmin.service.impl.AuthServiceImpl;
import cn.fabhub.shopadmin.util.ResponseUtils;
import cn.fabhub.shopadmin.util.TokenService;
import io.jsonwebtoken.JwtException;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author zhou
 * @date 2020-09-03 23:01
 */
@Component
public class JwtAuthorizationTokenFilter extends OncePerRequestFilter {

    @Resource
    private AuthServiceImpl authService;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws IOException {
        // 获取登录名称
        try {
            if (TokenService.existToken()) {
                String loginName = TokenService.getLoginName();
                if (StringUtils.isNotBlank(loginName) && SecurityContextHolder.getContext().getAuthentication() == null) {
                    UserDetails userDetails = this.authService.loadUserByUsername(loginName);
                    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                            new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                }
            }
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        } catch (JwtException je) {
            logger.error(je);
            ResponseUtils.exceptionHandle(httpServletResponse, HttpStatus.SC_UNAUTHORIZED, "无效的认证信息");
        } catch (ServletException e) {
            e.printStackTrace();
            logger.error(e);
            ResponseUtils.exceptionHandle(httpServletResponse, HttpStatus.SC_INTERNAL_SERVER_ERROR, "服务器内部错误！");
        }
    }
}
