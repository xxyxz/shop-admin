package cn.fabhub.shopadmin.security.expression;


/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/23 17:26
 */
public interface PermissionService {

    boolean hasPermission(String code);

}
