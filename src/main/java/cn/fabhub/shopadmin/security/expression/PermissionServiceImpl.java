package cn.fabhub.shopadmin.security.expression;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/23 17:29
 */
@Service("ss")
public class PermissionServiceImpl implements PermissionService {


    @Override
    public boolean hasPermission(String code) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // 获得对象
        Object principal = authentication.getPrincipal();
        if (principal instanceof UserDetails) {
            Collection<? extends GrantedAuthority> authorities = ((UserDetails) principal).getAuthorities();
            if (authorities.contains(new SimpleGrantedAuthority("*"))) {
                return true;
            }
            return authorities.contains(new SimpleGrantedAuthority(code));
        }
        return false;
    }
}
