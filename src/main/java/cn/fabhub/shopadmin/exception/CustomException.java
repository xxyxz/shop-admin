package cn.fabhub.shopadmin.exception;

/**
 * @author zhou
 * @date 2020-03-22 23:23
 */
public class CustomException extends RuntimeException {


    public CustomException() {
    }


    public CustomException(String message) {
        super(message);
    }
}
