package cn.fabhub.shopadmin.exception;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/25 13:18
 */
public class Assert {

    private Assert() {
    }


    /**
     * 参数为空
     */
    public static void isNull(Object o, String message) {
        if (o != null) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void notNull(Object o, String message) {
        if (o == null) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void notNull(Object o) {
        notNull(o, "[Assertion failed] - this argument is required; it must not be null");
    }

}
