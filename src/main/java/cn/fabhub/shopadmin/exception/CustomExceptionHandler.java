package cn.fabhub.shopadmin.exception;

import cn.fabhub.shopadmin.vo.R;
import org.apache.http.HttpStatus;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.web.servlet.MultipartProperties;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import javax.annotation.Resource;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.SQLSyntaxErrorException;
import java.text.ParseException;


/**
 * @author zhou
 * @date 2020-02-29 11:15
 */
@RestControllerAdvice
public class CustomExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(CustomExceptionHandler.class);

    @Resource
    private MultipartProperties multipartProperties;

    /**
     * 参数异常
     */
    @ExceptionHandler(IllegalArgumentException.class)
    public R handler(IllegalArgumentException e) {
        logger.error(e.getMessage());
        return R.build(520, e.getMessage());
    }

    /**
     * 自定义异常拦截
     */
    @ExceptionHandler(CustomException.class)
    public R handler(CustomException e) {
        logger.error(e.getMessage());
        return R.build(500, e.getMessage());
    }

    /**
     * 任务调度异常
     */
    @ExceptionHandler(SchedulerException.class)
    public R handler(SchedulerException e) {
        logger.error(e.getMessage());
        return R.build(601, e.getMessage());
    }

    /**
     * SQL有语法错误
     */
    @ExceptionHandler(SQLSyntaxErrorException.class)
    public R handler(SQLSyntaxErrorException e) {
        e.printStackTrace();
        return R.build(602, "SQL执行语句有语法错误");
    }

    /**
     * 数据库外键关联
     */
    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    public R handler(SQLIntegrityConstraintViolationException sqe) {
        logger.error(sqe.getMessage());
        return R.build(521, "资源占用");
    }

    /**
     * 类型转换异常
     */
    @ExceptionHandler(ParseException.class)
    public R handler(ParseException e) {
        return R.build(233, e.getMessage());
    }

    /**
     * 表单请求方式，参数校验处理
     */
    @ExceptionHandler(BindException.class)
    public R handleBindException(BindException ex) {
        FieldError error = ex.getBindingResult().getFieldError();
        assert error != null;
        return R.build(1, error.getDefaultMessage());
    }

    /**
     * json请求方式，参数校验的处理
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public R handler(MethodArgumentNotValidException ex) {
        FieldError fieldError = ex.getBindingResult().getFieldError();
        assert fieldError != null;
        return R.build(1, fieldError.getDefaultMessage());
    }

    /**
     * 没有用户
     */
    @ExceptionHandler(UsernameNotFoundException.class)
    public R handler(UsernameNotFoundException e) {
        return R.build(408, e.getMessage());
    }

    /**
     * 文件上传异常
     */
    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public R handler() {
        return R.build(409, "文件超过上传限定大小：" + multipartProperties.getMaxFileSize().toMegabytes() + "M");
    }

    /**
     * 禁止访问
     */
    @ExceptionHandler(AccessDeniedException.class)
    public R handler(AccessDeniedException e) {
        e.printStackTrace();
        return R.build(HttpStatus.SC_FORBIDDEN, e.getMessage());
    }
}
