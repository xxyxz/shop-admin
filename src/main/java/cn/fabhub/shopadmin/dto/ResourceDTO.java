package cn.fabhub.shopadmin.dto;

import cn.fabhub.shopadmin.validate.ValidationGroup;
import io.swagger.annotations.*;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author zhou
 * @date 2020-05-30 22:17
 */
@Data
@ApiModel(value = "资源DTO")
public class ResourceDTO {
    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @NotNull(message = "资源的Id不能为空", groups = ValidationGroup.Update.class)
    private Long id;

    /**
     * 父id
     */
    @ApiModelProperty(value = "父id")
    @NotNull(message = "资源的上一级资源Id不能为空", groups = ValidationGroup.Add.class)
    private Long parentId;

    /**
     * 资源类型(字典)
     */
    @ApiModelProperty(value = "资源类型")
    @NotNull(message = "资源的类型不能为空", groups = ValidationGroup.Add.class)
    private Integer type;

    /**
     * 资源名称
     */
    @ApiModelProperty(value = "资源名称")
    @NotBlank(message = "资源的名称不能为空", groups = ValidationGroup.Add.class)
    private String name;

    /**
     * 图标
     */
    @ApiModelProperty(value = "图标")
    private String icon;

    /**
     * 组件路径
     */
    @ApiModelProperty(value = "组件路径")
    private String component;

    /**
     * 路由地址
     */
    @ApiModelProperty(value = "路由地址")
    private String router;

    /**
     * 权限编码
     */
    @ApiModelProperty(value = "权限编码")
    private String permission;

    /**
     * 是否外链
     */
    @ApiModelProperty(value = "是否外链")
    private Integer openType;

    /**
     * 资源是否可见
     */
    @ApiModelProperty(value = "资源是否可见")
    private Integer visible;

    /**
     * 重定向地址
     */
    @ApiModelProperty(value = "重定向地址")
    private String redirect;

    /**
     * 排序编号
     */
    @ApiModelProperty(value = "排序编号")
    private Integer orderNum;

    /**
     * 状态(字典)
     */
    @ApiModelProperty(value = "状态")
    @NotNull(message = "资源的状态不能为空", groups = ValidationGroup.Add.class)
    private Integer status;
}
