package cn.fabhub.shopadmin.dto;

import cn.fabhub.shopadmin.validate.ValidationGroup;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author zhou
 * @date 2020-08-19 21:32
 */
@Data
public class DictDataDTO {

    /**
     * 字典数据编号
     */
    @NotNull(message = "字典数据类型Id不能为空", groups = ValidationGroup.Patch.class)
    private Long id;

    /**
     * 字典数据类型
     */
    @NotBlank(message = "字典类型不能为空", groups = {ValidationGroup.Add.class, ValidationGroup.Update.class})
    private String dictType;

    /**
     * 字典标签
     */
    @NotBlank(message = "字典数据标签不能为空", groups = ValidationGroup.Add.class)
    private String dictLabel;

    /**
     * 字典键值
     */
    @NotNull(message = "字典数据值不能为空", groups = ValidationGroup.Add.class)
    private Integer dictValue;

    /**
     * 颜色
     */
    private String color;

    /**
     * 排序
     */
    private Integer orderNum;

    /**
     * 状态
     */
    @NotNull(message = "字典的状态不能为空", groups = ValidationGroup.Add.class)
    private Integer status;

    /**
     * 备注
     */
    private String remark;
}
