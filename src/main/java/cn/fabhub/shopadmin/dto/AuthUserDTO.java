package cn.fabhub.shopadmin.dto;

import cn.fabhub.shopadmin.validate.ValidationGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author zhou
 * @date 2020-05-30 23:22
 */
@Data
@ApiModel(value = "登录用户实体")
public class AuthUserDTO {

    /**
     * 登录名称，由系统自动生成
     */
    @ApiModelProperty(value = "登录名称")
    @NotBlank(message = "登录用户名不能为空", groups = ValidationGroup.Login.class)
    private String loginName;

    @ApiModelProperty(value = "手机号码")
    private String phone;

    @ApiModelProperty(value = "邮箱号")
    private String email;

    @ApiModelProperty(value = "登录密码")
    @NotBlank(message = "登录密码不能为空", groups = ValidationGroup.Login.class)
    private String password;

    @ApiModelProperty(value = "验证码")
    private String verifyCode;

}
