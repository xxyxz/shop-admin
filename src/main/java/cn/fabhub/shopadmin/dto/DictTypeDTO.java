package cn.fabhub.shopadmin.dto;

import cn.fabhub.shopadmin.validate.ValidationGroup;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author zhou
 * @date 2020-05-30 21:23
 */
@Data
public class DictTypeDTO {


    @NotNull(message = "字典的Id不能为空", groups = ValidationGroup.Update.class)
    private Long id;

    /**
     * 字典名称
     */
    @NotBlank(message = "字典名称不能为空", groups = ValidationGroup.Add.class)
    private String dictName;

    /**
     * 字典类型
     */
    @NotBlank(message = "字典的类型不能为空", groups = ValidationGroup.Add.class)
    private String dictType;

    /**
     * 状态（字典）
     */
    @NotNull(message = "字典的状态不能为空", groups = ValidationGroup.Add.class)
    private Integer status;

    /**
     * 排序
     */
    private Integer orderNum;

    /**
     * 字典的描述
     */
    private String remark;
}
