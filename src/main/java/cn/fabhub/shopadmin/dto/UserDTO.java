package cn.fabhub.shopadmin.dto;

import cn.fabhub.shopadmin.validate.ValidationGroup;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

/**
 * @author zhou
 * @date 2020-05-30 23:18
 */
@Data
@SuperBuilder
@NoArgsConstructor
@Accessors(chain = true)
public class UserDTO {

    /**
     * 用户Id
     */
    @NotNull(message = "用户Id不能为空",groups = ValidationGroup.Update.class)
    private Long id;

    /**
     * 登录名称，由系统自动生成
     */
    @NotBlank(message = "用户名登录名称不能为空", groups = ValidationGroup.Add.class)
    private String loginName;

    /**
     * 用户别名
     */
    private String username;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 邮件号
     */
    private String email;

    /**
     * 性别(字典)
     */
    private Integer sex;

    /**
     * 登录密码
     */
    @NotBlank(message = "用户密码不能为空", groups = ValidationGroup.Add.class)
    private String password;

    /**
     * 用户头像
     */
    private String avatar;

    /**
     * 激活码
     */
    private String actCode;

    /**
     * 状态(字典)
     */
    private Integer status;

    private List<Long> roleIds;

    private Set<String> roles;

    private Set<String> permissions;
}
