package cn.fabhub.shopadmin.dto;

import cn.fabhub.shopadmin.validate.ValidationGroup;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author zhou
 * @date 2020-05-30 19:42
 */
@Data
public class RoleDTO {


    @NotNull(message = "角色Id不能为空", groups = ValidationGroup.Update.class)
    private Long id;

    /**
     * 角色名称
     */
    @NotBlank(message = "角色的标识不能为空", groups = ValidationGroup.Add.class)
    private String roleName;

    /**
     * 角色中文名称
     */
    @NotBlank(message = "角色名称不能为空", groups = ValidationGroup.Add.class)
    private String zhName;

    /**
     * 角色状态
     */
    @NotNull(message = "角色的状态不能为空", groups = ValidationGroup.Add.class)
    private Integer status;

    /**
     * 是否默认角色
     */
    @NotNull(message = "是否为默认角色不能为空", groups = ValidationGroup.Add.class)
    private Integer isDefault;

    /**
     * 角色描述
     */
    private String remark;

    /**
     * 角色拥有的权限
     */
    private List<Long> resourceIds;
}
