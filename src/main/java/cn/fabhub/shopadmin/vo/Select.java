package cn.fabhub.shopadmin.vo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * @author zhou
 * @date 2020-07-27 18:04
 */
@Data
@SuperBuilder
@NoArgsConstructor
public class Select {

    private Serializable key;

    private String title;

}
