package cn.fabhub.shopadmin.vo;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @author zhou
 * @date 2019-12-25 14:37
 */
@Data
@Accessors(chain = true)
public class R implements Serializable {


    private Integer status = 200;


    private String msg = "ok";


    private Object data;


    // 定义jackson对象
    private static final ObjectMapper MAPPER = new ObjectMapper();


    public static R ok(String msg, Object data) {
        return new R(msg, data);
    }

    public static R ok(Object data) {
        return new R(data);
    }

    public static R ok() {
        return new R(null);
    }

    public R() {
    }

    public R(String msg, Object data) {
        this.msg = msg;
        this.data = data;
    }

    public R(Integer status, String msg, Object data) {
        this.status = status;
        this.msg = msg;
        this.data = data;
    }

    public R(Object data) {
        this.data = data;
    }

    public static R build(Integer status, String msg, Object data) {
        return new R(status, msg, data);
    }

    public static R build(Integer status, String msg) {
        return new R(status, msg, null);
    }

    /**
     * 将json结果集转化为Result对象
     *
     * @param jsonData json数据
     * @param clazz    Result中的object类型
     * @return Result对象
     */
    public static R formatToPojo(String jsonData, Class<?> clazz) {
        try {
            if (clazz == null) {
                return MAPPER.readValue(jsonData, R.class);
            }
            JsonNode jsonNode = MAPPER.readTree(jsonData);
            JsonNode data = jsonNode.get("data");
            Object obj = null;
            if (data.isObject()) {
                obj = MAPPER.readValue(data.traverse(), clazz);
            } else if (data.isTextual()) {
                obj = MAPPER.readValue(data.asText(), clazz);
            }
            return build(jsonNode.get("status").intValue(), jsonNode.get("msg").asText(), obj);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 没有object对象的转化
     *
     * @param json json字符串
     * @return Result 对象
     */
    public static R format(String json) {
        try {
            return MAPPER.readValue(json, R.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Object是集合转化
     *
     * @param jsonData json数据
     * @param clazz    集合中的类型
     * @return Result对象
     */
    public static R formatToList(String jsonData, Class<?> clazz) {
        try {
            JsonNode jsonNode = MAPPER.readTree(jsonData);
            JsonNode data = jsonNode.get("data");
            Object obj = null;
            if (data.isArray() && data.size() > 0) {
                obj = MAPPER.readValue(data.traverse(),
                        MAPPER.getTypeFactory().constructCollectionType(List.class, clazz));
            }
            return build(jsonNode.get("status").intValue(), jsonNode.get("msg").asText(), obj);
        } catch (Exception e) {
            return null;
        }
    }


}
