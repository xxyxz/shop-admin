package cn.fabhub.shopadmin.vo;

import java.util.Set;

/**
 * @author zhou
 * @date 2020-08-24 11:43
 */
// @Data
// @Accessors(chain = true)
public class LoginUser {

    // 唯一标识符
    private String token;

    // 用户名称
    private String username;

    // 用户头像
    private String avatar;

    // 用户角色
    private Set<String> roles;

    // 用户权限
    private Set<String> permissions;

    public LoginUser() {
    }

    public LoginUser(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public LoginUser setToken(String token) {
        this.token = token;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public LoginUser setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getAvatar() {
        return avatar;
    }

    public LoginUser setAvatar(String avatar) {
        this.avatar = avatar;
        return this;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public LoginUser setRoles(Set<String> roles) {
        this.roles = roles;
        return this;
    }

    public Set<String> getPermissions() {
        return permissions;
    }

    public LoginUser setPermissions(Set<String> permissions) {
        this.permissions = permissions;
        return this;
    }
}
