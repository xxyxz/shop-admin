package cn.fabhub.shopadmin.vo;

import lombok.Data;

/**
 * @author zhou
 * @date 2020-09-04 20:08
 */
@Data
public class MetaVO {

    private String title;

    private String icon;

    private Integer openType;

    public MetaVO(String title, String icon, Integer openType) {
        this.title = title;
        this.icon = icon;
        this.openType = openType;
    }
}
