package cn.fabhub.shopadmin.vo;

/**
 * @author zhou
 * @date 2020-08-20 9:54
 */
public class SelectedVO {

    public String label;

    public String value;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public SelectedVO() {
    }

    public SelectedVO(String label, String value) {
        this.label = label;
        this.value = value;
    }

    @Override
    public String toString() {
        return "SelectedVO{" +
                "label='" + label + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
