package cn.fabhub.shopadmin.vo;

import cn.fabhub.shopadmin.constant.NormalStatus;
import cn.fabhub.shopadmin.constant.ResourceType;
import cn.fabhub.shopadmin.entity.SysResource;
import cn.fabhub.shopadmin.factory.BaseTreeNode;
import lombok.Data;
import lombok.experimental.SuperBuilder;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhou
 * @date 2020-09-04 20:01
 */
@Data
@SuperBuilder
public class RouterVO implements BaseTreeNode<RouterVO> {
    // 路由名称
    private String name;
    // 路由
    private String path;
    // 是否隐藏路由：设置为true，不会出现在侧边栏
    private Boolean hidden;
    // 重定向地址
    private String redirect;
    // 组件地址
    private String component;
    // 其他信息
    private MetaVO meta;
    // 子节点
    private List<RouterVO> children;
    // id
    private Long id;
    // 父id
    private Long pid;


    /**
     * 获取节点Id
     *
     * @return id
     */
    @Override
    public Long getId() {
        return this.id;
    }

    /**
     * 获取父节点Id
     *
     * @return 父id
     */
    @Override
    public Long getPid() {
        return this.pid;
    }


    /**
     * 更新最顶级资源的组件：布局
     *
     * @param resource 资源
     * @return 组件
     */
    private static String getComponent(SysResource resource) {
        if (resource.getType().equals(ResourceType.CATALOGUE.getCode())) {
            return "Layout";
        }
        return resource.getComponent();
    }

    /**
     * 转换资源位路由
     *
     * @param resource 资源
     * @return 路由
     */
    private static RouterVO generateRouter(SysResource resource) {
        RouterVO build = RouterVO.builder().build();
        build.setComponent(getComponent(resource));
        build.setHidden(resource.getVisible().equals(NormalStatus.DISABLE.getCode()));
        build.setPath(resource.getRouter());
        build.setMeta(new MetaVO(resource.getName(), resource.getIcon(), resource.getOpenType()));
        build.setId(resource.getId());
        build.setPid(resource.getParentId());
        return build;
    }

    /**
     * 资源集合转换为路由集合
     *
     * @param list 资源集合
     * @return 路由集合
     */
    public static List<RouterVO> parseRouter(List<SysResource> list) {
        List<RouterVO> routerVOS = new ArrayList<>();
        for (SysResource resource : list) {
            RouterVO routerVO = generateRouter(resource);
            if (!CollectionUtils.isEmpty(resource.getChildren())) {
                routerVO.setChildren(parseRouter(resource.getChildren()));
            }
            routerVOS.add(routerVO);
        }
        return routerVOS;
    }

}
