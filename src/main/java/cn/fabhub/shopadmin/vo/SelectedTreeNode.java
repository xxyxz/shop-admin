package cn.fabhub.shopadmin.vo;

import cn.fabhub.shopadmin.factory.BaseTreeNode;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

/**
 * @author zhou
 * @date 2020-07-26 15:31
 */
@Data
public class SelectedTreeNode implements BaseTreeNode<SelectedTreeNode> {

    /*id*/
    private Long key;

    /*标题，显示内容*/
    private String title;

    /*筛选内容*/
    private Long value;

    /* 图标 */
    private String icon;

    /*父id*/
    private Long parentId;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<SelectedTreeNode> children;

    public SelectedTreeNode() {
    }

    public SelectedTreeNode(Long key, String title, Long parentId) {
        this.key = key;
        this.title = title;
        this.value = key;
        this.parentId = parentId;
    }

    /**
     * 获取节点Id
     *
     * @return id
     */
    @Override
    public Long getId() {
        return this.key;
    }

    /**
     * 获取父节点Id
     *
     * @return 父id
     */
    @Override
    public Long getPid() {
        return this.parentId;
    }
}
