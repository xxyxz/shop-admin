package cn.fabhub.shopadmin.vo;

import lombok.Data;

import java.util.UUID;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/10/18 18:59
 */
@Data
public class OnlineUser {

    /**
     * 会话id
     */
    private String sessionId = UUID.randomUUID().toString();

    /**
     * 用户令牌
     */
    private String token;

    /** 用户名称 */
    private String username;

    /** 登录IP地址 */
    private String ipaddr;

    /** 登录地址 */
    private String loginLocation;

    /** 浏览器类型 */
    private String browser;

    /** 操作系统 */
    private String os;

    /** 登录时间 */
    private Long loginTime;

}
