package cn.fabhub.shopadmin.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author zhou
 * @date 2020-07-25 20:47
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class PR<T> {

    /**
     * 当前页码
     */
    private Long pageNum;

    /**
     * 每页大小
     */
    private Long pageSize;

    /**
     * 总共条数
     */
    private Long total;

    /**
     * 数据
     */
    private List<T> rows;


    public PR(Long pageNum, Long pageSize) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }

    // 方法
    public static <T> PR<T> newInstance(Page<T> page) {
        PR<T> pr = new PR<>();
        pr.setPageNum(page.getCurrent());
        pr.setPageSize(page.getSize());
        pr.setTotal(page.getTotal());
        pr.setRows(page.getRecords());
        return pr;
    }

}
