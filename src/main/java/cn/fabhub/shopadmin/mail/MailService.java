package cn.fabhub.shopadmin.mail;

/**
 * @author Mr.zhou
 * @version 1.0
 * @date 2020/11/16 13:06
 */
public interface MailService {

    /**
     * 发送文本邮件
     *
     * @param to      收件人
     * @param subject 主题
     * @param content 内容
     */
    void sendSimpleMail(String to, String subject, String content);


    /**
     * 发送html邮件
     *
     * @param to      收件人
     * @param subject 主题
     * @param content 内容
     */
    void sendHtmlMail(String to, String subject, String content);


    /**
     * 发送带有附件的邮件
     *
     * @param to      收件人
     * @param subject 主题
     * @param content 内容
     */
    void sendAttachMail(String to, String subject, String content, String filePath);

}
