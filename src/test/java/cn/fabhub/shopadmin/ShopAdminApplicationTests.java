package cn.fabhub.shopadmin;

import cn.fabhub.shopadmin.mail.MailService;
import cn.fabhub.shopadmin.quartz.utils.JobClassUtils;
import cn.fabhub.shopadmin.spider.nwpu.SomeWebDriver;
import cn.fabhub.shopadmin.vo.Select;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.Set;

@SpringBootTest
class ShopAdminApplicationTests {

    @Resource
    private MailService mailService;

    @Resource
    private SomeWebDriver someWebDriver;


    @Test
    void testNext() {
        System.out.println(someWebDriver == null);
    }

    @Test
    void checkOut() {
        Set<Select> subClasses = JobClassUtils.getSubClasses();
        System.err.println(subClasses);
    }

    @Test
    void contextLoads() {
        // 邮件发送
        mailService.sendSimpleMail("1226105312@qq.com", "自动填写疫情结果", "已经成功自动填写每日疫情填报");
        System.err.println("hello");
    }
}
